function SgPickers() {

}

SgPickers.prototype.init = function() {
	jQuery(".sg-calndar").addClass("input-width-static");

	jQuery('.sg-calndar').bind("click",function() {
		jQuery("#ui-datepicker-div").css({'z-index': 9999});
	});
}

SgPickers.prototype.changeColor = function(elemet) {
	var selectedName = elemet.attr("name");
	var elementVal = elemet.val();
	if(selectedName == 'countdownNumbersTextColor') {
		jQuery("#sg-counts-text").remove();
	 	jQuery("body").append("<style id=\"sg-counts-text\">.flip-clock-wrapper ul li a div div.inn { color: "+elementVal+"; }</style>");
	}
	if(selectedName == 'countdownNumbersBgColor') {
		jQuery("#sg-counts-style").remove();
	 	jQuery("body").append("<style id=\"sg-counts-style\">.flip-clock-wrapper ul li a div div.inn { background-color: "+elementVal+"; }</style>");
	}
}

SgPickers.prototype.colorPicekr = function() {
	var that = this;
	sgColorPicker = jQuery('.sgOverlayColor').wpColorPicker({
		change: function() {
			var sgColorpicker = jQuery(this);
			that.changeColor(sgColorpicker);
		}
	});
	jQuery(".wp-picker-holder").bind('click',function() {
		var selectedInput = jQuery(this).prev().find('.sgOverlayColor');
		that.changeColor(selectedInput);
	});
}

SgPickers.prototype.datepicker = function() {
	var that = this;
	sgCalendar = jQuery('.sg-calndar').datepicker({
		dateFormat : 'M dd yy',
		minDate: 0
	});
}

jQuery(document).ready(function($){

	pickersObj = new SgPickers();
	pickersObj.init();
	pickersObj.colorPicekr();
	pickersObj.datepicker();
});