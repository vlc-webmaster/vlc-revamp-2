<?php /* Prayer Engine - Uninstall the Plugin */

// If uninstall not called from WordPress exit 

if( !defined( 'WP_UNINSTALL_PLUGIN' ) ) exit (); 

global $wpdb;
	if (function_exists('is_multisite') && is_multisite()) {
		$blog_list = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs",""));
		foreach ($blog_list as $blog) {
			switch_to_blog($blog);
			enm_prayerengine_uninstall();
		}
		switch_to_blog($wpdb->blogid);
	} else {
		enm_prayerengine_uninstall();
	}	

function enm_prayerengine_uninstall() { 
	// Delete option from options table 

	delete_option( 'enm_prayerengine_options' ); 
	delete_option( 'enmpe_db_version' );

	//remove any additional options and custom tables 

	global $wpdb;
	$deletedtable = $wpdb->prefix . "prayers";
	$dprayerwalls = $wpdb->prefix . "pe_prayerwalls";
	$wpdb->query("DROP TABLE IF EXISTS $deletedtable, $dprayerwalls");
}

?>