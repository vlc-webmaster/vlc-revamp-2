<?php 
/* Plugin Name: Prayer Engine 
Plugin URI: http://theprayerengine.com
Description: Prayer Engine is the best way to share prayer requests online. To get started, activate the plugin and open the new "Prayer Engine" menu. Follow the instructions on the <a href="admin.php?page=prayerengine_plugin/prayerengine_plugin.php_userguide">User Guide page</a> to embed your prayer wall, change the color scheme and more.
Version: 1.6.1
Author: Eric Murrell (Volacious) 
Author URI: http://theprayerengine.com */ 


/* ----- Install the Plugin ----- */
global $wp_version;

define ( 'ENMPE_CURRENT_VERSION', '1.6.1' );

register_activation_hook( __FILE__, 'enm_prayerengine_install_ms' ); 

//add_action('activated_plugin','save_error'); // Generate Error Messages for Activation hooks
//function save_error(){
//    update_option('plugin_error',  ob_get_contents());
//}

function enm_prayerengine_install_ms( $network_wide ) { // Check for multisite

	global $wpdb;
	if ( $network_wide ) {
		$blog_list = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs",""));
		foreach ($blog_list as $blog) {
			switch_to_blog($blog);
			enm_prayerengine_install();

			$data = get_option( 'enm_prayerengine_options' ); ;	
			$css_dir = plugin_dir_path( __FILE__ ) . 'css/'; // Shorten code, save 1 call
			ob_start(); // Capture all output (output buffering)
			include($css_dir . 'pe_styles_generate.php'); // Generate CSS
			$css = ob_get_clean(); // Get generated CSS (output buffering)
			file_put_contents($css_dir . 'pe_' . $blog . '_styles.css', $css, LOCK_EX); // Save it
		}
		switch_to_blog($wpdb->blogid);
	} else {
		enm_prayerengine_install();
	}	

}

add_action( 'wpmu_new_blog', 'enmpe_new_blog', 10, 6); // Multisite - If a new site is added		
 
function enmpe_new_blog($blog_id, $user_id, $domain, $path, $site_id, $meta ) {
	global $wpdb;
 
	if (is_plugin_active_for_network('prayerengine_plugin/prayerengine_plugin.php')) {
		$old_blog = $wpdb->blogid;
		switch_to_blog($blog_id);
		enm_prayerengine_install();

		$data = get_option( 'enm_prayerengine_options' ); ;	
		$css_dir = plugin_dir_path( __FILE__ ) . 'css/'; // Shorten code, save 1 call
		ob_start(); // Capture all output (output buffering)
		include($css_dir . 'pe_styles_generate.php'); // Generate CSS
		$css = ob_get_clean(); // Get generated CSS (output buffering)
		file_put_contents($css_dir . 'pe_' . $blog_id . '_styles.css', $css, LOCK_EX); // Save it
		switch_to_blog($old_blog);
	}
}

function enm_prayerengine_install() { // Install Process
	if ( version_compare( get_bloginfo( 'version' ), '3.3', '<' ) ) { // Don't activate plugin if WordPress version is less than 2.8
		$enmpe_old_version_message = "WordPress 3.3 or greater is required to use The Prayer Engine. Please upgrade!";
		exit ($enmpe_old_version_message);
	}

	// Create PE database tables
	global $wpdb;

	// Define DB version
	global $enmpe_db_version;
	$enmpe_db_version = "1.6.1";
	if( !defined(get_option( 'enmpe_db_version' )) ) {
		add_option("enmpe_db_version", $enmpe_db_version);
	} else {
		update_option("enmpe_db_version", $enmpe_db_version);
	}

	$prayers = $wpdb->prefix . "prayers"; 
	if( $wpdb->get_var("SHOW TABLES LIKE '$prayers'") != $prayers ) { // Create and populate the table if it doesn't already exist

		$sql = "CREATE TABLE `$prayers` ( 
			`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  			`name` varchar(100) DEFAULT NULL,
  			`email` varchar(150) DEFAULT NULL,
  			`phone` varchar(20) DEFAULT NULL,
  			`share_option` varchar(30) DEFAULT NULL,
  			`prayer_count` int(11) DEFAULT NULL,
  			`moderated_by` varchar(100) DEFAULT NULL,
  			`post_this` int(1) DEFAULT NULL,
  			`prayer` text,
  			`posted_prayer` text,
  			`date_received` datetime DEFAULT NULL,
  			`brand_new` int(1) DEFAULT NULL,
  			`notifyme` tinyint(1) DEFAULT NULL,
  			`prayer_tweet` varchar(160) DEFAULT NULL,
  			`twitter_ok` tinyint(1) DEFAULT NULL,
  			`desired_share_option` varchar(30) DEFAULT NULL,
			`wall_id` int(11) DEFAULT NULL,
  			`delete_code` char(32) DEFAULT NULL,
  			`mod_code` char(32) DEFAULT NULL,
  			`answered` int(1) DEFAULT NULL) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
		);"; 
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php'); 
		dbDelta($sql); 
	
		$firstprayer = array( // Add a sample prayer to the database
			'id' => '1', 
			'name' => 'Sample Prayer Request', 
			'email' => 'sample@theprayerengine.com',
			'phone' => '',
			'share_option' => 'Share Online',
			'prayer_count' => '5',
			'moderated_by' => 'Setup Robot',
			'post_this' => '1',
			'prayer' => 'This is a sample prayer request. If you\'re seeing this, it means your installation of Prayer Engine is set up successfully! Please log in to WordPress to delete this fake prayer request and complete the Prayer Engine setup process. We hope Prayer Engine is a valuable asset to your ministry!',
			'posted_prayer' => 'This is a sample prayer request. If you\'re seeing this, it means your installation of Prayer Engine is set up successfully! Please log in to WordPress to delete this fake prayer request and complete the Prayer Engine setup process. We hope Prayer Engine is a valuable asset to your ministry!',
			'date_received' => '2010-01-16 17:30:18',
			'brand_new' => '0',
			'notifyme' => '0',
			'prayer_tweet' => 'This is a sample prayer tweet.',
			'twitter_ok' => '1',
			'desired_share_option' => 'Share Online',
			'wall_id' => '1',
			'delete_code' => 'b8d30156ef00fe65f8aaec817f0564fa',
			'answered' => '0'
		); 
		$wpdb->insert( $prayers, $firstprayer );
	}

	$prayerwalls = $wpdb->prefix . "pe_prayerwalls"; 
	if( $wpdb->get_var("SHOW TABLES LIKE '$prayerwalls'") != $prayerwalls ) { // Create and populate the table if it doesn't already exist

		$sql = "CREATE TABLE `$prayerwalls` ( 
			`pwid` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  			`wall_name` varchar(255) DEFAULT NULL,
  			`wall_instructions` text,
  			`wall_success` text,
  			`displaynumber` int(11) DEFAULT NULL,
  			`displaylimit` varchar(25) DEFAULT NULL,
  			`moderation` int(1) DEFAULT NULL,
  			`enabletwitter` int(1) DEFAULT NULL,
  			`enablecount` int(1) DEFAULT NULL) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
		);"; 
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php'); 
		dbDelta($sql); 
	
		$firstprayerwall = array( // Add a sample prayer to the database
			'pwid' => '1', 
			'wall_name' => 'Default Prayer Wall', 
			'wall_instructions' => 'You may add your prayer request to our prayer wall using the form below. Once your prayer request is received, we will share it according to your instructions. Feel free to submit as many prayer requests as you like!',
			'wall_success' => 'Thank you for submitting your prayer request. It is currently being moderated, and will then be shared according to your instructions. Feel free to submit another request by filling out the form again.',
			'displaynumber' => '10',
			'displaylimit' => 'nolimit',
			'moderation' => '1',
			'enabletwitter' => '0',
			'enablecount' => '1'
		); 
		$wpdb->insert( $prayerwalls, $firstprayerwall );
	}
	
	// Set default options
	$enm_prayerengine_options = array(  
		'ministryname' => 'Your Ministry Name Here',
		'robotemail' => 'sample@address.com',
		'spamprotection' => '0',
		'feedtitle' => 'Your Ministry\'s Prayer Feed',
		'twitterfeedtitle' => 'Your Ministry\'s Twitter Feed',
		'feeddescription' => 'This feed delivers the latest prayer requests as soon as they\'re received.',
		'twitterfeeddescription' => 'This feed delivers the latest prayer tweets as soon as they\'re received.',
		'formicon' => 'light',
		'pbg' => 'aaaaaa',
		'pt' => 'ffffff',
		'ftbg' => '35b1bb',
		'ftt' => 'ffffff',
		'fbdr' => '9e9e9e',
		'fbg' => 'f1f1f1',
		'ffbdr' => 'ffffff',
		'fib' => 'ffffff',
		'fit' => '000000',
		'ffbg' => 'e7e7e7',
		'fft' => '000000',
		'fsubbg' => '35b1bb',
		'fsubt' => 'ffffff',
		'ermsg' => '999999',
		'ermsgt' => 'ffffff',
		'sumsg' => '44DEE2',
		'sumsgt' => 'ffffff',
		'pbdr' => '9e9e9e',
		'pft' => '000000',
		'ft' => '000000',
		'prdis' => 'e1e1e1',
		'prdist' => 'ffffff',
		'nchigh' => '35b1bb',
		'patext' => 'ffffff',
		'pabg' => '35b1bb',
		'pag' => '35B1BB',
		'pagt' => 'ffffff',
		'spag' => 'f1f1f1',
		'spagt' => 'D4D4D4',
		'prbg' => 'ffffff',
		'aprbg' => 'f1f1f1',
		'prn' => '000000',
		'prt' => '000000',
		'prd' => '919191',
		'copyt' => 'd6d6d6',
		'credits' => 'light',
		'explorebg' => 'd4d4d4',
		'enableperaw' => '0'); 
	add_option( 'enm_prayerengine_options', $enm_prayerengine_options ); 

	register_uninstall_hook( __FILE__, 'enm_prayerengine_uninstall_ms' ); 
};


/* ----- Uninstall the Plugin ----- */

function enm_prayerengine_uninstall_ms() { // Check for multisite

	global $wpdb;
	if (function_exists('is_multisite') && is_multisite()) {
		$blog_list = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs",""));
		foreach ($blog_list as $blog) {
			switch_to_blog($blog);
			enm_prayerengine_uninstall();
		}
		switch_to_blog($wpdb->blogid);
	} else {
		enm_prayerengine_uninstall();
	}	

}

function enm_prayerengine_uninstall() { 
	delete_option( 'enm_prayerengine_options' ); 
	delete_option( 'enmpe_db_version' ); 
	global $wpdb;
	$deletedtable = $wpdb->prefix . "prayers";
	$dprayerwalls = $wpdb->prefix . "pe_prayerwalls";
	$wpdb->query("DROP TABLE IF EXISTS $deletedtable, $dprayerwalls");
}; 

/* ----- Generate Static Stylesheet ----- */

function generate_pe_options_css($newdata) {

	$data = $newdata;	
	$css_dir = plugin_dir_path( __FILE__ ) . 'css/'; // Shorten code, save 1 call
	ob_start(); // Capture all output (output buffering)

	include($css_dir . 'pe_styles_generate.php'); // Generate CSS

	$css = ob_get_clean(); // Get generated CSS (output buffering)

	if(is_multisite()) { 
		global $current_blog;
		file_put_contents($css_dir . 'pe_' . $current_blog->blog_id . '_styles.css', $css, LOCK_EX); // Save it
	} else {
		file_put_contents($css_dir . 'pe_styles.css', $css, LOCK_EX); // Save it
	}
	
}

/* ----- Modify User Theme to Add Stylesheets and JavaScript ----- */

add_action('template_redirect', 'enm_prayerengine_frontend_content'); 

function enm_prayerengine_frontend_content() {
	/*$enmpe_options = get_option( 'enm_prayerengine_options' ); 
	$enmpe_custom = $enmpe_options['custom'];
	if ( $enmpe_custom == 1 ) {
		wp_register_style( 'PrayerEngineFrontendStylesOne', plugins_url('/custom/css/pe_screen.css', __FILE__) );
		wp_enqueue_style( 'PrayerEngineFrontendStylesOne' );
		wp_register_style( 'PrayerEngineFrontendStylesTwo', plugins_url('/custom/css/pe_colors.css', __FILE__) );
		wp_enqueue_style( 'PrayerEngineFrontendStylesTwo' );
	} else {
		wp_register_style( 'PrayerEngineFrontendStyles', plugins_url('/css/pe_screen.php', __FILE__) );
		wp_enqueue_style( 'PrayerEngineFrontendStyles' );
	}*/
	if(is_multisite()) { 
		global $current_blog;
		wp_register_style( 'PrayerEngineFrontendStyles', plugins_url('/css/pe_' . $current_blog->blog_id . '_styles.css', __FILE__) );
		wp_enqueue_style( 'PrayerEngineFrontendStyles' );
	} else {
		wp_register_style( 'PrayerEngineFrontendStyles', plugins_url('/css/pe_styles.css', __FILE__) );
		wp_enqueue_style( 'PrayerEngineFrontendStyles' );
	}
	wp_register_script( 'PrayerEngineFrontendJavascript', plugins_url('/js/prayerwall.js', __FILE__) );
	wp_enqueue_script( 'PrayerEngineFrontendJavascript' );
}

/* ----- Add IE Code to Header ----- */

add_action ( 'wp_head', 'enmpe_ie_compatibility' );

function enmpe_ie_compatibility()
{
    echo '<!-- Display fixes for Internet Explorer -->
	<!--[if lte IE 6]>
	<link href="' . plugins_url() .'/prayerengine_plugin/css/ie6_fix.css' . '" rel="stylesheet" type="text/css" />
	<![endif]-->
	<!--[if IE 7]>
	<link href="' . plugins_url() .'/prayerengine_plugin/css/ie7_fix.css' . '" rel="stylesheet" type="text/css" />
	<![endif]-->
	<!--[if IE 8]>
	<link href="' . plugins_url() .'/prayerengine_plugin/css/ie8_fix.css' . '" rel="stylesheet" type="text/css" />
	<![endif]-->
	<!-- end display fixes for Internet Explorer -->';
}


/* ----- Create the Admin Menus ----- */

add_action( 'admin_menu', 'enm_prayerengine_create_menu' );

function enm_prayerengine_create_menu() { 
	add_menu_page( 
		'Manage Prayer Requests', 
		'Prayer Engine', 
		'edit_posts', 
		__FILE__, 
		'enm_prayerengine_prayerrequests_page', 
		plugins_url( '/images/blank.png', __FILE__ )
	); 
	
	add_submenu_page( __FILE__, 'Add a Prayer Request', 'Add New Request', 'edit_posts', __FILE__ . '_addnew', 'enm_prayerengine_addnew_page'); 

	add_submenu_page( __FILE__, 'Manage Prayer Walls', 'Manage Prayer Walls', 'edit_posts', __FILE__ . '_prayerwalls', 'enm_prayerengine_prayerwalls_page'); 

	add_submenu_page( __FILE__, 'Embedding Your Prayer Wall', 'Embed Prayer Walls', 'edit_pages', __FILE__ . '_embed', 'enm_prayerengine_embed_page'); 

	add_submenu_page( __FILE__, 'Print Prayer Reports', 'Report Library', 'edit_pages', __FILE__ . '_reports', 'enm_prayerengine_reports_page'); 

	add_submenu_page( __FILE__, 'Using The Prayer Engine', 'User Guide', 'edit_pages', __FILE__ . '_userguide', 'enm_prayerengine_userguide_page'); 

	add_submenu_page( __FILE__, 'Check for Updates', 'Check for Updates', 'edit_pages', __FILE__ . '_update', 'enm_prayerengine_update_page'); 
} 

/* Admin Menu - Settings Page */

add_action('admin_menu', 'enm_prayerengine_add_page'); 

function enm_prayerengine_add_page() { 
	add_options_page( 'Prayer Engine', 'Prayer Engine', 'manage_options', 'enm_prayerengine', 'enm_prayerengine_options_page' ); 
} 

function enm_prayerengine_options_page() {
	include(plugin_dir_path( __FILE__ ) . 'includes/admin/settings.php'); 
}

add_action('admin_init', 'enm_prayerengine_admin_init'); 

function enm_prayerengine_admin_init() {
	// Add stylesheet
	wp_register_style( 'PrayerEngineAdminStylesheet', plugins_url('/css/pe_backend.css', __FILE__) );
	wp_enqueue_style( 'PrayerEngineAdminStylesheet' );
	
	register_setting( 
		'enm_prayerengine_options', 
		'enm_prayerengine_options', 
		'enm_prayerengine_validate_options' 
	); 
	
	// General Settings
	add_settings_section( 
		'enm_prayerengine_settings', 
		'General Settings', 
		'enm_prayerengine_settings_text', 
		'prayerengine_plugin' 
	); 
	
	add_settings_field( 
		'enm_prayerengine_ministry_name', 
		'Your Ministry Name:', 
		'enm_prayerengine_ministryname_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_settings' 
	);
		
	add_settings_field( 
		'enm_prayerengine_robotemail', 
		'Prayer Robot Email Address:', 
		'enm_prayerengine_robotemail_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_settings' 
	);
		
	add_settings_field( 
		'enm_prayerengine_spamprotection', 
		'Use reCAPTCHA to Prevent Spam?', 
		'enm_prayerengine_spamprotection_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_settings' 
	);

	add_settings_section( 
		'enm_prayerengine_rss_settings', 
		'Feed Setup', 
		'enm_prayerengine_rss_settings_text', 
		'prayerengine_plugin' 
	);
		
	// Prayer Wall Styles
	
	add_settings_section( 
		'enm_prayerengine_style_settings', 
		'', 
		'enm_prayerengine_style_settings_text', 
		'prayerengine_plugin' 
	);

	add_settings_section( 
		'enm_prayerengine_form_settings', 
		'', 
		'enm_prayerengine_style_form_text', 
		'prayerengine_plugin' 
	);

	add_settings_section( 
		'enm_prayerengine_pag_settings', 
		'', 
		'enm_prayerengine_style_pag_text', 
		'prayerengine_plugin' 
	);

	add_settings_section( 
		'enm_prayerengine_prayer_settings', 
		'', 
		'enm_prayerengine_style_prayer_text', 
		'prayerengine_plugin' 
	);

	add_settings_section( 
		'enm_prayerengine_brand_settings', 
		'', 
		'enm_prayerengine_style_brand_text', 
		'prayerengine_plugin' 
	);

	add_settings_section( 
		'enm_prayerengine_blank_settings', 
		'', 
		'enm_prayerengine_blank_text', 
		'prayerengine_plugin' 
	);
	
	/*add_settings_field( 
		'enm_prayerengine_custom_style', 
		'Custom CSS? (Experts Only!):', 
		'enm_prayerengine_custom_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_style_settings' 
	);*/
	
	add_settings_field( 
		'enm_prayerengine_override_style', 
		'Enable Formatting Override? (Experts Only!):', 
		'enm_prayerengine_override_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_style_settings' 
	);
	
	/*add_settings_field( 
		'enm_prayerengine_fhbg_style', 
		'Page/Form Title Background:', 
		'enm_prayerengine_fhbg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_style_settings' 
	);*/
	
	add_settings_field( 
		'enm_prayerengine_formicon_style', 
		'Form Toggle Icon:', 
		'enm_prayerengine_formicon_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);

	add_settings_field( 
		'enm_prayerengine_explorebg_style', 
		'Explore Bar Background:', 
		'enm_prayerengine_explorebg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_ftbg_style', 
		'Form Toggle Background:', 
		'enm_prayerengine_ftbg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_ftt_style', 
		'Form Toggle Text:', 
		'enm_prayerengine_ftt_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	
	add_settings_field( 
		'enm_prayerengine_ft_style', 
		'Form Text:', 
		'enm_prayerengine_ft_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_fbg_style', 
		'Form Background:', 
		'enm_prayerengine_fbg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_ffbdr_style', 
		'Form Field Border:', 
		'enm_prayerengine_ffbdr_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_fib_style', 
		'Form Field Background:', 
		'enm_prayerengine_fib_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_fit_style', 
		'Form Field Text:', 
		'enm_prayerengine_fit_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_ffbg_style', 
		'Form Footer Background:', 
		'enm_prayerengine_ffbg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_fsubbg_style', 
		'Form Submit Background:', 
		'enm_prayerengine_fsubbg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_fsubt_style', 
		'Form Submit Text:', 
		'enm_prayerengine_fsubt_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_ermsg_style', 
		'Error Message Background:', 
		'enm_prayerengine_ermsg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_ermsgt_style', 
		'Error Message Text:', 
		'enm_prayerengine_ermsgt_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_sumsg_style', 
		'Success Message Background:', 
		'enm_prayerengine_sumsg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_sumsgt_style', 
		'Success Message Text:', 
		'enm_prayerengine_sumsgt_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_form_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_pag_style', 
		'Page Number Background:', 
		'enm_prayerengine_pag_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_pag_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_pagt_style', 
		'Page Number Text:', 
		'enm_prayerengine_pagt_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_pag_settings' 
	);

	add_settings_field( 
		'enm_prayerengine_spag_style', 
		'Selected Page Background:', 
		'enm_prayerengine_spag_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_pag_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_spagt_style', 
		'Selected Page Text:', 
		'enm_prayerengine_spagt_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_pag_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_prbg_style', 
		'Prayer Background:', 
		'enm_prayerengine_prbg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_aprbg_style', 
		'Alternate Prayer Background:', 
		'enm_prayerengine_aprbg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_pbg_style', 
		'"I Prayed for This" Background:', 
		'enm_prayerengine_pbg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
		);

	add_settings_field( 
		'enm_prayerengine_pt_style', 
		'"I Prayed for This" Text:', 
		'enm_prayerengine_pt_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_prdis_style', 
		'Disabled "I Prayed" Background:', 
		'enm_prayerengine_prdis_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_prdist_style', 
		'Disabled "I Prayed" Text:', 
		'enm_prayerengine_prdist_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_nchigh_style', 
		'Prayer Count Highlight:', 
		'enm_prayerengine_nchigh_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);

	add_settings_field( 
		'enm_prayerengine_patext_style', 
		'Prayer Answered Text:', 
		'enm_prayerengine_patext_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);

	add_settings_field( 
		'enm_prayerengine_pabg_style', 
		'Prayer Answered Background:', 
		'enm_prayerengine_pabg_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_prn_style', 
		'Prayer Name:', 
		'enm_prayerengine_prn_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_prt_style', 
		'Prayer Text:', 
		'enm_prayerengine_prt_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_prd_style', 
		'Prayer Date:', 
		'enm_prayerengine_prd_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_prayer_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_copyt_style', 
		'Copyright Text:', 
		'enm_prayerengine_copyt_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_brand_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_credits_style', 
		'Prayer Engine Logo:', 
		'enm_prayerengine_credits_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_brand_settings' 
	);
	
	// Feed Setup
	
	add_settings_field( 
		'enm_prayerengine_feed_title', 
		'Title of Prayer Feed:', 
		'enm_prayerengine_feedtitle_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_rss_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_feed_description', 
		'Feed Description:', 
		'enm_prayerengine_feeddescription_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_rss_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_twitterfeed_title', 
		'Title of Twitter Feed:', 
		'enm_prayerengine_twitterfeedtitle_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_rss_settings' 
	);
	
	add_settings_field( 
		'enm_prayerengine_twitterfeed_description', 
		'Feed Description:', 
		'enm_prayerengine_twitterfeeddescription_input', 
		'prayerengine_plugin', 
		'enm_prayerengine_rss_settings' 
	);
	

}
	
function enm_prayerengine_settings_text() {
	$pe_prayerwall_url = admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php' ) . '_prayerwalls';
	echo '<div id="enmpe-general-settings"><p>Use the fields below to modify the core settings of the Prayer Engine. You can find Prayer Wall specific settings on the <a href="' . $pe_prayerwall_url . '">Manage Prayer Walls</a> page.</p>';
};

function enm_prayerengine_rss_settings_text() {
	$twitterfeedaddress = home_url();
	echo '<p>Use the fields below to modify the text displayed with The Prayer Engine\'s RSS feeds. If you have prayer tweets enabled, you can import them to Twitter with a service like TwitterFeed using the feed listed under General Settings.</p>';
};

function enm_prayerengine_style_settings_text() {
	$pe_userguide_url = admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php' ) . '_userguide#pe-customizing';
	echo '</div><div id="enmpe-style-settings" style="display: none"><p>Change various styles and color settings throughout the Prayer Engine plugin.</p>';
};

function enm_prayerengine_style_form_text() {
	echo '<h3>Form Styles</h3><p>Change the look of the slide down prayer form and the button that triggers it.</p>';
};

function enm_prayerengine_style_pag_text() {
	echo '<h3>Page Numbers</h3><p>Change the look of the pagination at the bottom of the page.</p>';
};

function enm_prayerengine_style_prayer_text() {
	echo '<h3>Prayer Request List</h3><p>The list of prayer requests on your prayer wall.</p>';
};

function enm_prayerengine_style_brand_text() {
	echo '<h3>Prayer Engine Branding</h3><p>The Prayer Engine copyright at the bottom of the prayer wall.</p>';
};

function enm_prayerengine_blank_text() {
	echo '</div>';
};

function enm_prayerengine_ministryname_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$ministryname = $pe_options['ministryname'];
	echo "<input id='ministryname' name='enm_prayerengine_options[ministryname]' type='text' value='{$pe_options['ministryname']}' size='35' />";
};

function enm_prayerengine_robotemail_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$robotemail = $pe_options['robotemail'];
	echo "<input id='robotemail' name='enm_prayerengine_options[robotemail]' type='text' value='{$pe_options['robotemail']}' size='35' />";
};

function enm_prayerengine_credits_input() { 
	$pe_options = get_option( 'enm_prayerengine_options' );
	$credits = $pe_options['credits'];
	if ($credits == "light") {
		echo "<select id='credits' name='enm_prayerengine_options[credits]'><option value='light' selected='selected'>Light</option><option value='dark'>Dark</option><option value='text'>Text Only</option></select>";
	} elseif ($credits == "dark" ) {
		echo "<select id='credits' name='enm_prayerengine_options[credits]'><option value='light'>Light</option><option value='dark' selected='selected'>Dark</option><option value='text'>Text Only</option></select>";
	} elseif ($credits == "text" ) {
		echo "<select id='credits' name='enm_prayerengine_options[credits]'><option value='light'>Light</option><option value='dark'>Dark</option><option value='text' selected='selected'>Text Only</option></select>";
	} else {
		echo "<select id='credits' name='enm_prayerengine_options[credits]'><option value='light'>Light</option><option value='dark'>Dark</option><option value='text'>Text Only</option></select>";
	}
};

function enm_prayerengine_spamprotection_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$spamprotection = $pe_options['spamprotection'];
	if ($spamprotection == 1) {
		echo "<select id='spamprotection' name='enm_prayerengine_options[spamprotection]'><option value='1' selected='selected'>Yes</option><option value='0'>No</option></select>";
	} elseif ($spamprotection == 0 ) {
		echo "<select id='spamprotection' name='enm_prayerengine_options[spamprotection]'><option value='1'>Yes</option><option value='0' selected='selected'>No</option></select>";
	} else {
		echo "<select id='spamprotection' name='enm_prayerengine_options[spamprotection]'><option value='1'>Yes</option><option value='0'>No</option></select>";
	}
};

function enm_prayerengine_formicon_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$formicon = $pe_options['formicon'];
	if ($formicon == "light") {
		echo "<select id='formicon' name='enm_prayerengine_options[formicon]'><option value='light' selected='selected'>Light</option><option value='dark'>Dark</option></select>";
	} elseif ($formicon == "dark" ) {
		echo "<select id='formicon' name='enm_prayerengine_options[formicon]'><option value='light'>Light</option><option value='dark' selected='selected'>Dark</option></select>";
	} else {
		echo "<select id='formicon' name='enm_prayerengine_options[formicon]'><option value='light'>Light</option><option value='dark'>Dark</option></select>";
	}
};

function enm_prayerengine_custom_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$custom = $pe_options['custom'];
	if ($custom == 1) {
		echo "<select id='custom' name='enm_prayerengine_options[custom]'><option value='1' selected='selected'>Yes</option><option value='0'>No</option></select><p>When enabled, you can dramatically alter the appearance of your prayer wall by modifying the stylesheets in this plugin's \"custom\" folder.</p>";
	} elseif ($custom == 0 ) {
		echo "<select id='custom' name='enm_prayerengine_options[custom]'><option value='1'>Yes</option><option value='0' selected='selected'>No</option></select><p>When enabled, you can dramatically alter the appearance of your prayer wall by modifying the stylesheets in this plugin's \"custom\" folder.</p>";
	} else {
		echo "<select id='custom' name='enm_prayerengine_options[custom]'><option value='1'>Yes</option><option value='0'>No</option></select><p>When enabled, you can dramatically alter the appearance of your prayer wall by modifying the stylesheets in this plugin's \"custom\" folder.</p>";
	}
};

function enm_prayerengine_override_input() {
	$pe_userguide_url = admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php' ) . '_userguide#pe-css-troubleshooting';
	$pe_options = get_option( 'enm_prayerengine_options' );
	$override = $pe_options['enableperaw'];
	if ($override == 1) {
		echo "<select id='override' name='enm_prayerengine_options[enableperaw]'><option value='1' selected='selected'>Yes</option><option value='0'>No</option></select><p>When enabled, you can <a href=\"" . $pe_userguide_url . "\">override overzealous formatting</a> from your theme or another plugin with a shortcode.</p>";
	} elseif ($override == 0 ) {
		echo "<select id='override' name='enm_prayerengine_options[enableperaw]'><option value='1'>Yes</option><option value='0' selected='selected'>No</option></select><p>When enabled, you can <a href=\"" . $pe_userguide_url . "\">override overzealous formatting</a> from your theme or another plugin with a shortcode.</p>";
	} else {
		echo "<select id='override' name='enm_prayerengine_options[enableperaw]'><option value='1'>Yes</option><option value='0'>No</option></select><p>When enabled, you can <a href=\"" . $pe_userguide_url . "\">override overzealous formatting</a> from your theme or another plugin with a shortcode.</p>";
	}
};

function enm_prayerengine_feedtitle_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$feedtitle = stripslashes($pe_options['feedtitle']);
	echo "<input id='feedtitle' name='enm_prayerengine_options[feedtitle]' type='text' value=\"{$feedtitle}\" size='35' />";
};

function enm_prayerengine_feeddescription_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$feeddescription = stripslashes($pe_options['feeddescription']);
	echo "<textarea name=\"enm_prayerengine_options[feeddescription]\" rows=\"5\" cols=\"40\">{$feeddescription}</textarea>";
};

function enm_prayerengine_twitterfeedtitle_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$twitterfeedtitle = stripslashes($pe_options['twitterfeedtitle']);
	echo "<input id='feedtitle' name='enm_prayerengine_options[twitterfeedtitle]' type='text' value=\"{$twitterfeedtitle}\" size='35' />";
};

function enm_prayerengine_twitterfeeddescription_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$twitterfeeddescription = stripslashes($pe_options['twitterfeeddescription']);
	echo "<textarea name=\"enm_prayerengine_options[twitterfeeddescription]\" rows=\"5\" cols=\"40\">{$twitterfeeddescription}</textarea><br /><br />";
};

// Style Fields

function enm_prayerengine_explorebg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$explorebg = stripslashes($pe_options['explorebg']);
	echo "<div id='c-explorebg' class='pe-colorpicker' style='background-color: #{$explorebg}'></div>#<input id='explorebg' name='enm_prayerengine_options[explorebg]' type='text' value=\"{$explorebg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_pbg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$pbg = stripslashes($pe_options['pbg']);
	echo "<div id='c-pbg' class='pe-colorpicker' style='background-color: #{$pbg}'></div>#<input id='pbg' name='enm_prayerengine_options[pbg]' type='text' value=\"{$pbg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_pt_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$pt = stripslashes($pe_options['pt']);
	echo "<div id='c-pt' class='pe-colorpicker' style='background-color: #{$pt}'></div>#<input id='pt' name='enm_prayerengine_options[pt]' type='text' value=\"{$pt}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_ftbg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$ftbg = stripslashes($pe_options['ftbg']);
	echo "<div id='c-ftbg' class='pe-colorpicker' style='background-color: #{$ftbg}'></div>#<input id='ftbg' name='enm_prayerengine_options[ftbg]' type='text' value=\"{$ftbg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_ftt_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$ftt = stripslashes($pe_options['ftt']);
	echo "<div id='c-ftt' class='pe-colorpicker' style='background-color: #{$ftt}'></div>#<input id='ftt' name='enm_prayerengine_options[ftt]' type='text' value=\"{$ftt}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_fbg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$fbg = stripslashes($pe_options['fbg']);
	echo "<div id='c-fbg' class='pe-colorpicker' style='background-color: #{$fbg}'></div>#<input id='fbg' name='enm_prayerengine_options[fbg]' type='text' value=\"{$fbg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_ffbdr_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$ffbdr = stripslashes($pe_options['ffbdr']);
	echo "<div id='c-ffbdr' class='pe-colorpicker' style='background-color: #{$ffbdr}'></div>#<input id='ffbdr' name='enm_prayerengine_options[ffbdr]' type='text' value=\"{$ffbdr}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_ffbg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$ffbg = stripslashes($pe_options['ffbg']);
	echo "<div id='c-ffbg' class='pe-colorpicker' style='background-color: #{$ffbg}'></div>#<input id='ffbg' name='enm_prayerengine_options[ffbg]' type='text' value=\"{$ffbg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_fft_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$fft = stripslashes($pe_options['fft']);
	echo "<div id='c-fft' class='pe-colorpicker' style='background-color: #{$fft}'></div>#<input id='fft' name='enm_prayerengine_options[fft]' type='text' value=\"{$fft}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_fsubbg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$fsubbg = stripslashes($pe_options['fsubbg']);
	echo "<div id='c-fsubbg' class='pe-colorpicker' style='background-color: #{$fsubbg}'></div>#<input id='fsubbg' name='enm_prayerengine_options[fsubbg]' type='text' value=\"{$fsubbg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_fsubt_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$fsubt = stripslashes($pe_options['fsubt']);
	echo "<div id='c-fsubt' class='pe-colorpicker' style='background-color: #{$fsubt}'></div>#<input id='fsubt' name='enm_prayerengine_options[fsubt]' type='text' value=\"{$fsubt}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_pft_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$pft = stripslashes($pe_options['pft']);
	echo "<div id='c-pft' class='pe-colorpicker' style='background-color: #{$pft}'></div>#<input id='pft' name='enm_prayerengine_options[pft]' type='text' value=\"{$pft}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_ft_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$ft = stripslashes($pe_options['ft']);
	echo "<div id='c-ft' class='pe-colorpicker' style='background-color: #{$ft}'></div>#<input id='ft' name='enm_prayerengine_options[ft]' type='text' value=\"{$ft}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_pag_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$pag = stripslashes($pe_options['pag']);
	echo "<div id='c-pag' class='pe-colorpicker' style='background-color: #{$pag}'></div>#<input id='pag' name='enm_prayerengine_options[pag]' type='text' value=\"{$pag}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_spag_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$spag = stripslashes($pe_options['spag']);
	echo "<div id='c-spag' class='pe-colorpicker' style='background-color: #{$spag}'></div>#<input id='spag' name='enm_prayerengine_options[spag]' type='text' value=\"{$spag}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_spagt_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$spagt = stripslashes($pe_options['spagt']);
	echo "<div id='c-spagt' class='pe-colorpicker' style='background-color: #{$spagt}'></div>#<input id='spagt' name='enm_prayerengine_options[spagt]' type='text' value=\"{$spagt}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_prbg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$prbg = stripslashes($pe_options['prbg']);
	echo "<div id='c-prbg' class='pe-colorpicker' style='background-color: #{$prbg}'></div>#<input id='prbg' name='enm_prayerengine_options[prbg]' type='text' value=\"{$prbg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_aprbg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$aprbg = stripslashes($pe_options['aprbg']);
	echo "<div id='c-aprbg' class='pe-colorpicker' style='background-color: #{$aprbg}'></div>#<input id='aprbg' name='enm_prayerengine_options[aprbg]' type='text' value=\"{$aprbg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_prn_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$prn = stripslashes($pe_options['prn']);
	echo "<div id='c-prn' class='pe-colorpicker' style='background-color: #{$prn}'></div>#<input id='prn' name='enm_prayerengine_options[prn]' type='text' value=\"{$prn}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_prt_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$prt = stripslashes($pe_options['prt']);
	echo "<div id='c-prt' class='pe-colorpicker' style='background-color: #{$prt}'></div>#<input id='prt' name='enm_prayerengine_options[prt]' type='text' value=\"{$prt}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_prd_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$prd = stripslashes($pe_options['prd']);
	echo "<div id='c-prd' class='pe-colorpicker' style='background-color: #{$prd}'></div>#<input id='prd' name='enm_prayerengine_options[prd]' type='text' value=\"{$prd}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_fib_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$fib = stripslashes($pe_options['fib']);
	echo "<div id='c-fib' class='pe-colorpicker' style='background-color: #{$fib}'></div>#<input id='fib' name='enm_prayerengine_options[fib]' type='text' value=\"{$fib}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_fit_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$fit = stripslashes($pe_options['fit']);
	echo "<div id='c-fit' class='pe-colorpicker' style='background-color: #{$fit}'></div>#<input id='fit' name='enm_prayerengine_options[fit]' type='text' value=\"{$fit}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_fbdr_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$fbdr = stripslashes($pe_options['fbdr']);
	echo "<div id='c-fbdr' class='pe-colorpicker' style='background-color: #{$fbdr}'></div>#<input id='fbdr' name='enm_prayerengine_options[fbdr]' type='text' value=\"{$fbdr}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_pbdr_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$pbdr = stripslashes($pe_options['pbdr']);
	echo "<div id='c-pbdr' class='pe-colorpicker' style='background-color: #{$pbdr}'></div>#<input id='pbdr' name='enm_prayerengine_options[pbdr]' type='text' value=\"{$pbdr}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_ermsg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$ermsg = stripslashes($pe_options['ermsg']);
	echo "<div id='c-ermsg' class='pe-colorpicker' style='background-color: #{$ermsg}'></div>#<input id='ermsg' name='enm_prayerengine_options[ermsg]' type='text' value=\"{$ermsg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_ermsgt_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$ermsgt = stripslashes($pe_options['ermsgt']);
	echo "<div id='c-ermsgt' class='pe-colorpicker' style='background-color: #{$ermsgt}'></div>#<input id='ermsgt' name='enm_prayerengine_options[ermsgt]' type='text' value=\"{$ermsgt}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_sumsg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$sumsg = stripslashes($pe_options['sumsg']);
	echo "<div id='c-sumsg' class='pe-colorpicker' style='background-color: #{$sumsg}'></div>#<input id='sumsg' name='enm_prayerengine_options[sumsg]' type='text' value=\"{$sumsg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_sumsgt_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$sumsgt = stripslashes($pe_options['sumsgt']);
	echo "<div id='c-sumsgt' class='pe-colorpicker' style='background-color: #{$sumsgt}'></div>#<input id='sumsgt' name='enm_prayerengine_options[sumsgt]' type='text' value=\"{$sumsgt}\" size='10' class='pe-colorfield' /><br /><br />";
};

function enm_prayerengine_prdis_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$prdis = stripslashes($pe_options['prdis']);
	echo "<div id='c-prdis' class='pe-colorpicker' style='background-color: #{$prdis}'></div>#<input id='prdis' name='enm_prayerengine_options[prdis]' type='text' value=\"{$prdis}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_prdist_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$prdist = stripslashes($pe_options['prdist']);
	echo "<div id='c-prdist' class='pe-colorpicker' style='background-color: #{$prdist}'></div>#<input id='prdist' name='enm_prayerengine_options[prdist]' type='text' value=\"{$prdist}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_nchigh_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$nchigh = stripslashes($pe_options['nchigh']);
	echo "<div id='c-nchigh' class='pe-colorpicker' style='background-color: #{$nchigh}'></div>#<input id='nchigh' name='enm_prayerengine_options[nchigh]' type='text' value=\"{$nchigh}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_patext_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$patext = stripslashes($pe_options['patext']);
	echo "<div id='c-patext' class='pe-colorpicker' style='background-color: #{$patext}'></div>#<input id='patext' name='enm_prayerengine_options[patext]' type='text' value=\"{$patext}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_pabg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$pabg = stripslashes($pe_options['pabg']);
	echo "<div id='c-pabg' class='pe-colorpicker' style='background-color: #{$pabg}'></div>#<input id='pabg' name='enm_prayerengine_options[pabg]' type='text' value=\"{$pabg}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_pagt_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$pagt = stripslashes($pe_options['pagt']);
	echo "<div id='c-pagt' class='pe-colorpicker' style='background-color: #{$pagt}'></div>#<input id='pagt' name='enm_prayerengine_options[pagt]' type='text' value=\"{$pagt}\" size='10' class='pe-colorfield' />";
};

function enm_prayerengine_copyt_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$copyt = stripslashes($pe_options['copyt']);
	echo "<div id='c-copyt' class='pe-colorpicker' style='background-color: #{$copyt}'></div>#<input id='copyt' name='enm_prayerengine_options[copyt]' type='text' value=\"{$copyt}\" size='10' class='pe-colorfield' />";
};

/*function enm_prayerengine_fhbg_input() {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$fhbg = stripslashes($pe_options['fhbg']);
	echo "<div id='c-fhbg' class='pe-colorpicker' style='background-color: #{$fhbg}'></div>#<input id='fhbg' name='enm_prayerengine_options[fhbg]' type='text' value=\"{$fhbg}\" size='10' class='pe-colorfield' />";
};*/

// Validate Prayer Engine Settings
function enm_prayerengine_validate_options($input) {

	$valid['ministryname'] = strip_tags( $input['ministryname'] );
	$valid['robotemail'] = $input['robotemail'];
	$valid['spamprotection'] = $input['spamprotection'];
	$valid['credits'] = $input['credits'];
	$valid['feedtitle'] = addslashes(strip_tags($input['feedtitle']));
	$valid['feeddescription'] = addslashes(strip_tags($input['feeddescription']));
	$valid['twitterfeedtitle'] = addslashes(strip_tags($input['twitterfeedtitle']));
	$valid['twitterfeeddescription'] = addslashes(strip_tags($input['twitterfeeddescription']));
	$valid['enableperaw'] = $input['enableperaw'];
	$valid['formicon'] = $input['formicon'];
	$valid['explorebg'] = $input['explorebg'];
	$valid['pbg'] = strip_tags( $input['pbg'] );
	$valid['pt'] = strip_tags( $input['pt'] );
	$valid['ftbg'] = strip_tags( $input['ftbg'] );
	$valid['fbg'] = strip_tags( $input['fbg'] );
	$valid['ftt'] = strip_tags( $input['ftt'] );
	$valid['ffbdr'] = strip_tags( $input['ffbdr'] );
	$valid['ffbg'] = strip_tags( $input['ffbg'] );
	$valid['fsubbg'] = strip_tags( $input['fsubbg'] );
	$valid['fsubt'] = strip_tags( $input['fsubt'] );
	$valid['ft'] = strip_tags( $input['ft'] );
	$valid['pag'] = strip_tags( $input['pag'] );
	$valid['spag'] = strip_tags( $input['spag'] );
	$valid['spagt'] = strip_tags( $input['spagt'] );
	$valid['prbg'] = strip_tags( $input['prbg'] );
	$valid['aprbg'] = strip_tags( $input['aprbg'] );
	$valid['prn'] = strip_tags( $input['prn'] );
	$valid['prt'] = strip_tags( $input['prt'] );
	$valid['prd'] = strip_tags( $input['prd'] );
	$valid['fib'] = strip_tags( $input['fib'] );
	$valid['fit'] = strip_tags( $input['fit'] );
	$valid['ermsg'] = strip_tags( $input['ermsg'] );
	$valid['ermsgt'] = strip_tags( $input['ermsgt'] );
	$valid['sumsg'] = strip_tags( $input['sumsg'] );
	$valid['sumsgt'] = strip_tags( $input['sumsgt'] );
	$valid['prdis'] = strip_tags( $input['prdis'] );
	$valid['prdist'] = strip_tags( $input['prdist'] );
	$valid['nchigh'] = strip_tags( $input['nchigh'] );
	$valid['patext'] = strip_tags( $input['patext'] );
	$valid['pabg'] = strip_tags( $input['pabg'] );
	$valid['pagt'] = strip_tags( $input['pagt'] );
	$valid['copyt'] = strip_tags( $input['copyt'] );
	/*$valid['fhbg'] = strip_tags( $input['fhbg'] );*/
		
	if ( empty( $input['ministryname'] ) ) {
		add_settings_error( 'enm_prayerengine_ministryname_input', 'enm_prayerengine_texterror', 'You must enter a ministry name!', 'error' );
		$valid['ministryname'] = 'Your Ministry Name Here';
	}
	
	if (!preg_match('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,8})$^', $input['robotemail'])) { 
		add_settings_error( 'enm_prayerengine_robotemail_input', 'enm_prayerengine_texterror', 'Please enter an email address for the robot!', 'error' );
		$valid['robotemail'] = "sample@address.com";
	}

	if ( empty( $input['explorebg'] ) ) {
		$valid['explorebg'] = 'd4d4d4';
	}
	
	if ( empty( $input['pbg'] ) ) {
		$valid['pbg'] = 'aaaaaa';
	}
	
	if ( empty( $input['pt'] ) ) {
		$valid['pt'] = 'ffffff';
	}
	
	if ( empty( $input['fhbg'] ) ) {
		$valid['fhbg'] = 'ffffff';
	}
	
	if ( empty( $input['ftbg'] ) ) {
		$valid['ftbg'] = '35b1bb';
	}
	
	if ( empty( $input['ftt'] ) ) {
		$valid['ftt'] = 'ffffff';
	}
	
	if ( empty( $input['fbdr'] ) ) {
		$valid['fbdr'] = '9e9e9e';
	}
	
	if ( empty( $input['fbg'] ) ) {
		$valid['fbg'] = 'f1f1f1';
	}
	
	if ( empty( $input['ffbdr'] ) ) {
		$valid['ffbdr'] = 'ffffff';
	}
	
	if ( empty( $input['fib'] ) ) {
		$valid['fib'] = 'ffffff';
	}
	
	if ( empty( $input['fit'] ) ) {
		$valid['fit'] = '000000';
	}
	
	if ( empty( $input['ffbg'] ) ) {
		$valid['ffbg'] = 'e7e7e7';
	}
	
	if ( empty( $input['fft'] ) ) {
		$valid['fft'] = '000000';
	}
	
	if ( empty( $input['fsubbg'] ) ) {
		$valid['fsubbg'] = '35B1BB';
	}
	
	if ( empty( $input['fsubt'] ) ) {
		$valid['fsubt'] = 'ffffff';
	}
	
	if ( empty( $input['ermsg'] ) ) {
		$valid['ermsg'] = '999999';
	}
	
	if ( empty( $input['ermsgt'] ) ) {
		$valid['ermsgt'] = 'ffffff';
	}
	
	if ( empty( $input['sumsg'] ) ) {
		$valid['sumsg'] = '44DEE2';
	}
	
	if ( empty( $input['sumsgt'] ) ) {
		$valid['sumsgt'] = 'ffffff';
	}
	
	if ( empty( $input['pbdr'] ) ) {
		$valid['pbdr'] = '9e9e9e';
	}
	
	if ( empty( $input['pft'] ) ) {
		$valid['pft'] = '000000';
	}
	
	if ( empty( $input['ft'] ) ) {
		$valid['ft'] = '000000';
	}
	
	if ( empty( $input['prdis'] ) ) {
		$valid['prdis'] = 'e1e1e1';
	}
	
	if ( empty( $input['prdist'] ) ) {
		$valid['prdist'] = 'ffffff';
	}
	
	if ( empty( $input['nchigh'] ) ) {
		$valid['nchigh'] = '35b1bb';
	}

	if ( empty( $input['patext'] ) ) {
		$valid['patext'] = 'ffffff';
	}

	if ( empty( $input['pabg'] ) ) {
		$valid['pabg'] = '919191';
	}
	
	if ( empty( $input['pag'] ) ) {
		$valid['pag'] = '35B1BB';
	}
	
	if ( empty( $input['pagt'] ) ) {
		$valid['pagt'] = 'ffffff';
	}
	
	if ( empty( $input['prbg'] ) ) {
		$valid['prbg'] = 'ffffff';
	}
	
	if ( empty( $input['aprbg'] ) ) {
		$valid['aprbg'] = 'f1f1f1';
	}
	
	if ( empty( $input['prn'] ) ) {
		$valid['prn'] = '000000';
	}
	
	if ( empty( $input['prt'] ) ) {
		$valid['prt'] = '000000';
	}
	
	if ( empty( $input['prd'] ) ) {
		$valid['prd'] = '919191';
	}
	
	if ( empty( $input['copyt'] ) ) {
		$valid['copyt'] = 'd6d6d6';
	}

	if ( empty( $input['spag'] ) ) { 
		$valid['spag'] = 'f1f1f1';
	}

	if ( empty( $input['spagt'] ) ) { 
		$valid['spagt'] = 'D4D4D4';
	}

	return $valid; 
};

/* Admin Menu - Prayer Requests Page */

function enm_prayerengine_prayerrequests_page() {
	include(plugin_dir_path( __FILE__ ) . 'includes/admin/prayerrequests.php'); 
}

/* Admin Menu - Add a New Request */

function enm_prayerengine_addnew_page() {
	include(plugin_dir_path( __FILE__ ) . 'includes/admin/addnew.php'); 
}

/* Admin Menu - Embed Code */

function enm_prayerengine_embed_page() {
	include(plugin_dir_path( __FILE__ ) . 'includes/admin/embed.php'); 
}

/* Admin Menu - Prayer Reports */

function enm_prayerengine_reports_page() {
	include(plugin_dir_path( __FILE__ ) . 'includes/admin/reports.php'); 
}

/* Admin Menu - User Guide */

function enm_prayerengine_userguide_page() {
	include(plugin_dir_path( __FILE__ ) . 'includes/admin/userguide.php'); 
}

/* Admin Menu - Check for Updates */

function enm_prayerengine_update_page() {
	include(plugin_dir_path( __FILE__ ) . 'includes/admin/update.php'); 
}

/* Admin Menu - Manage Prayer Walls */

function enm_prayerengine_prayerwalls_page() {
	include(plugin_dir_path( __FILE__ ) . 'includes/admin/prayerwalls.php'); 
}

/* ----- Embed the Prayer Wall ----- */

// Register a new shortcode: [prayerengine] 
add_shortcode( 'prayerengine', 'enm_prayerengine_embedwall' ); // The callback function that will replace [prayerengine] 

function enm_prayerengine_wall() {
	include(plugin_dir_path( __FILE__ ) . 'includes/prayerwall.php'); 
	
}
function enm_prayerengine_embedwall() {
	ob_start(); // do it this way to render within page content
	$enmpe_s = 0;
	include(plugin_dir_path( __FILE__ ) . 'includes/prayerwall.php'); 
	$content = ob_get_clean();
	return $content; 
}

/* ------ Embed the Custom Prayer Wall ------- */

// Register a new shortcode: [prayerengine_s] 
add_shortcode( 'prayerengine_s', 'enm_prayerengine_embedwall_s' ); // The callback function that will replace [prayerengine] 

function enm_prayerengine_embedwall_s( $attr ) {
	ob_start(); // do it this way to render within page content
	if ( isset($attr['enmpe_sw']) ) { // Is a wall specified?
		$enmpe_s = 1;
		$enmpe_sw = $attr['enmpe_sw'];
	} else {
		$enmpe_s = 0;
	}
	include(plugin_dir_path( __FILE__ ) . 'includes/prayerwall.php'); 
	$content = ob_get_clean();
	return $content; 
}


/* ----- Custom RSS Feeds ----- */

// Main RSS Feed of Prayer Requests

function enm_prayerengine_prayer_feed() {
	load_template( plugin_dir_path( __FILE__ ) . 'feeds/prayers.php' ); 
}

add_action('do_feed_prayerengine', 'enm_prayerengine_prayer_feed', 10, 1);

function enm_prayerengine_twitter_feed() {
	load_template( plugin_dir_path( __FILE__ ) . 'feeds/twitter.php' ); 
}

add_action('do_feed_prayertweets', 'enm_prayerengine_twitter_feed', 10, 1);

/* ----- Modify User Theme to Add jQuery ----- */

add_action('init', 'enm_prayerengine_add_jquery'); 

function enm_prayerengine_add_jquery() {
	wp_enqueue_script( 'jquery' );
}

/* ----- Add Prayer Engine Settings to User Page ----- */

add_action( 'show_user_profile', 'enm_prayerengine_user_settings' ); 
add_action( 'edit_user_profile', 'enm_prayerengine_user_settings' ); 

function enm_prayerengine_user_settings( $user ) { // show prayer engine form on admin
	global $wp_version;
	if ( $wp_version >= 3.0 ) {
		$enmpe_adminemail = get_user_meta( $user->ID, 'prayerengine_admin_email', true ); 
	} else {
		$enmpe_adminemail = get_usermeta( $user->ID, 'prayerengine_admin_email', true ); 
	}
	include(plugin_dir_path( __FILE__ ) . 'includes/admin/admin_user_settings.php'); 
}

add_action( 'personal_options_update', 'enm_prayerengine_update_user_settings' ); 
add_action( 'edit_user_profile_update', 'enm_prayerengine_update_user_settings' ); 

function enm_prayerengine_update_user_settings( $user_id ) { // show prayer engine form on admin
	if ( !current_user_can( 'edit_user', $user_id ) ) 
	return false; 
	$updatepeadminemail = $_POST['prayerengine_admin_email']; 
	global $wp_version;
	if ( $wp_version >= 3.0 ) {
		update_user_meta( $user_id, 'prayerengine_admin_email', $updatepeadminemail ); 
	} else {
		update_usermeta( $user_id, 'prayerengine_admin_email', $updatepeadminemail ); 
	}
}

/* ----- Check for New Versions of the Plugin ----- */

function enmpe_trigger_force_updates_check() {

	if( ! isset( $_GET['action'] ) || 'prayer_engine_update' != $_GET['action'] ) {
		return;
	}

	if( ! current_user_can( 'install_plugins' ) ) {
		return;
	}

	set_site_transient( 'update_plugins', null );

	wp_safe_redirect( admin_url( 'index.php' ) ); exit;

}
add_action( 'admin_init', 'enmpe_trigger_force_updates_check' );

/* ----- Check for New Versions of the Plugin ----- */

define( 'ENMPE_ALT_API', 'http://pluginupdates.theprayerengine.com' );

// Hook into the plugin update check
add_filter('pre_set_site_transient_update_plugins', 'enmpe_altapi_check');

function enmpe_altapi_check( $transient ) {

    if( empty( $transient->checked ) )
        return $transient;
    
    $plugin_slug = plugin_basename( __FILE__ );
    
    $args = array(
        'action' => 'update-check',
        'plugin_name' => $plugin_slug,
        'version' => $transient->checked[$plugin_slug]
    );
    
    $response = enmpe_altapi_request( $args );
    
    if( false !== $response ) {
        $transient->response[$plugin_slug] = $response;
    }
    
    return $transient;
}

// Send a request to the alternative API, return an object
function enmpe_altapi_request( $args ) {

    $request = wp_remote_post( ENMPE_ALT_API, array( 'body' => $args ) );
    
    if( is_wp_error( $request )
    or
    wp_remote_retrieve_response_code( $request ) != 200
    ) {
        return false;
    }
    
    $response = unserialize( wp_remote_retrieve_body( $request ) );
    if( is_object( $response ) ) {
        return $response;
    } else {
        return false;
    }
}

// Plugin details screen

add_filter('plugins_api', 'enmpe_altapi_information', 10, 3);

function enmpe_altapi_information( $false, $action, $args ) {

    $plugin_slug = plugin_basename( __FILE__ );

    if( $args->slug != $plugin_slug ) {
        return $false;
    }
        
    $args = array(
        'action' => 'plugin_information',
        'plugin_name' => $plugin_slug,
        'version' => $transient->checked[$plugin_slug]
    );
    
    $response = enmpe_altapi_request( $args );
    
    $request = wp_remote_post( ENMPE_ALT_API, array( 'body' => $args ) );

    return $response;
}

/* Override WordPress's Crazy default insertion of elements using [peraw] */

function enmpe_formatter($content) {
	$pe_options = get_option( 'enm_prayerengine_options' );
	$enableperaw = $pe_options['enableperaw'];
	if ($enableperaw == 1) {
		remove_filter('the_content', 'wpautop');
		remove_filter('the_content', 'wptexturize');
	   	$new_content = '';
       	$pattern_full = '{(\[peraw\].*?\[/peraw\])}is';
       	$pattern_contents = '{\[peraw\](.*?)\[/peraw\]}is';
       	$pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

       	foreach ($pieces as $piece) {
        	if (preg_match($pattern_contents, $piece, $matches)) {
            	$new_content .= $matches[1];
        	} else {
                $new_content .= wptexturize(wpautop($piece));
            }
		}

       	return $new_content;
	} else { // Do nothing if the code's not turned on
		return $content;
	}
}

add_filter('the_content', 'enmpe_formatter', 99);

/* Upgrade the Plugin DB */

// Running less than version 1.2?
if ( !get_option( 'enmpe_db_version' ) ) {
	
	if ( !get_option( 'enm_prayerengine_options' ) ) {
	} else {
		global $wpdb;

		$pe_options = get_option( 'enm_prayerengine_options' );
		generate_pe_options_css($pe_options);

		// Define DB version
		global $enmpe_db_version;
		$enmpe_db_version = "1.6.1";
		add_option("enmpe_db_version", $enmpe_db_version);

		$prayerwalls = $wpdb->prefix . "pe_prayerwalls"; 
		if( $wpdb->get_var("SHOW TABLES LIKE '$prayerwalls'") != $prayerwalls ) { // Create and populate the table if it doesn't already exist

			$sql = "CREATE TABLE `$prayerwalls` ( 
				`pwid` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  				`wall_name` varchar(255) DEFAULT NULL,
  				`wall_instructions` text,
  				`wall_success` text,
  				`displaynumber` int(11) DEFAULT NULL,
  				`displaylimit` varchar(25) DEFAULT NULL,
  				`moderation` int(1) DEFAULT NULL,
  				`enabletwitter` int(1) DEFAULT NULL,
  				`enablecount` int(1) DEFAULT NULL) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
			);"; 
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php'); 
			dbDelta($sql); 

			$enmpe_options = get_option( 'enm_prayerengine_options' );  
	
			$firstprayerwall = array( // Add a sample prayer to the database
				'pwid' => '1', 
				'wall_name' => 'Default Prayer Wall', 
				'wall_instructions' => $enmpe_options['instructions'],
				'wall_success' => $enmpe_options['success'],
				'displaynumber' => $enmpe_options['prayersdisplayed'],
				'displaylimit' => $enmpe_options['displaylimit'],
				'moderation' => $enmpe_options['moderation'],
				'enabletwitter' => $enmpe_options['enabletwitter'],
				'enablecount' => '1'
			); 
			$wpdb->insert( $prayerwalls, $firstprayerwall );
		}

		$prayercheck = $wpdb->prefix . "prayers";
	
		if ( $wpdb->get_var("SHOW TABLES LIKE '$prayercheck'") == $prayercheck ) {
			$sqltwo = "ALTER TABLE " . $prayercheck .
				" ADD mod_code char(32) DEFAULT NULL, ADD answered int(1) DEFAULT NULL";
			$wpdb->query($sqltwo);
		}
	}

// Upgrade 1.2 users to 1.4	
} elseif ( get_option('enmpe_db_version') == "1.2" ) {

	function enmpe_onefourzero() {
		global $wpdb;

		$pe_options = get_option( 'enm_prayerengine_options' );
		generate_pe_options_css($pe_options);
		
		// Define DB version
		global $enmpe_db_version;
		$enmpe_db_version = "1.6.1";
		update_option("enmpe_db_version", $enmpe_db_version);

		$prayercheck = $wpdb->prefix . "prayers";
	
		if ( $wpdb->get_var("SHOW TABLES LIKE '$prayercheck'") == $prayercheck ) {
			$sqltwo = "ALTER TABLE " . $prayercheck .
				" ADD mod_code char(32) DEFAULT NULL, ADD answered int(1) DEFAULT NULL";
			$wpdb->query($sqltwo);
		}

		$countcheck = $wpdb->prefix . "pe_prayerwalls";
	
		if ( $wpdb->get_var("SHOW TABLES LIKE '$countcheck'") == $countcheck ) {
			$sqlthree = "ALTER TABLE " . $countcheck .
				" ADD enablecount int(1) DEFAULT NULL";
			$wpdb->query($sqlthree);
		}
	}
		

	if (function_exists('is_multisite') && is_multisite()) { // Check for Multisite
		global $wpdb;
		$blog_list = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs",""));
		foreach ($blog_list as $blog) {
			switch_to_blog($blog);
			$data = get_option( 'enm_prayerengine_options' ); ;	
			$css_dir = plugin_dir_path( __FILE__ ) . 'css/'; // Shorten code, save 1 call
			ob_start(); // Capture all output (output buffering)
			include($css_dir . 'pe_styles_generate.php'); // Generate CSS
			$css = ob_get_clean(); // Get generated CSS (output buffering)
			file_put_contents($css_dir . 'pe_' . $blog . '_styles.css', $css, LOCK_EX); // Save it

			// Define DB version
			global $enmpe_db_version;
			$enmpe_db_version = "1.6.1";
			update_option("enmpe_db_version", $enmpe_db_version);	

			$prayercheck = $wpdb->prefix . "prayers";
	
			if ( $wpdb->get_var("SHOW TABLES LIKE '$prayercheck'") == $prayercheck ) {
				$sqltwo = "ALTER TABLE " . $prayercheck .
					" ADD mod_code char(32) DEFAULT NULL, ADD answered int(1) DEFAULT NULL";
				$wpdb->query($sqltwo);
			}

			$countcheck = $wpdb->prefix . "pe_prayerwalls";
	
			if ( $wpdb->get_var("SHOW TABLES LIKE '$countcheck'") == $countcheck ) {
				$sqlthree = "ALTER TABLE " . $countcheck .
					" ADD enablecount int(1) DEFAULT NULL";
				$wpdb->query($sqlthree);
			}
		}
		switch_to_blog($wpdb->blogid);
	} else {
		 enmpe_onefourzero();
	}

} elseif ( get_option('enmpe_db_version') == "1.4" ) { // Upgrade users to 1.5
	function enmpe_onefivezero() {
		global $wpdb;

		$pe_options = get_option( 'enm_prayerengine_options' );
		generate_pe_options_css($pe_options);

		// Define DB version
		global $enmpe_db_version;
		$enmpe_db_version = "1.6.1";
		update_option("enmpe_db_version", $enmpe_db_version);

		$prayercheck = $wpdb->prefix . "prayers";
	
		if ( $wpdb->get_var("SHOW TABLES LIKE '$prayercheck'") == $prayercheck ) {
			$sqltwo = "ALTER TABLE " . $prayercheck .
				" ADD answered int(1) DEFAULT NULL";
			$wpdb->query($sqltwo);
		}

		$countcheck = $wpdb->prefix . "pe_prayerwalls";
	
		if ( $wpdb->get_var("SHOW TABLES LIKE '$countcheck'") == $countcheck ) {
			$sqlthree = "ALTER TABLE " . $countcheck .
				" ADD enablecount int(1) DEFAULT NULL";
			$wpdb->query($sqlthree);
		}	
		
	}
		

	if (function_exists('is_multisite') && is_multisite()) { // Check for Multisite
		global $wpdb;
		$blog_list = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs",""));
		foreach ($blog_list as $blog) {
			switch_to_blog($blog);
			$data = get_option( 'enm_prayerengine_options' ); ;	
			$css_dir = plugin_dir_path( __FILE__ ) . 'css/'; // Shorten code, save 1 call
			ob_start(); // Capture all output (output buffering)
			include($css_dir . 'pe_styles_generate.php'); // Generate CSS
			$css = ob_get_clean(); // Get generated CSS (output buffering)
			file_put_contents($css_dir . 'pe_' . $blog . '_styles.css', $css, LOCK_EX); // Save it

			// Define DB version
			global $enmpe_db_version;
			$enmpe_db_version = "1.6.1";
			update_option("enmpe_db_version", $enmpe_db_version);	

			$prayercheck = $wpdb->prefix . "prayers";
	
			if ( $wpdb->get_var("SHOW TABLES LIKE '$prayercheck'") == $prayercheck ) {
				$sqltwo = "ALTER TABLE " . $prayercheck .
					" ADD answered int(1) DEFAULT NULL";
				$wpdb->query($sqltwo);
			}

			$countcheck = $wpdb->prefix . "pe_prayerwalls";
	
			if ( $wpdb->get_var("SHOW TABLES LIKE '$countcheck'") == $countcheck ) {
				$sqlthree = "ALTER TABLE " . $countcheck .
					" ADD enablecount int(1) DEFAULT NULL";
				$wpdb->query($sqlthree);
			}
		}
		switch_to_blog($wpdb->blogid);
	} else {
		 enmpe_onefivezero();
	}
} elseif ( get_option('enmpe_db_version') == "1.5" ) { // Upgrade users to 1.6
	function enmpe_onesixzero() {
		global $wpdb;

		$pe_options = get_option( 'enm_prayerengine_options' );
		generate_pe_options_css($pe_options);

		// Define DB version
		global $enmpe_db_version;
		$enmpe_db_version = "1.6.1";
		update_option("enmpe_db_version", $enmpe_db_version);	

		$countcheck = $wpdb->prefix . "pe_prayerwalls";
	
		if ( $wpdb->get_var("SHOW TABLES LIKE '$countcheck'") == $countcheck ) {
			$sqlthree = "ALTER TABLE " . $countcheck .
				" ADD enablecount int(1) DEFAULT NULL";
			$wpdb->query($sqlthree);
		}
	}
		

	if (function_exists('is_multisite') && is_multisite()) { // Check for Multisite
		global $wpdb;
		$blog_list = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs",""));
		foreach ($blog_list as $blog) {
			switch_to_blog($blog);
			$data = get_option( 'enm_prayerengine_options' ); ;	
			$css_dir = plugin_dir_path( __FILE__ ) . 'css/'; // Shorten code, save 1 call
			ob_start(); // Capture all output (output buffering)
			include($css_dir . 'pe_styles_generate.php'); // Generate CSS
			$css = ob_get_clean(); // Get generated CSS (output buffering)
			file_put_contents($css_dir . 'pe_' . $blog . '_styles.css', $css, LOCK_EX); // Save it

			// Define DB version
			global $enmpe_db_version;
			$enmpe_db_version = "1.6.1";
			update_option("enmpe_db_version", $enmpe_db_version);	

			$countcheck = $wpdb->prefix . "pe_prayerwalls";
	
			if ( $wpdb->get_var("SHOW TABLES LIKE '$countcheck'") == $countcheck ) {
				$sqlthree = "ALTER TABLE " . $countcheck .
					" ADD enablecount int(1) DEFAULT NULL";
				$wpdb->query($sqlthree);
			}
		}
		switch_to_blog($wpdb->blogid);
	} else {
		 enmpe_onesixzero();
	}
} elseif ( get_option('enmpe_db_version') == "1.6" ) { // Upgrade users to 1.6.1
	function enmpe_onesixone() {
		global $wpdb;

		$pe_options = get_option( 'enm_prayerengine_options' );
		generate_pe_options_css($pe_options);

		// Define DB version
		global $enmpe_db_version;
		$enmpe_db_version = "1.6.1";
		update_option("enmpe_db_version", $enmpe_db_version);	

	}
		

	if (function_exists('is_multisite') && is_multisite()) { // Check for Multisite
		global $wpdb;
		$blog_list = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs",""));
		foreach ($blog_list as $blog) {
			switch_to_blog($blog);
			$data = get_option( 'enm_prayerengine_options' ); ;	
			$css_dir = plugin_dir_path( __FILE__ ) . 'css/'; // Shorten code, save 1 call
			ob_start(); // Capture all output (output buffering)
			include($css_dir . 'pe_styles_generate.php'); // Generate CSS
			$css = ob_get_clean(); // Get generated CSS (output buffering)
			file_put_contents($css_dir . 'pe_' . $blog . '_styles.css', $css, LOCK_EX); // Save it

			// Define DB version
			global $enmpe_db_version;
			$enmpe_db_version = "1.6.1";
			update_option("enmpe_db_version", $enmpe_db_version);	

		}
		switch_to_blog($wpdb->blogid);
	} else {
		 enmpe_onesixone();
	}
}
 
?>