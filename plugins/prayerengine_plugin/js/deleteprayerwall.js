jQuery(document).ready(function() { /* ----- Series Engine - Delete a Topic ----- */
	jQuery('.prayerengine_delete').click(function() {
		var answer = confirm("Are you SURE you want to delete this Prayer Wall? All associated prayers will be deleted as well. Click 'OK' to continue deleting...")
		if (answer){
			var id = jQuery(this).attr("name");
			jQuery("#prayerengine-deleteform"+id).submit();
		};
	});
});