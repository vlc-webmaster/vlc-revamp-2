jQuery(document).ready(function() {
	jQuery('#pbg, #explorebg, #spag, #spagt, #pt, #ftbg, #ftt, #fbg, #ffbdr, #fib, #fit, #ffbg, #fft, #fsubbg, #fsubt, #pft, #ft, #pag, #prbg, #aprbg, #prn, #prt, #prd, #fbdr, #pbdr, #ermsg, #ermsgt, #sumsg, #sumsgt, #prdis, #prdist, #nchigh, #patext, #pabg, #pagt, #copyt, #fhbg, #copyh').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			jQuery(el).val(hex);
			jQuery(el).ColorPickerHide();
			var eleid = jQuery(el).attr("id");
			var ccolor = jQuery(el).attr("value");
			jQuery('#c-'+eleid).css("background-color","#"+ccolor);
		},
		onBeforeShow: function () {
			jQuery(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		jQuery(this).ColorPickerSetColor(this.value);
	});
	
	jQuery('.pe-colorfield').change(function() {
		var eleid = jQuery(this).attr("id");
		var color = jQuery(this).attr("value");
		jQuery('#c-'+eleid).css("background-color","#"+color);
	});

	jQuery('#enmpe-settings-general').click(function() {
		jQuery('#enmpe-settings-general').parent().addClass('selected');
		jQuery('#enmpe-settings-styles').parent().removeClass('selected');
		jQuery("#enmpe-style-settings").hide();
		jQuery("#enmpe-general-settings").show();
	});
	
	jQuery('#enmpe-settings-styles').click(function() { 
		jQuery('#enmpe-settings-general').parent().removeClass('selected');
		jQuery('#enmpe-settings-styles').parent().addClass('selected');
		jQuery("#enmpe-style-settings").show();
		jQuery("#enmpe-general-settings").hide();			
	});
});