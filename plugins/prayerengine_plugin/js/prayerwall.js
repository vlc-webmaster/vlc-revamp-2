/* Prayer Engine - This updates the prayer counter on individual prayer requests via AJAX */
jQuery(document).ready(function() {
	jQuery(".submitlink").click(function() {
		var prayercountform = jQuery(this).parents("form").serialize();
		var formurl = jQuery(this).parents("form").attr("action");
		var x = jQuery(this).siblings("input.id").attr("value");
		var y = Math.floor(Math.random()*1001);
		function loadrefreshednumber() {
			jQuery('#count'+x).load(formurl+'?id='+x+'&time='+y);
		};
		jQuery.post(formurl, prayercountform, loadrefreshednumber);
		return false; 
	});	
	
	jQuery("#prayerengine_twitter_ok").click(function () { //If enabled, toggle the Prayer Tweet field
		jQuery("#pe-twitter-area").slideToggle();
	});
});