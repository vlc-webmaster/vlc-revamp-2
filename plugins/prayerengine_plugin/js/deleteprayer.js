jQuery(document).ready(function() {
	jQuery('.prayerengine_delete').click(function() {
		var answer = confirm("Are you SURE you want to delete this prayer request? Click 'OK' to continue deleting this prayer request.")
		if (answer){
			var id = jQuery(this).attr("name");
			jQuery("#prayerengine-deleteform"+id).submit();
		};
	});
});