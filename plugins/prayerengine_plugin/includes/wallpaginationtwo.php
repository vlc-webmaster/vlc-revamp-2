<?php /* Prayer Engine - Pagination for Various Pages */
	if ( $wp_version != null ) {
		
		if ( !defined('ENMPE_FIND_PAGETWO') ) {
			if (!function_exists('enmpe_find_page_two')) {
				function enmpe_find_page_two() {
					define('ENMPE_FIND_PAGETWO', 'yes');
					$enmpe_get_url = parse_url( strtok( $_SERVER['REQUEST_URI'], '&' ) );
					if ( $enmpe_get_url['query'] == null ) {
						return strtok( $_SERVER['REQUEST_URI'], '&' ) . "?enmpe=1";
					} else {
						return strtok( $_SERVER['REQUEST_URI'], '&' );
					}
				}
			}
		}

		if ($enmpe_pages > 1) {
			$enmpe_thispagetwo = enmpe_find_page_two();
			echo '<div class="pe-pagination">';
			$enmpe_current_pagetwo = ($enmpe_start/$enmpe_display) + 1;
			if ($enmpe_current_pagetwo != 1) { // make previous button if not first page
				echo '<a href="' . $enmpe_thispagetwo . '&amp;enmpe_c=' . ($enmpe_start - $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '" class="previous page-numbers">&laquo;<span> Back</span></a> ';
			}
			
			if ($enmpe_pages > 11) { // Make no more than 10 links to other pages at a time.
				if ($enmpe_current_pagetwo < 6) {
					for ($i = 1; $i <= 11; $i++) { 
						if ($i != $enmpe_current_pagetwo) {
							echo '<a href="' . $enmpe_thispagetwo . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">' . $i . '</a> ';
						} else {
							echo '<span class="pe-current-page">' . $i . '</span> ';
						}
					} 
					echo '<a href="' . $enmpe_thispagetwo . '&amp;enmpe_c=' . (($enmpe_pages - 1) * $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">&hellip;' . $enmpe_pages . '</a>';
				} else {

					if ($enmpe_pages - $enmpe_current_pagetwo <= 5) {
						echo '<a href="' . $enmpe_thispagetwo . '&amp;enmpe_c=0&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">1&hellip;</a>';
						$enmpe_startpoint = $enmpe_pages - 10;
						for ($i = $enmpe_startpoint; $i <= $enmpe_pages; $i++) { 
							if ($i != $enmpe_current_pagetwo) {
								echo '<a href="' . $enmpe_thispagetwo . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">' . $i . '</a> ';
							} else {
								echo '<span class="pe-current-page">' . $i . '</span> ';
							}
						} 
						
					} else {
						if ($enmpe_current_pagetwo != 6) {
							echo '<a href="' . $enmpe_thispagetwo . '&amp;enmpe_c=0&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">1&hellip;</a>';
						}					
						$enmpe_startpoint = $current_page - 5;
						$endpoint = $current_page + 5;
						for ($i = $enmpe_startpoint; $i <= $endpoint; $i++) { 
							if ($i != $enmpe_current_pagetwo) {
								echo '<a href="' . $enmpe_thispagetwo . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">' . $i . '</a> ';
							} else {
								echo '<span class="pe-current-page">' . $i . '</span> ';
							}
						} 
						echo '<a href="' . $enmpe_thispagetwo . '&amp;enmpe_c=' . (($enmpe_pages - 1) * $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">&hellip;' . $enmpe_pages . '</a>';	
					}
				}
			} else {
				for ($i = 1; $i <= $enmpe_pages; $i++) { 
					if ($i != $enmpe_current_pagetwo) {
						echo '<a href="' . $enmpe_thispagetwo . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">' . $i . '</a> ';
					} else {
						echo '<span class="pe-current-page">' . $i . '</span> ';
					}
				} 
			}
			
			if ($enmpe_current_pagetwo != $enmpe_pages) { // make next button if not the last page
				echo '<a href="' . $enmpe_thispagetwo . '&amp;enmpe_c=' . ($enmpe_start + $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '" class="next page-numbers"><span>More </span>&raquo;</a>';
			}
	
			echo "<div style=\"clear: both;\"></div></div>\n"; 	
		}
		
		// Deny access to sneaky people!
		} else {
			exit("Access Denied");
		}
?>