<?php

if ( $wp_version != null ) {
	
$enmpe_display = $enmpe_prayerwall->displaynumber; // How many records to display

if ( $enmpe_prayerwall->displaylimit != NULL ) { // How far back should we display prayer requests?
	$enmpe_displaylimit = $enmpe_prayerwall->displaylimit;
} else {
	$enmpe_displaylimit = "nolimit";
}

if ( $enmpe_displaylimit == "3days" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 2 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "1week" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 6 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "2weeks" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 13 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "month" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "2months" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 60 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "3months" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 90 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "nolimit" ) {
	$enmpe_displaylimitsql = "";
}


$enmpe_countsql = "SELECT id FROM " . $wpdb->prefix . "prayers" . " WHERE post_this = 1 AND (share_option = 'Share Online' OR share_option = 'Share Online Anonymously') " . $enmpe_displaylimitsql . " AND wall_id " . $enmpe_swid . " ORDER BY id DESC"; 
$enmpe_runsqlcount = $wpdb->get_results( $enmpe_countsql );
$enmpe_prayercount = $wpdb->num_rows;

if (isset($_GET['enmpe_p']) && is_numeric($_GET['enmpe_p'])) { // # of pages already been determined.
	$enmpe_pages = strip_tags($_GET['enmpe_p']);
} else { // Need to determine # of pages.
	if ($enmpe_prayercount > $enmpe_display) { // More than 1 page.
		$enmpe_pages = ceil($enmpe_prayercount/$enmpe_display);
	} else {
		$enmpe_pages = 1;
	}
}

// Determine where in the database to start returning results...
if (isset($_GET['enmpe_c']) && is_numeric($_GET['enmpe_c'])) {
	$enmpe_start = strip_tags($_GET['enmpe_c']);
} else {
	$enmpe_start = 0;
}

// Get records from database according to the page and display count
$enmpe_preparredsql = "SELECT * FROM " . $wpdb->prefix . "prayers" . " WHERE post_this = 1 AND (share_option = 'Share Online' OR share_option = 'Share Online Anonymously') " . $enmpe_displaylimitsql . " AND wall_id " . $enmpe_swid . " ORDER BY id DESC LIMIT %d, %d"; 
$enmpe_sql = $wpdb->prepare( $enmpe_preparredsql, $enmpe_start, $enmpe_display );
$enmpe_prayers = $wpdb->get_results( $enmpe_sql );

// Deny access to sneaky people!
} else {
	exit("Access Denied");
}
?>