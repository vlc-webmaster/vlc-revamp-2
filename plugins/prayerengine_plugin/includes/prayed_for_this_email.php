<?php /* Prayer Engine - Notify By Email When "I Prayed For This" is Clicked */

if ( $wp_version != null ) {
	
if ($enmpe_prayer->notifyme == 1) {	
	$enmpe_prayedfor_message = "This is a quick email to let you know that someone has just prayed for your prayer request!\n\n";
	$enmpe_prayedfor_message .= "This is an automated notification sent from " . home_url() . ". Please do not respond to this email, as this account is not monitored.";
	$enmpe_prayedfor_to = $enmpe_prayer->email; 
	$enmpe_prayedfor_subject = 'Someone just prayed for you!'; 
	$enmpe_prayedfor_header = 'From: "' . $enmpe_ministry_name . '" <' . $enmpe_admin_email . '>'; 
	wp_mail( $enmpe_prayedfor_to, $enmpe_prayedfor_subject, $enmpe_prayedfor_message, $enmpe_prayedfor_header );
} 

// Deny access to sneaky people!
} else {
	exit("Access Denied");
}

?>