<?php /* Prayer Engine - Pagination for Various Pages */

	if ( $wp_version != null ) {
		
		if ( !defined('ENMPE_FIND_PAGE') ) {
			if (!function_exists('enmpe_find_page')) {
				function enmpe_find_page() {
					define('ENMPE_FIND_PAGE', 'yes');
					$enmpe_get_url = parse_url( strtok( $_SERVER['REQUEST_URI'], '&' ) );
					if ( $enmpe_get_url['query'] == null ) {
						return strtok( $_SERVER['REQUEST_URI'], '&' ) . "?enmpe=1";
					} else {
						return strtok( $_SERVER['REQUEST_URI'], '&' );
					}
				}
			}
		}
		
		if ($enmpe_pages > 1) {
			$enmpe_thispage = enmpe_find_page();
			echo '<div class="pe-pagination">';
			$enmpe_current_page = ($enmpe_start/$enmpe_display) + 1;
			if ($enmpe_current_page != 1) { // make previous button if not first page
				echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . ($enmpe_start - $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '">&laquo; Previous</a> ';
			}
			
			if ($enmpe_pages > 11) { // Make no more than 10 links to other pages at a time.
				if ($enmpe_current_page < 6) {
					for ($i = 1; $i <= 11; $i++) { 
						if ($i != $enmpe_current_page) {
							echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">' . $i . '</a> ';
						} else {
							echo '<span class="pe-current-page">' . $i . '</span> ';
						}
					} 
					echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_pages - 1) * $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">&hellip;' . $enmpe_pages . '</a>';
				} else {

					if ($enmpe_pages - $enmpe_current_page <= 5) {
						echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=0&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">1&hellip;</a>';
						$enmpe_startpoint = $enmpe_pages - 10;
						for ($i = $enmpe_startpoint; $i <= $enmpe_pages; $i++) { 
							if ($i != $enmpe_current_page) {
								echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">' . $i . '</a> ';
							} else {
								echo '<span class="pe-current-page">' . $i . '</span> ';
							}
						} 
						
					} else {
						if ($enmpe_current_page != 6) {
							echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=0&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">1&hellip;</a>';
						}					
						$enmpe_startpoint = $enmpe_current_page - 5;
						$endpoint = $enmpe_current_page + 5;
						for ($i = $enmpe_startpoint; $i <= $endpoint; $i++) { 
							if ($i != $enmpe_current_page) {
								echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">' . $i . '</a> ';
							} else {
								echo '<span class="pe-current-page">' . $i . '</span> ';
							}
						} 
						echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_pages - 1) * $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">&hellip;' . $enmpe_pages . '</a>';	
					}
				}
			} else {
				for ($i = 1; $i <= $enmpe_pages; $i++) { 
					if ($i != $enmpe_current_page) {
						echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="pepnumber">' . $i . '</a> ';
					} else {
						echo '<span class="pe-current-page">' . $i . '</span> ';
					}
				} 
			}
			
			if ($enmpe_current_page != $enmpe_pages) { // make next button if not the last page
				echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . ($enmpe_start + $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '">Next &raquo;</a>';
			}
	
			echo "<div style=\"clear: both;\"></div></div>\n"; 	
		}
		
		// Deny access to sneaky people!
		} else {
			exit("Access Denied");
		}
?>