<?php /* Prayer Engine - Update Prayer Count on Main Prayer Wall */
	require '../../../../wp-blog-header.php'; // ADJUST THIS PATH if using a non-standard WordPress install
	header('HTTP/1.1 200 OK');
	
	$enmpe_options = get_option( 'enm_prayerengine_options' ); 
	$enmpe_admin_email = $enmpe_options['robotemail'];
	$enmpe_ministry_name = $enmpe_options['ministryname'];
	
	global $wpdb;
	
	if ($_POST) {
		$enmpe_id = strip_tags($_POST['id']);
		$enmpe_prayercount = strip_tags($_POST['prayer_count']);
		
		if ( wp_verify_nonce( $_POST['enmpe_iprayed' . $enmpe_id], 'iprayed' . $enmpe_id ) ) {
			//update the prayer count
			$enmpe_new_values = array( 'prayer_count' => $enmpe_prayercount ); 
			$enmpe_where = array( 'id' => $enmpe_id ); 
			$wpdb->update( $wpdb->prefix . "prayers", $enmpe_new_values, $enmpe_where ); 
		}
		
		$enmpe_findprayer_preparred = "SELECT * FROM " . $wpdb->prefix . "prayers" . " WHERE id = %d"; 
		$enmpe_findprayer = $wpdb->prepare( $enmpe_findprayer_preparred, $enmpe_id );
		$enmpe_prayer = $wpdb->get_row( $enmpe_findprayer, OBJECT );
		
		//Send a notification email if they signed up for one.
		include ('prayed_for_this_email.php');
	} else {
		//get specified prayer request
		$enmpe_id = $_GET['id'];
		$enmpe_findprayer_preparred = "SELECT * FROM " . $wpdb->prefix . "prayers" . " WHERE id = %d"; 
		$enmpe_findprayer = $wpdb->prepare( $enmpe_findprayer_preparred, $enmpe_id );
		$enmpe_prayer = $wpdb->get_row( $enmpe_findprayer, OBJECT );
	};
?>
<blockquote>Thanks for Praying</blockquote>
<?php if ($enmpe_prayer->prayer_count == 1) { ?>
<h4>Prayed for <strong><?php echo number_format($enmpe_prayer->prayer_count, 0, '.', ','); ?></strong> time.</h4>
<?php } elseif ($enmpe_prayer->prayer_count > 1) { ?>
<h4>Prayed for <strong><?php echo number_format($enmpe_prayer->prayer_count, 0, '.', ','); ?></strong> times.</h4>
<?php } else {} ?>