<?php /* Prayer Engine - Prayer Wall */

global $wpdb;
global $wp_version;

if ( $wp_version != null ) {

	if ( $enmpe_s > 0 ) { // Find the relevant prayer wall
		if ( $enmpe_sw == 0 ) {
			$enmpe_findtheprayerwallsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " WHERE pwid = %d"; 
			$enmpe_findtheprayerwall = $wpdb->prepare( $enmpe_findtheprayerwallsql, 1 );
			$enmpe_prayerwall = $wpdb->get_row( $enmpe_findtheprayerwall, OBJECT );
		} else {
			$enmpe_findtheprayerwallsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " WHERE pwid = %d"; 
			$enmpe_findtheprayerwall = $wpdb->prepare( $enmpe_findtheprayerwallsql, $enmpe_sw );
			$enmpe_prayerwall = $wpdb->get_row( $enmpe_findtheprayerwall, OBJECT );
		}
	} else {
		$enmpe_findtheprayerwallsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " WHERE pwid = %d"; 
		$enmpe_findtheprayerwall = $wpdb->prepare( $enmpe_findtheprayerwallsql, 1 );
		$enmpe_prayerwall = $wpdb->get_row( $enmpe_findtheprayerwall, OBJECT );
	}

$enmpe_options = get_option( 'enm_prayerengine_options' ); 
$enmpe_twitter = $enmpe_prayerwall->enabletwitter;

if ( $enmpe_prayerwall->enablecount == null ) {
	$enmpe_count = 1;
} else {
	$enmpe_count = $enmpe_prayerwall->enablecount;
}

if ( $enmpe_options['spamprotection'] != NULL ) {
	$enmpe_spamprotection = $enmpe_options['spamprotection'];
} else {
	$enmpe_spamprotection = 0;
}

if ( isset($enmpe_options['credits']) ) {
	$enmpe_credits = $enmpe_options['credits'];
} else {
	$enmpe_credits = "light";
}

$enmpe_moderation = $enmpe_prayerwall->moderation;
$enmpe_instructions = $enmpe_prayerwall->wall_instructions;
$enmpe_success = $enmpe_prayerwall->wall_success;
$enmpe_feedtitle = $enmpe_options['feedtitle'];
$enmpe_admin_email = $enmpe_options['robotemail'];
$enmpe_ministry_name = $enmpe_options['ministryname'];
$enmpe_ft = $enmpe_options['ft'];

if ( !defined('MARKDOWN_VERSION') ) {
	include('admin/markdown.php');
}

$enmpe_was_deleted = 0;

if ( $enmpe_s > 0 ) {
	if ( $enmpe_sw == 0 ) {
		$enmpe_swid = ">= 1";
		$enmpe_wallid = 1;
	} else {
		$enmpe_swid = "= " . $enmpe_sw;
		$enmpe_wallid = $enmpe_sw;
	}
} else {
	$enmpe_swid = "= 1";
	$enmpe_wallid = 1;
}

if ( isset($_GET['enmpe_e']) && isset($_GET['enmpe_dc']) ) { // Delete request if delete link supplied
	
	// Validate email and encrypted validation code:
	$enmpe_e = $enmpe_dc = FALSE;
	if (isset($_GET['enmpe_e']) && preg_match('/^[\w.-]+@[\w.-]+\.[A-Za-z]{2,8}$/', $_GET['enmpe_e']) ) { 
		$enmpe_e = strip_tags($_GET['enmpe_e']);
	}
	
	if (isset($_GET['enmpe_dc']) && (strlen($_GET['enmpe_dc']) == 32 ) ) { 
		$enmpe_dc = strip_tags($_GET['enmpe_dc']);
	}

	if ($enmpe_e && $enmpe_dc) {
		
		$enmpe_checkcombo_preparred = "SELECT id FROM " . $wpdb->prefix . "prayers" . " WHERE (email=%s AND delete_code=%s) LIMIT 1"; 
		$enmpe_checkcombo = $wpdb->prepare( $enmpe_checkcombo_preparred, $enmpe_e, $enmpe_dc );
		$enmpe_prayer = $wpdb->get_row( $enmpe_checkcombo, OBJECT );
		$enmpe_returnedprayer = $wpdb->num_rows;

		if ($enmpe_returnedprayer == 1) { // Available for deletion.
			$enmpe_delete_query_preparred = "DELETE FROM " . $wpdb->prefix . "prayers" . " WHERE id=%d";
			$enmpe_delete_query = $wpdb->prepare( $enmpe_delete_query_preparred, $enmpe_prayer->id ); 
			$enmpe_deleted = $wpdb->query( $enmpe_delete_query );
			$enmpe_was_deleted = 1;
		}
	}
}

if ( isset($_GET['enmpe_e']) && isset($_GET['enmpe_ac']) ) { // Mark Request as Answered
	
	// Validate email and encrypted validation code:
	$enmpe_e = $enmpe_dc = FALSE;
	if (isset($_GET['enmpe_e']) && preg_match('/^[\w.-]+@[\w.-]+\.[A-Za-z]{2,8}$/', $_GET['enmpe_e']) ) { 
		$enmpe_e = strip_tags($_GET['enmpe_e']);
	}
	
	if (isset($_GET['enmpe_ac']) && (strlen($_GET['enmpe_ac']) == 32 ) ) { 
		$enmpe_dc = strip_tags($_GET['enmpe_ac']);
	}

	if ($enmpe_e && $enmpe_dc) {
		
		$enmpe_checkcombo_preparred = "SELECT id FROM " . $wpdb->prefix . "prayers" . " WHERE (email=%s AND delete_code=%s) LIMIT 1"; 
		$enmpe_checkcombo = $wpdb->prepare( $enmpe_checkcombo_preparred, $enmpe_e, $enmpe_dc );
		$enmpe_prayer = $wpdb->get_row( $enmpe_checkcombo, OBJECT );
		$enmpe_returnedprayer = $wpdb->num_rows;
		$enmpe_was_answered = 0;

		if ($enmpe_returnedprayer == 1) { // Available to edit
			$enmpe_new_values = array( 'answered' => 1 ); 
			$enmpe_where = array( 'id' => $enmpe_prayer->id ); 
			$wpdb->update( $wpdb->prefix . "prayers", $enmpe_new_values, $enmpe_where ); 
			$enmpe_was_answered = 1;
		}
	}
}

if ( isset($_GET['enmpe_id']) && isset($_GET['enmpe_mc']) ) { // Approve Request if Correct Criteria is Supplied
	
	// Validate email and encrypted validation code:
	$enmpe_id = $enmpe_mc = FALSE;
	if (isset($_GET['enmpe_id']) && is_numeric($_GET['enmpe_id']) ) { 
		$enmpe_id = strip_tags($_GET['enmpe_id']);
	}
	
	if (isset($_GET['enmpe_mc']) && (strlen($_GET['enmpe_mc']) == 32 ) ) { 
		$enmpe_mc = strip_tags($_GET['enmpe_mc']);
	}

	if ($enmpe_id && $enmpe_mc) {
		
		$enmpe_checkcombo_preparred = "SELECT id, prayer FROM " . $wpdb->prefix . "prayers" . " WHERE (id=%s AND mod_code=%s AND post_this=0 ) LIMIT 1"; 
		$enmpe_checkcombo = $wpdb->prepare( $enmpe_checkcombo_preparred, $enmpe_id, $enmpe_mc );
		$enmpe_prayer = $wpdb->get_row( $enmpe_checkcombo, OBJECT );
		$enmpe_returnedprayer = $wpdb->num_rows;
		$enmpe_can_moderate = 0;

		if ($enmpe_returnedprayer == 1) { // Available for moderation.
			$enmpe_new_values = array( 'post_this' => 1, 'brand_new' => 0, 'moderated_by' => 'Instant Post', 'posted_prayer' => $enmpe_prayer->prayer ); 
			$enmpe_where = array( 'id' => $enmpe_prayer->id ); 
			$wpdb->update( $wpdb->prefix . "prayers", $enmpe_new_values, $enmpe_where ); 
			$enmpe_can_moderate = 1;
		}
	}
}

if ($_POST) { // Prayer form submitted
	$enmpe_errors = array(); //Set up errors array
	
	if ( wp_verify_nonce( $_POST['enmpe_nonce'], 'prayersubmit' ) ) {
		
		if ( $enmpe_spamprotection == 1 ) { // Optional reCAPTCHA spam protection
			require_once('recaptchalib.php');
			$privatekey = "6Lek7dMSAAAAAIBoaEfh0smBj2zZI9oVG2pSnpjZ";
			$resp = recaptcha_check_answer ($privatekey,
										  $_SERVER["REMOTE_ADDR"],
										  $_POST["recaptcha_challenge_field"],
										  $_POST["recaptcha_response_field"]);
			
			if (!$resp->is_valid) {
			  $enmpe_errors[] = 'Please prove you\'re not a spam robot.';
			}
		}
		
		//Get all info from the form and validate some of it
	
		$enmpe_name = strip_tags($_POST['prayerengine_name']);
	
		if (empty($_POST['prayerengine_email'])) { //validate presence and format of email
			$enmpe_errors[] = 'An email address is required to submit a prayer request.';
		} else {
			if (preg_match('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,8})$^', $_POST['prayerengine_email'])) { 
				$enmpe_email = $_POST['prayerengine_email'];
			} else {
				$enmpe_errors[] = 'You must provide a valid email address.';
			};
		};
	
		$enmpe_phone = strip_tags($_POST['prayerengine_phone']);
	
		$enmpe_shareoption = strip_tags($_POST['prayerengine_desired_share_option']);		
	
	
		if (empty($_POST['prayerengine_prayer']) || strlen($_POST['prayerengine_prayer']) < 10) { //validate presence and length of prayer
			$enmpe_errors[] = 'Please enter a prayer request.';
		} else {
			if (preg_match('/(href=)/', $_POST['prayerengine_prayer']) || preg_match('/(HREF=)/', $_POST['prayerengine_prayer'])) { // Simple check for spam hyperlinks
				$enmpe_errors[] = 'Sorry, no HTML is allowed in your prayer request.';
			} else {
				$enmpe_prayer = strip_tags($_POST['prayerengine_prayer']);
			}
		};
	
		if (isset($_POST['prayerengine_notifyme'])) { //email notification
			$enmpe_notifyme = strip_tags($_POST['prayerengine_notifyme']);
		} else {
			$enmpe_notifyme = 0;
		}
	
		if (isset($_POST['prayerengine_twitter_ok'])) { //add to twitter prayer feed
			$enmpe_twitterok = strip_tags($_POST['prayerengine_twitter_ok']);
		} else {
			$enmpe_twitterok = 0;
		}
	
		if (isset($_POST['prayerengine_twitter_ok'])) { //validate prayer tweet
			if (!empty($_POST['prayerengine_prayer_tweet'])) { 
				if (preg_match('/(href=)/', $_POST['prayerengine_prayer_tweet']) || preg_match('/(HREF=)/', $_POST['prayerengine_prayer_tweet'])) { // Simple check for spam hyperlinks
					$enmpe_errors[] = 'Sorry, no HTML is allowed in your prayer tweet.';
				} else {
					$enmpe_prayertweet = strip_tags(substr($_POST['prayerengine_prayer_tweet'],0,140));
				}
			} else {
				$enmpe_errors[] = 'Please enter a prayer tweet.';
			};
		} else {
			$enmpe_prayertweet = NULL;
		}
	
		$enmpe_datereceived = strip_tags($_POST['prayerengine_date_received']);
		$enmpe_prayercount = strip_tags($_POST['prayerengine_prayer_count']);
		$enmpe_brandnew = strip_tags($_POST['prayerengine_brand_new']);
		$enmpe_postthis = strip_tags($_POST['prayerengine_post_this']);
	
		$enmpe_dc = md5(uniqid(rand(), true));
		$enmpe_mc = md5(uniqid(rand(), true));
		$enmpe_wall_id = $enmpe_wallid;
	} else {
		$enmpe_errors[] = 'Oops! A server error occured. Try submitting your request again, and it should go through.';
	}
	
	if (empty($enmpe_errors)) { /* IF THERE AREN'T ANY ERRORS, WRITE IT TO THE DB */
		//Submit Prayer Request
		if ($enmpe_moderation == 0) {
			$enmpe_newprayer = array(
				'name' => $enmpe_name, 
				'email' => $enmpe_email,
				'phone' => $enmpe_phone,
				'desired_share_option' => $enmpe_shareoption,
				'prayer' => $enmpe_prayer,
				'posted_prayer' => $enmpe_prayer,
				'date_received' => $enmpe_datereceived,
				'prayer_count' => $enmpe_prayercount,
				'brand_new' => $enmpe_brandnew,
				'share_option' => $enmpe_shareoption,
				'post_this' => 1,
				'notifyme' => $enmpe_notifyme,
				'twitter_ok' => $enmpe_twitterok,
				'prayer_tweet' => $enmpe_prayertweet,
				'delete_code' => $enmpe_dc,
				'mod_code' => $enmpe_mc,
				'wall_id' => $enmpe_wall_id,
				'moderated_by' => 'Not Moderated'
			);			
		} else {
			$enmpe_newprayer = array(
				'name' => $enmpe_name, 
				'email' => $enmpe_email,
				'phone' => $enmpe_phone,
				'desired_share_option' => $enmpe_shareoption,
				'prayer' => $enmpe_prayer,
				'date_received' => $enmpe_datereceived,
				'prayer_count' => $enmpe_prayercount,
				'brand_new' => $enmpe_brandnew,
				'share_option' => $enmpe_shareoption,
				'post_this' => $enmpe_postthis,
				'notifyme' => $enmpe_notifyme,
				'twitter_ok' => $enmpe_twitterok,
				'prayer_tweet' => $enmpe_prayertweet,
				'wall_id' => $enmpe_wall_id,
				'delete_code' => $enmpe_dc,
				'mod_code' => $enmpe_mc
			);			
		}

		$wpdb->insert( $wpdb->prefix . "prayers", $enmpe_newprayer );
				
		// Send 'Request Received' email with delete link
		include ('prayer_received_email.php');
		//Send emails to specific admins if enabled
		include ('email_notification.php');
	}
}

include ('paginated_prayer_requests.php');

?>
<div id="prayerengine">
	<!-- PRAYER ENGINE PRAYER WALL - EDIT AT YOUR OWN RISK! -->	
		<div id="pe-functions">
			<?php include ('form_state_options.php'); ?>
			<?php if ( $enmpe_was_deleted == 1 ) { ?>
			<script type="text/javascript">
				jQuery(document).ready(function() {
					alert('Your prayer request has been permanently deleted.');
				});
			</script>
			<?php } ?>
			<?php if ( isset($enmpe_can_moderate) && $enmpe_can_moderate == 1 ) { ?>
			<script type="text/javascript">
				jQuery(document).ready(function() {
					alert('This request has been instantly approved and posted to the wall!');
				});
			</script>
			<?php } elseif ( isset($enmpe_can_moderate) && $enmpe_can_moderate == 0 ) { ?>
			<script type="text/javascript">
				jQuery(document).ready(function() {
					alert('It looks like someone has already moderated that request, or it no longer exists.');
				});
			</script>
			<?php } ?>
			<?php if ( isset($enmpe_was_answered) && $enmpe_was_answered == 1 ) { ?>
			<script type="text/javascript">
				jQuery(document).ready(function() {
					alert('This request has been marked as answered!');
				});
			</script>
			<?php } elseif ( isset($enmpe_was_answered) && $enmpe_was_answered == 0 ) { ?>
			<script type="text/javascript">
				jQuery(document).ready(function() {
					alert('It looks like this prayer request no longer exists in our system.');
				});
			</script>
			<?php } ?>
		</div>
		<div id="pe-container">
			<div class="pe-explore-bar">
				<h4 class="pe-form-toggle"><a href="#" class="pe-prayer-form">Share Your Prayer Request</a></h4>
			</div>
			<!-- begin prayer request submit form -->
			<div id="pe-submit-container">
				<div id="pe-form-container" <?php if ($_POST) {echo 'style="display: block;"';} ?>>
					<?php if (!empty($enmpe_errors)) { // PHP Errors ?>
					<div id="errors">
						<p>Your prayer request cannot be submitted due to the following errors...</p>
						<ul>
						<?php foreach ($enmpe_errors as $error) {
							echo "<li>$error</li>";
						}; ?>	
						</ul>
					</div>
					<?php }; ?>
					<?php if ($_POST && empty($enmpe_errors)) { // Success Message ?>
					<div id="success">
						<p><?php echo stripslashes($enmpe_success); ?></p>
					</div>
					<?php }; ?>
					<p class="instructions"><?php echo stripslashes($enmpe_instructions); ?></p>
					<?php if ( $enmpe_spamprotection == 1 ) { // Optional reCAPTCHA spam protection ?>
					<script type="text/javascript">var RecaptchaOptions = {theme : 'custom',  lang: 'en', custom_theme_widget: 'perecaptcha_widget'};</script>
					 <?php } ?>
					<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="prayerform">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="label"><label for="prayerengine_name">Your Name:</label></td>
								<td class="inputcell"><input type="text" name="prayerengine_name" value="<?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_name'];}; ?>" id="prayerengine_name" tabindex="1" /></td>
							</tr>
							<tr>
								<td class="label"><label for="prayerengine_email">Your Email:</label></td>
								<td class="inputcell"><input type="text" name="prayerengine_email" value="<?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_email'];}; ?>" id="prayerengine_email" tabindex="2" /></td>
							</tr>
							<tr>
								<td class="label"><label for="prayerengine_phone">Your Phone:</label></td>
								<td class="inputcell"><input type="text" name="prayerengine_phone" value="<?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_phone'];}; ?>" id="prayerengine_phone" tabindex="3" /></td>
							</tr>
							<tr>
								<td class="label dropdown"><label for="prayerengine_desired_share_option">Please...</label></td>
								<td class="optioncell">
									<select name="prayerengine_desired_share_option" id="prayerengine_desired_share_option" tabindex="4">
										<option value="Share Online" <?php if ($_POST && !empty($enmpe_errors) && ($_POST['prayerengine_desired_share_option'] == "Share Online")) {?>selected="selected"<?php }; ?>>Share This</option>
										<option value="Share Online Anonymously" <?php if ($_POST && !empty($enmpe_errors) && ($_POST['prayerengine_desired_share_option'] == "Share Online Anonymously")) {?>selected="selected"<?php }; ?>>Share This Anonymously</option>
										<option value="DO NOT Share Online" <?php if ($_POST && !empty($enmpe_errors) && ($_POST['prayerengine_desired_share_option'] == "DO NOT Share Online")) {?>selected="selected"<?php }; ?>>DO NOT Share This</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="label"><label for="prayerengine_prayer">Your Prayer Request:</label></td>
								<td class="prayercell">
									<textarea name="prayerengine_prayer" rows="10" cols="50" id="prayerengine_prayer" tabindex="5"><?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_prayer'];}; ?></textarea>
								</td>
							</tr>
							<tr>
								<td class="label empty"></td>
								<td class="optionarea">
									<?php if ( $enmpe_count == 1 ) { ?><input name="prayerengine_notifyme" id="prayerengine_notifyme" type="checkbox" value="1" class="check" tabindex="6" <?php if ($_POST && !empty($enmpe_errors) && isset($_POST['prayerengine_notifyme'])) {echo 'checked="checked"';} ?> /> <label for="prayerengine_notifyme" class="checkbox">Email Me When Someone Prays</label><br /><?php } ?>
									<?php if ($enmpe_twitter == 1) { // If Prayer Tweets enabled ?>
									<input name="prayerengine_twitter_ok" id="prayerengine_twitter_ok" type="checkbox" value="1" class="check" tabindex="7" <?php if ($_POST && !empty($enmpe_errors) && isset($_POST['prayerengine_twitter_ok'])) {echo 'checked="checked"';} ?> /> <label for="prayerengine_twitter_ok" class="checkbox">It's Okay to Share This on Twitter</label>
									<?php } ?>
								</td>
							</tr>
						</table>
						<?php if ($enmpe_twitter == 1) { // Take Prayer Tweets if Enabled ?>
						<div id="pe-twitter-area">
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td class="label"><label for="prayerengine_prayer_tweet">Prayer Tweet:</label></td>
									<td class="twittercell">
										<textarea name="prayerengine_prayer_tweet" rows="10" cols="50" id="prayerengine_prayer_tweet" tabindex="8"><?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_prayer_tweet'];} else { ?>Please list a shorter version of your request for Twitter.<?php } ?></textarea>
										<p class="twitter-counter"><span id="counter">0</span>&nbsp;characters remaining.</p>
									</td>
								</tr>
							</table>
						</div>
						<?php } ?>
						<?php if ( $enmpe_spamprotection == 1 ) { // Optional reCAPTCHA spam protection ?>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="label spam"><label for="prayerengine_prayer">Spam Protection:</label></td>
								<td class="spamcell">
									<div id="perecaptcha_widget">
										<div id="recaptcha_image"></div>
   										<div class="recaptcha_only_if_incorrect_sol" style="color:red">Incorrect please try again</div>
										<span class="recaptcha_only_if_image">Enter the words above:</span>
   										<span class="recaptcha_only_if_audio">Enter the numbers you hear:</span>
   										<input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
										<span class="perecaptcha_options">reCAPTCHA Options:</span>
										<div style="display: inline" class="perecaptcha_link"><a href="javascript:Recaptcha.reload()" style="color: #<?php echo $enmpe_ft; ?>">Reload</a></div>
   										<div class="recaptcha_only_if_image" style="display: inline"><a href="javascript:Recaptcha.switch_type('audio')" style="color: #<?php echo $enmpe_ft; ?>">Audio Version</a></div>
   										<div class="recaptcha_only_if_audio" style="display: inline"><a href="javascript:Recaptcha.switch_type('image')" style="color: #<?php echo $enmpe_ft; ?>">Image Version</a></div>
   										<div style="display: inline" class="perecaptcha_link"><a href="javascript:Recaptcha.showhelp()" style="color: #<?php echo $enmpe_ft; ?>">Help</a></div>
									</div>
									<script type="text/javascript" src="http://www.google.com/recaptcha/api/challenge?k=6Lek7dMSAAAAABeodLWvWpzzNU6V2m_NlkZx-1V_"></script>
 									<noscript>
   										<iframe src="http://www.google.com/recaptcha/api/noscript?k=6Lek7dMSAAAAABeodLWvWpzzNU6V2m_NlkZx-1V_" height="300" width="500" frameborder="0"></iframe><br>
   										<textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
   										<input type="hidden" name="recaptcha_response_field" value="manual_challenge">
 									</noscript> 
									<?php
									  require_once('recaptchalib.php');
									  $publickey = "6Lek7dMSAAAAABeodLWvWpzzNU6V2m_NlkZx-1V_";
									  echo recaptcha_get_html($publickey);
									?>
								</td>
							</tr>
						</table>
						<?php } ?>
						<div id="pe-submit-area">
							<input type="hidden" name="prayerengine_date_received" value="<?php echo date("Y-m-d H:i:s", time()) ?>" id="prayerengine_date_received" />
							<input type="hidden" name="prayerengine_prayer_count" value="0" id="prayerengine_prayer_count" />
							<input type="hidden" name="prayerengine_brand_new" value="1" id="prayerengine_brand_new" />
							<input type="hidden" name="prayerengine_post_this" value="0" id="prayerengine_post_this" />
							<?php wp_nonce_field("prayersubmit","enmpe_nonce"); ?>
							<input type="submit" value="Submit Request" class="pe-submit" tabindex="9" />
						</div>
					</form>
				</div>
			</div>
			<div class="ie6-spacer"></div>
			<!-- end prayer request submit form -->	
			<!-- list of prayer requests -->
			<div id="pe-prayer-list">
			<?php $rowcycle = 'even'; // Alternate odd and even rows from database
				foreach ($enmpe_prayers as $prayer) { 
					if ($rowcycle == 'even') {
						$rowcycle = 'odd';
					} else {
						$rowcycle = 'even';	
					}
			?>
				<div class="pe-prayer-<?php echo $rowcycle; ?>" id="prayer<?php echo $prayer->id ?>">
					<?php if ($prayer->answered == 1) { ?>
					<h4 class="answered"><span>This prayer has been </span>answered!</h4>
					<?php } ?>
					<?php if ( $enmpe_count == 1 ) { ?>
					<div class="pe-count-area" id="count<?php echo $prayer->id ?>">
						<form action="<?php echo plugins_url() .'/prayerengine_plugin/includes/updateprayer.php'; ?>" id="form<?php echo $prayer->id ?>" method="post">
							<?php if ( $prayer->answered == 0 || $prayer->answered == null ) { ?><a href="#" class="submitlink">I prayed for this</a><?php } ?>
							<?php if ($prayer->prayer_count == 1) { ?>
							<h4>Prayed for <?php echo number_format($prayer->prayer_count, 0, '.', ','); ?> time.</h4>
							<?php } elseif ($prayer->prayer_count > 1) { ?>
							<h4>Prayed for <?php echo number_format($prayer->prayer_count, 0, '.', ','); ?> times.</h4>
							<?php } else {} ?>
							<?php $newcount = $prayer->prayer_count + 1 ?>
							<input type="hidden" name="prayer_count" value="<?php echo $newcount ?>"  />
							<input type="hidden" name="id" value="<?php echo $prayer->id ?>" class="id" />
							<?php wp_nonce_field( "iprayed" . $prayer->id,"enmpe_iprayed" . $prayer->id ); ?>
						</form>
					</div>
					<?php } ?>
					<h3 class="pe-name"><?php if ($prayer->share_option == "Share Online") {echo stripslashes($prayer->name);} else {echo "Anonymous";}; if (($prayer->share_option == "Share Online") && (strlen($prayer->name) == 0)) {echo "Anonymous";} ?></h3>
					<?php echo Markdown(stripslashes($prayer->posted_prayer)); ?><h4>Received: <?php echo date('F j, Y', strtotime($prayer->date_received)) ?></h4>
				</div>
			<?php } ?>
			</div>
			<?php include 'wallpaginationtwo.php'; //Paginate Prayer Requests ?>
			<?php if ( $enmpe_credits != "text" ) { ?>
			<h3 class="enmpe-poweredby"><a href="http://www.theprayerengine.com" target="_blank">Powered by Prayer Engine</a></h3>
			<?php } else { ?>
			<p class="enmpe-poweredbytext">Powered by <a href="http://www.theprayerengine.com" target="_blank">Prayer Engine</a></p>
			<?php } ?>
			<!-- Version 1.6.1 11.11.14 -->
		</div>
</div>
<?php // Deny access to sneaky people!
} else {
	exit("Access Denied");
} ?>