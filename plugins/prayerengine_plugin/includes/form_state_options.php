<?php /* Prayer Engine - Handles Display of Elements During Form Submit Process */
if ($enmpe_twitter == 1) { ?>
<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/jquery.limit.js'; ?>"></script>
<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/prayertweet.js'; ?>"></script>
<?php } ?> 
<?php if ($_POST) { ?>
<script type="text/javascript">
		jQuery(document).ready(function() {
		jQuery(".pe-prayer-form").toggle( //If JavaScript is on, toggle the visibility and text of the Prayer Request form
			function () { 
				jQuery("#pe-form-container").slideUp(500);
			},
			function () {
				jQuery("#pe-form-container").slideDown(500);
			}
		);
		});
</script>
<?php } else { //Hide form unless it has been posted ?> 
<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery(".pe-prayer-form").toggle( //If JavaScript is on, toggle the visibility and text of the Prayer Request form
				function () { 
					jQuery("#pe-form-container").slideDown(500);
				},
				function () {
					jQuery("#pe-form-container").slideUp(500);
				}
			);
		});
</script>
<?php } ?>
<?php if ($_POST && !empty($enmpe_errors) && isset($_POST['prayerengine_twitter_ok'])) { // Show prayer tweet if POST and checkbox selected  ?>
<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('#pe-twitter-area').show();
		});
</script>
<?php } else { // Hide prayer tweet and replace text if not selected ?>
<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('#pe-twitter-area').hide();
			jQuery('#prayerengine_prayer_tweet').focus(function() { // Replace instructions with current prayer request on click.
				var prayer = jQuery('#prayerengine_prayer').attr('value');
				jQuery(this).val(prayer);
				jQuery(this).unbind();
			});
		});
</script>
<?php } ?>