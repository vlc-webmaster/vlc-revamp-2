<?php /* Prayer Engine - Admin User Settings */
global $wp_version;
if ( $wp_version != null ) { // Verify that user is allowed to access this page
?>
<h3>The Prayer Engine</h3> 

<table class="form-table"> 
	<tr> 
		<th>
			<label for="favorite_post">Get "Prayer Received" Emails?:</label>
		</th> 
		<td>
			<input name="prayerengine_admin_email" type="checkbox" id="prayerengine_admin_email" value="1" <?php if ($enmpe_adminemail == 1) { echo 'checked="checked"'; } ?>  />
		</td>
	</tr>
</table>
<?php  // Deny access to sneaky people!
} else {
	exit("Access Denied");
} ?>