<?php /* Prayer Engine - Load a Single Admin Request */

global $wp_version;
if ( $wp_version != null ) { // Verify that user is allowed to access this page
if ( current_user_can( 'edit_posts' ) ) {
	
if ( isset($_GET['enmpe_pid']) && is_numeric($_GET['enmpe_pid']) ) {
	$enmpe_pid = strip_tags($_GET['enmpe_pid']);
}

$enmpe_findtheprayersql = "SELECT * FROM " . $wpdb->prefix . "prayers" . " WHERE id = %d"; 
$enmpe_findtheprayer = $wpdb->prepare( $enmpe_findtheprayersql, $enmpe_pid );
$enmpe_prayer = $wpdb->get_row( $enmpe_findtheprayer, OBJECT ); 

$enmpe_userdetails = wp_get_current_user(); 

} // Deny access to sneaky people!
} else {
	exit("Access Denied");
}

?>