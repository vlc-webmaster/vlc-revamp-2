<?php /* ----- Prayer Engine - Add edit and remove Prayer Walls ----- */

global $wp_version;
if ( $wp_version != null ) { // Verify that user is allowed to access this page
	if ( current_user_can( 'edit_posts' ) ) {
		global $wpdb;
		$enmpe_errors = array(); //Set up errors array
		$enmpe_messages = array(); //Set up messages array
		
		if ( $_POST && isset($_GET['enmpe_did']) ) { // If deleting a Prayer Wall
			if ( $_POST['prayerwall_delete'] > 1 ) {
				$enmpe_deleted_id = strip_tags($_POST['prayerwall_delete']);
				$enmpe_delete_query_preparred = "DELETE FROM " . $wpdb->prefix . "pe_prayerwalls" . " WHERE pwid=%d";
				$enmpe_delete_query = $wpdb->prepare( $enmpe_delete_query_preparred, $enmpe_deleted_id ); 
				$enmpe_deleted = $wpdb->query( $enmpe_delete_query ); 
			
				$enmpe_deletem_query_preparred = "DELETE FROM " . $wpdb->prefix . "prayers" . " WHERE wall_id=%d";
				$enmpe_deletem_query = $wpdb->prepare( $enmpe_deletem_query_preparred, $enmpe_deleted_id ); 
				$enmpe_deletedm = $wpdb->query( $enmpe_deletem_query ); 

				$enmpe_messages[] = "The prayer wall was successfully deleted.";
			} else {
				$enmpe_messages[] = "Sorry... You're not allowed to do that.";
			}
			
		}
		
		if ( isset($_GET['enmpe_action']) ) {
			$enmpe_prayerwall_created = null;

			if ( $_GET['enmpe_action'] == 'edit' ) { // Edit Prayer Wall
				if ( $_POST ) {
					if ( isset($_GET['enmpe_pwid']) && is_numeric($_GET['enmpe_pwid']) ) {
						$enmpe_pwid = strip_tags($_GET['enmpe_pwid']);
					}
					
					if (empty($_POST['prayerwall_name'])) { 
						if ( $enmpe_pwid == 1 ) {
							$enmpe_wall_name = "Default Prayer Wall";
						} else {
							$enmpe_errors[] = '- You must give the prayer wall a name.';
						}
					} else {
						$enmpe_wall_name = strip_tags($_POST['prayerwall_name']);
					}

					if (empty($_POST['prayerwall_instructions'])) { 
						$enmpe_errors[] = '- You must provide instructions for your prayer wall.';
					} else {
						$enmpe_instructions = strip_tags($_POST['prayerwall_instructions']);
					}

					if (empty($_POST['prayerwall_success'])) { 
						$enmpe_errors[] = '- You must provide a success message for your prayer wall.';
					} else {
						$enmpe_success = strip_tags($_POST['prayerwall_success']);
					}

					if ( isset($_POST['prayerwall_displaynumber']) && is_numeric($_POST['prayerwall_displaynumber']) ) {
						$enmpe_displaynumber = strip_tags($_POST['prayerwall_displaynumber']);
					} else {
						$enmpe_errors[] = '- You must enter a number of requests to display.';
					}

					$enmpe_displaylimit = strip_tags($_POST['prayerwall_displaylimit']);
					$enmpe_moderation = strip_tags($_POST['prayerwall_moderation']);
					$enmpe_enabletwitter = strip_tags($_POST['prayerwall_enabletwitter']);
					$enmpe_enablecount = strip_tags($_POST['prayerwall_enablecount']);
					
					if (empty($enmpe_errors)) {
						$enmpe_new_values = array( 'wall_name' => $enmpe_wall_name , 'wall_instructions' => $enmpe_instructions, 'wall_success' => $enmpe_success, 'displaynumber' => $enmpe_displaynumber, 'displaylimit' => $enmpe_displaylimit, 'moderation' => $enmpe_moderation, 'enabletwitter' => $enmpe_enabletwitter, 'enablecount' => $enmpe_enablecount  ); 
						$enmpe_where = array( 'pwid' => $enmpe_pwid ); 
						$wpdb->update( $wpdb->prefix . "pe_prayerwalls", $enmpe_new_values, $enmpe_where ); 
						$enmpe_messages[] = "Prayer Wall successfully updated!";

						$enmpe_findtheprayerwallsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " WHERE pwid = %d"; 
						$enmpe_findtheprayerwall = $wpdb->prepare( $enmpe_findtheprayerwallsql, $enmpe_pwid );
						$enmpe_prayerwall = $wpdb->get_row( $enmpe_findtheprayerwall, OBJECT );
						$enmpe_prayerwallcount = $wpdb->num_rows;
					} else {
						$enmpe_findtheprayerwallsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " WHERE pwid = %d"; 
						$enmpe_findtheprayerwall = $wpdb->prepare( $enmpe_findtheprayerwallsql, $enmpe_pwid );
						$enmpe_prayerwall = $wpdb->get_row( $enmpe_findtheprayerwall, OBJECT );
						$enmpe_prayerwallcount = $wpdb->num_rows;
					}

					
				} else {
					if ( isset($_GET['enmpe_pwid']) && is_numeric($_GET['enmpe_pwid']) ) {
						$enmpe_pwid = strip_tags($_GET['enmpe_pwid']);
					}

					$enmpe_findtheprayerwallsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " WHERE pwid = %d"; 
					$enmpe_findtheprayerwall = $wpdb->prepare( $enmpe_findtheprayerwallsql, $enmpe_pwid );
					$enmpe_prayerwall = $wpdb->get_row( $enmpe_findtheprayerwall, OBJECT );
					$enmpe_prayerwallcount = $wpdb->num_rows;
				}	
			}
			
			if ( ($_GET['enmpe_action'] == 'new' && !isset($_GET['enmpe_did']) ) && ( $_POST ) ) { // New prayer wall
				if (empty($_POST['prayerwall_name'])) { 
					$enmpe_errors[] = '- You must give the prayer wall a name.';
				} else {
					$enmpe_wall_name = strip_tags($_POST['prayerwall_name']);
				}

				if (empty($_POST['prayerwall_instructions'])) { 
						$enmpe_errors[] = '- You must provide instructions for your prayer wall.';
					} else {
						$enmpe_instructions = strip_tags($_POST['prayerwall_instructions']);
					}

					if (empty($_POST['prayerwall_success'])) { 
						$enmpe_errors[] = '- You must provide a success message for your prayer wall.';
					} else {
						$enmpe_success = strip_tags($_POST['prayerwall_success']);
					}

					if ( isset($_POST['prayerwall_displaynumber']) && is_numeric($_POST['prayerwall_displaynumber']) ) {
						$enmpe_displaynumber = strip_tags($_POST['prayerwall_displaynumber']);
					} else {
						$enmpe_errors[] = '- You must enter a number of requests to display.';
					}

					$enmpe_displaylimit = strip_tags($_POST['prayerwall_displaylimit']);
					$enmpe_moderation = strip_tags($_POST['prayerwall_moderation']);
					$enmpe_enabletwitter = strip_tags($_POST['prayerwall_enabletwitter']);
					$enmpe_enablecount = strip_tags($_POST['prayerwall_enablecount']);
				
				if (empty($enmpe_errors)) {
					$enmpe_prayerwall_created = "yes";
					
					$enmpe_newprayerwall = array(
						'wall_name' => $enmpe_wall_name,
						'wall_instructions' => $enmpe_instructions,
						'wall_success' => $enmpe_success,
						'displaynumber' => $enmpe_displaynumber,
						'displaylimit' => $enmpe_displaylimit,
						'moderation' => $enmpe_moderation,
						'enabletwitter' => $enmpe_enabletwitter,
						'enablecount' => $enmpe_enablecount
						); 
					$wpdb->insert( $wpdb->prefix . "pe_prayerwalls", $enmpe_newprayerwall );
					$enmpe_messages[] = "You have successfully added a new prayer wall to Prayer Engine!";
				}
			}
		}
		
		// Get All Prayer Walls
		$enmpe_preparredsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " ORDER BY pwid ASC"; 
		$enmpe_prayerwalls = $wpdb->get_results( $enmpe_preparredsql );
	} else {
		exit("Access Denied");
	}
	
?>
<div class="wrap"> 
<?php if ( isset($_GET['enmpe_action']) && ( $enmpe_prayerwall_created == null && !isset($_GET['enmpe_did']) ) ) { if ( $_GET['enmpe_action'] == 'new' ) { // If they're adding a new Prayer Wall ?>
		
		<h2 class="enmpe">Add a New Prayer Wall</h2>
		<?php include ('errorbox.php'); ?>
		<p>Use the form below to add a new Prayer Wall into the Prayer Engine. <a href="<?php echo admin_url() . "admin.php?page=prayerengine_plugin/prayerengine_plugin.php_userguide#pe-prayerwalls"; ?>" class="enmpe-learn-more">Learn more about Prayer Walls...</a></p>
		<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
			<table class="form-table">
				<tr valign="top">
					<th scope="row">Prayer Wall Name:</th>
					<td><input id='prayerwall_name' name='prayerwall_name' type='text' value='<?php if ($_POST && !empty($enmpe_errors)) {echo stripslashes($_POST['prayerwall_name']);} ?>' tabindex="1" /></td>
				</tr>
				<tr valign="top">
					<th scope="row">Number of Requests to Display:</th>
					<td><input id='prayerwall_displaynumber' name='prayerwall_displaynumber' type='text' value="<?php if ($_POST && !empty($enmpe_errors)) {echo stripslashes($_POST['prayerwall_displaynumber']);} else {echo '10';} ?>" tabindex="2" size="5" /></td>
				</tr>
			<tr valign="top">
				<th scope="row">Display Requests From...:</th>
				<td>
					<select id='prayerwall_displaylimit' name='prayerwall_displaylimit' tabindex="3">
						<option value='3days' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "3days") { echo 'selected="selected"';};} ?>>up to three days ago</option>
						<option value='1week' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "1week") { echo 'selected="selected"';};}  ?>>up to one week ago</option>
						<option value='2weeks' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "2weeks") { echo 'selected="selected"';};} ?>>up to two weeks ago</option>
						<option value='month' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "month") { echo 'selected="selected"';};} ?>>up to one month ago</option>
						<option value='2months' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "2months") { echo 'selected="selected"';};} ?>>up to two months ago</option>
						<option value='3months' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "3months") { echo 'selected="selected"';};} ?>>up to three months ago</option>
						<option value='nolimit' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "nolimit") { echo 'selected="selected"';};} ?>>the beginning of time</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Require Moderation?:</th>
				<td>
					<select id='prayerwall_moderation' name='prayerwall_moderation' tabindex="4">
						<option value='1' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_moderation'] == "1") { echo 'selected="selected"';};} ?>>Yes</option>
						<option value='0' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_moderation'] == "0") { echo 'selected="selected"';};} ?>>No</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Collect Prayer Tweets?:</th>
				<td>
					<select id='prayerwall_enabletwitter' name='prayerwall_enabletwitter' tabindex="5">
						<option value='1' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_enabletwitter'] == "1") { echo 'selected="selected"';};} ?>>Yes</option>
						<option value='0' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_enabletwitter'] == "0") { echo 'selected="selected"';};} ?>>No</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Enable "I Prayed for This?":</th>
				<td>
					<select id='prayerwall_enablecount' name='prayerwall_enablecount' tabindex="6">
						<option value='1' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_enablecount'] == "1") { echo 'selected="selected"';};} ?>>Yes</option>
						<option value='0' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_enablecount'] == "0") { echo 'selected="selected"';};} ?>>No</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Form Instructions:</th>
				<td>
					<textarea name="prayerwall_instructions" rows="8" cols="40" tabindex="7"><?php if ($_POST && !empty($enmpe_errors)) {echo stripslashes($_POST['prayerwall_instructions']);} else { ?>You may add your prayer request to our prayer wall using the form below. Once your prayer request is received, we will share it according to your instructions. Feel free to submit as many prayer requests as you like!<?php } ?></textarea>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Success Message:</th>
				<td>
					<textarea name="prayerwall_success" rows="8" cols="40" tabindex="8"><?php if ($_POST && !empty($enmpe_errors)) {echo stripslashes($_POST['prayerwall_success']);} else { ?>Thank you for submitting your prayer request. It is currently being moderated, and will then be shared according to your instructions. Feel free to submit another request by filling out the form again.<?php } ?></textarea>
				</td>
			</tr>
			</table>
			<p class="submit"><input name="Submit" type="submit" class="button-primary" value="Add New Prayer Wall" tabindex="9" /></p>
		</form>
		<p><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php_prayerwalls', __FILE__ ) ?>">&laquo; All Prayer Walls</a></p>
		<?php include ('pecredits.php'); ?>
<?php } elseif ( ($_GET['enmpe_action'] == 'edit') && ( $enmpe_prayerwallcount == 1 ) ) { ?>
	
	<h2 class="enmpe">Edit Prayer Wall</h2>
	<?php include ('errorbox.php'); ?>
	<?php include ('messagebox.php'); ?>
	<p>Use the form below to update the Prayer Wall within the Prayer Engine. <a href="<?php echo admin_url() . "admin.php?page=prayerengine_plugin/prayerengine_plugin.php_userguide#pe-prayerwalls"; ?>" class="enmpe-learn-more">Learn more about Prayer Walls...</a></p>
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<table class="form-table">
			<tr valign="top">
				<th scope="row">Prayer Wall Name:</th>
				<td><input id='prayerwall_name' name='prayerwall_name' type='text' value="<?php if ($_POST && !empty($enmpe_errors)) {echo stripslashes($_POST['prayerwall_name']);} else {echo stripslashes($enmpe_prayerwall->wall_name);} ?>" tabindex="1" <?php if ( $enmpe_prayerwall->pwid == 1 ) {echo 'disabled="disabled" style="opacity: 0.5"';} ?> /></td>
			</tr>
			<tr valign="top">
				<th scope="row">Number of Requests to Display:</th>
				<td><input id='prayerwall_displaynumber' name='prayerwall_displaynumber' type='text' value="<?php if ($_POST && !empty($enmpe_errors)) {echo stripslashes($_POST['prayerwall_displaynumber']);} else {echo stripslashes($enmpe_prayerwall->displaynumber);} ?>" tabindex="2" size="5" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">Display Requests From...:</th>
				<td>
					<select id='prayerwall_displaylimit' name='prayerwall_displaylimit' tabindex="3">
						<option value='3days' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "3days") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->displaylimit == "3days") { echo 'selected="selected"';};} ?>>up to three days ago</option>
						<option value='1week' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "1week") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->displaylimit == "1week") { echo 'selected="selected"';};} ?>>up to one week ago</option>
						<option value='2weeks' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "2weeks") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->displaylimit == "2weeks") { echo 'selected="selected"';};} ?>>up to two weeks ago</option>
						<option value='month' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "month") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->displaylimit == "month") { echo 'selected="selected"';};} ?>>up to one month ago</option>
						<option value='2months' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "2months") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->displaylimit == "2months") { echo 'selected="selected"';};} ?>>up to two months ago</option>
						<option value='3months' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "3months") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->displaylimit == "3months") { echo 'selected="selected"';};} ?>>up to three months ago</option>
						<option value='nolimit' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_displaylimit'] == "nolimit") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->displaylimit == "nolimit") { echo 'selected="selected"';};} ?>>the beginning of time</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Require Moderation?:</th>
				<td>
					<select id='prayerwall_moderation' name='prayerwall_moderation' tabindex="4">
						<option value='1' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_moderation'] == "1") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->moderation == "1") { echo 'selected="selected"';};} ?>>Yes</option>
						<option value='0' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_moderation'] == "0") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->moderation == "0") { echo 'selected="selected"';};} ?>>No</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Collect Prayer Tweets?:</th>
				<td>
					<select id='prayerwall_enabletwitter' name='prayerwall_enabletwitter' tabindex="5">
						<option value='1' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_enabletwitter'] == "1") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->enabletwitter == "1") { echo 'selected="selected"';};} ?>>Yes</option>
						<option value='0' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_enabletwitter'] == "0") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->enabletwitter == "0") { echo 'selected="selected"';};} ?>>No</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Enable "I Prayed for This?":</th>
				<td>
					<select id='prayerwall_enablecount' name='prayerwall_enablecount' tabindex="6">
						<option value='1' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_enablecount'] == "1") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->enablecount == "1") { echo 'selected="selected"';};} ?>>Yes</option>
						<option value='0' <?php if ($_POST && !empty($enmpe_errors)) {  if ($_POST['prayerwall_enablecount'] == "0") { echo 'selected="selected"';};} else {if ($enmpe_prayerwall->enablecount == "0") { echo 'selected="selected"';};} ?>>No</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Form Instructions:</th>
				<td>
					<textarea name="prayerwall_instructions" rows="8" cols="40" tabindex="7"><?php if ($_POST && !empty($enmpe_errors)) {echo stripslashes($_POST['prayerwall_instructions']);} else {echo stripslashes($enmpe_prayerwall->wall_instructions);} ?></textarea>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Success Message:</th>
				<td>
					<textarea name="prayerwall_success" rows="8" cols="40" tabindex="8"><?php if ($_POST && !empty($enmpe_errors)) {echo stripslashes($_POST['prayerwall_success']);} else {echo stripslashes($enmpe_prayerwall->wall_success);} ?></textarea>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">RSS Feed:</th>
				<td><p><?php echo home_url() . "/?feed=prayerengine&pwid=" . $enmpe_prayerwall->pwid; ?></p></td>
			</tr>
			<tr valign="top">
				<th scope="row">Twitter Feed (if enabled):</th>
				<td><p><?php echo home_url() . "/?feed=prayertweets&pwid=" . $enmpe_prayerwall->pwid; ?></p></td>
			</tr>

		</table>
		<p class="submit"><input name="Submit" type="submit" class="button-primary" value="Save Changes" tabindex="9" /></p>
	</form>
	<p><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php_prayerwalls', __FILE__ ); ?>">&laquo; All Prayer Walls</a></p>
	<?php include ('pecredits.php'); ?>
<?php }} else { // Display the main listing of prayer walls ?>
	<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/deleteprayerwall.js'; ?>"></script>
	
	<h2 class="enmpe">Manage Prayer Walls <a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php_prayerwalls&enmpe_action=new', __FILE__ ) ?>" class="add-new-h2">Add New</a></h2>
	<?php include ('messagebox.php'); ?>
	<p>All Prayer Walls are listed in the table below. Click on the name of the Prayer Wall to edit it. Click on the "View Prayer Requests" links to view a list of Prayer Requests associated with a Prayer Wall. You can permanently delete the Prayer Wall (and all associated Prayer Requests) from the Prayer Engine by clicking the "Delete" link. <a href="<?php echo admin_url() . "admin.php?page=prayerengine_plugin/prayerengine_plugin.php_userguide#pe-prayerwalls"; ?>" class="enmpe-learn-more">Learn more about Prayer Walls...</a></p>
	<table class="widefat" id="enmpe-topics"> 
		<thead> 
			<tr> 
				<th>Prayer Wall Name</th>
				<th>View Prayer Requests</th> 
				<th>Delete?</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ( $enmpe_prayerwalls as $enmpe_pw ) { ?>
			<tr id="row_<?php echo $enmpe_speaker->speaker_id ?>">
				<td><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php_prayerwalls&amp;enmpe_action=edit&amp;enmpe_pwid=' . $enmpe_pw->pwid, __FILE__ ); ?>"><?php echo stripslashes($enmpe_pw->wall_name); ?></a></td>				
				<td><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) . "&amp;enmpe_swid=" . $enmpe_pw->pwid ?>">View Requests from This Prayer Wall</a></td>
				<td class="enmpe-delete"><?php if ( $enmpe_pw->pwid > 1 ) { ?><form action="<?php echo $_SERVER['REQUEST_URI']; ?>&amp;enmpe_did=1" method="post" id="prayerengine-deleteform<?php echo $enmpe_pw->pwid ?>"><input type="hidden" name="prayerwall_delete" value="<?php echo $enmpe_pw->pwid ?>"></form><a href="#" class="prayerengine_delete" name="<?php echo $enmpe_pw->pwid ?>">Delete</a><?php } ?></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<?php include ('pecredits.php'); ?>	
</div>
<?php }  // Deny access to sneaky people!
} else {
	exit("Access Denied");
} ?>
