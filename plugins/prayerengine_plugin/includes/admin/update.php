<?php /* ----- Series Engine - Update Check Page ----- */

global $wp_version;
if ( $wp_version != null ) { // Verify that user is allowed to access this page
	if ( current_user_can( 'edit_posts' ) ) {						
		
		$url = "http://pluginupdates.theprayerengine.com/version.php";
		$enmpefindresponse = wp_remote_post( $url, array('method' => 'POST' ));
		$enmperesponse = unserialize( wp_remote_retrieve_body( $enmpefindresponse ) );

?>
<div class="wrap">
	<h2 class="enmpe">Check for Updates</h2>
		
		<?php 

		if ( is_wp_error( $enmperesponse ) ) {
   				echo '<h3 style="color: #afca44; font-size: 1.6em">I\'m Having Trouble Checking the Version Status...</h3>';
   				echo '<p>...but you may still see an update available on the <a href="' . admin_url( '/plugins.php', __FILE__ ) . '">Plugins page</a>.</p>';   			
		} else {
			$enmpeversionnumber = $enmperesponse->version;
   			if ( $enmpeversionnumber > ENMPE_CURRENT_VERSION ) {
   				echo '<h3 style="color: #8fbec6; font-size: 1.6em">An Update is Available! (Version ' . $enmpeversionnumber . ')</h3>';  			
   			} else {
   				echo '<h3 style="font-size: 1.6em">It Looks Like You\'re Up to Date!</h3>';
   				echo '<p>Keep an eye on our <a href="http://twitter.com/PrayerEngine">Twitter page</a> for the latest news and updates.</p>';
   			}
		}

		?>

		<p>Prayer Engine is frequently updated with new features and bug fixes.</p>
		<p>If you're not seeing Prayer Engine updates in the WordPress updater, <a href="<?php echo admin_url( '/index.php?action=prayer_engine_update', __FILE__ ); ?>">click here</a>, then check again (you may need to refresh the plugins page once or twice too). Some WordPress installs don't check our servers very often, so <a href="<?php echo admin_url( '/index.php?action=prayer_engine_update', __FILE__ ); ?>">this link</a> forces WordPress to check for the latest version.</p>
		<p><a href="http://theprayerengine.com/changelog.php" target="_blank">Click here</a> to view the changelog for previous updates.</p>	
		
		
		<?php include ('pecredits.php'); ?>	
</div>
<?php }  // Deny access to sneaky people!
} else {
	exit("Access Denied");
} ?>

