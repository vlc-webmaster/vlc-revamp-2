<?php /* Prayer Engine - Admin Prayer List and Edit Request*/

global $wp_version;
if ( $wp_version != null ) { // Verify that user is allowed to access this page
if ( current_user_can( 'edit_posts' ) ) { 

$enmpe_peoptions = get_option( 'enm_prayerengine_options' ); 

if ( !defined('MARKDOWN_VERSION') ) {
	include ('markdown.php');
}

global $wpdb;

$enmpe_errors = array(); //Set up errors array
$enmpe_messages = array(); //Set up messages array

if ( $_POST && isset($_GET['enmpe_did']) ) { // If deleting a prayer request
	$enmpe_deleted_id = strip_tags($_POST['prayerengine_delete']);
	$enmpe_delete_query_preparred = "DELETE FROM " . $wpdb->prefix . "prayers" . " WHERE id=%d";
	$enmpe_delete_query = $wpdb->prepare( $enmpe_delete_query_preparred, $enmpe_deleted_id ); 
	$enmpe_deleted = $wpdb->query( $enmpe_delete_query ); 

	if ( isset($_GET['enmpe_swid']) ) {
		$enmpe_swid = " WHERE wall_id = " . strip_tags($_GET['enmpe_swid']);
	} else {
		$enmpe_swid = "";
	}
	
	$enmpe_messages[] = "The prayer request was successfully deleted.";
	include ('paginated_prayer_requests.php'); // Get all prayer requests	

	// Get All Prayer Walls
	$enmpe_wallpreparredsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " ORDER BY pwid ASC"; 
	$enmpe_prayerwalls = $wpdb->get_results( $enmpe_wallpreparredsql );

} elseif ($_POST) { // If updating a prayer request
	$enmpe_id = strip_tags($_POST['prayerengine_id']);
	$enmpe_username = strip_tags($_POST['prayerengine_moderated_by']);
	$enmpe_post_this = strip_tags($_POST['prayerengine_post_this']);
	
	$enmpe_name = strip_tags($_POST['prayerengine_name']); // Displayed name of Prayer Request
	
	$enmpe_share_option = strip_tags($_POST['prayerengine_share_option']); // How to actually share it
	
	$enmpe_wall_id = strip_tags($_POST['prayerengine_wall_id']); // Which prayer wall?

	if (isset($_POST['prayerengine_notifyme'])) {
		$enmpe_notifyme = strip_tags($_POST['prayerengine_notifyme']);
	} else {
		$enmpe_notifyme = 0;
	}

	if (isset($_POST['prayerengine_answered'])) {
		$enmpe_answered = strip_tags($_POST['prayerengine_answered']);
	} else {
		$enmpe_answered = 0;
	}
		
	if (empty($_POST['prayerengine_posted_prayer'])) { //validate presence and length of prayer
		$enmpe_errors[] = '- Please enter a prayer request.';
	} else {
		if (preg_match('/(href=)/', $_POST['prayerengine_posted_prayer']) || preg_match('/(HREF=)/', $_POST['prayerengine_posted_prayer'])) { // Simple check for spam hyperlinks
			$enmpe_errors[] = '- Sorry, no HTML is allowed in the prayer request.';
		} else {
			$enmpe_prayer = strip_tags($_POST['prayerengine_posted_prayer']);
		}
	};
	
	if (isset($_POST['prayerengine_twitter_ok'])) { //tweet prayer
		$enmpe_twitter_ok = strip_tags($_POST['prayerengine_twitter_ok']);
	} else {
		$enmpe_twitter_ok = 0;
	}
	
	if (isset($_POST['prayerengine_twitter_ok'])) { //validate prayer tweet
		if (!empty($_POST['prayerengine_prayer_tweet'])) { 
			if (preg_match('/(href=)/', $_POST['prayerengine_prayer_tweet']) || preg_match('/(HREF=)/', $_POST['prayerengine_prayer_tweet'])) { // Simple check for spam hyperlinks
				$enmpe_errors[] = '- Sorry, no HTML is allowed in the prayer tweet.';
			} else {
				$enmpe_prayer_tweet = strip_tags(substr($_POST['prayerengine_prayer_tweet'],0,140));
			}
		} else {
			$enmpe_errors[] = '- Please enter a prayer tweet.';
		};
	} else {
		$enmpe_prayer_tweet = NULL;
	}
	
	
	if (empty($enmpe_errors)) {
		$enmpe_new_values = array( 'name' => $enmpe_name, 'wall_id' => $enmpe_wall_id, 'posted_prayer' => $enmpe_prayer, 'moderated_by' => $enmpe_username, 'post_this' => $enmpe_post_this, 'share_option' => $enmpe_share_option, 'notifyme' => $enmpe_notifyme, 'twitter_ok' => $enmpe_twitter_ok, 'prayer_tweet' => $enmpe_prayer_tweet, 'wall_id' => $enmpe_wall_id, 'answered' => $enmpe_answered ); 
		$enmpe_where = array( 'id' => $enmpe_id ); 
		$wpdb->update( $wpdb->prefix . "prayers", $enmpe_new_values, $enmpe_where ); 
		$enmpe_messages[] = "Prayer Request successfully updated!";
		
		include ('single_request.php'); // Find a save a single request
	} else {
		include ('single_request.php'); // Find a save a single request
	}

	// Get All Prayer Walls
	$enmpe_wallpreparredsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " ORDER BY pwid ASC"; 
	$enmpe_prayerwalls = $wpdb->get_results( $enmpe_wallpreparredsql );
	
} else { // View list or single request
	if ( isset($_GET['enmpe_swid']) ) {
		$enmpe_swid = " WHERE wall_id = " . strip_tags($_GET['enmpe_swid']);
	} else {
		$enmpe_swid = "";
	}

	if ( isset($_GET['enmpe_pid']) && is_numeric($_GET['enmpe_pid']) ) {
		if (isset($_GET['enmpe_new'])) { //Mark new prayer request as read
			if ($_GET['enmpe_new'] == 'yes') {
				$enmpe_id = strip_tags($_GET['enmpe_pid']);
				$enmpe_brandnew = 0;
				$enmpe_new_values = array( 'brand_new' => $enmpe_brandnew ); 
				$enmpe_where = array( 'id' => $enmpe_id ); 
				$wpdb->update( $wpdb->prefix . "prayers", $enmpe_new_values, $enmpe_where );
			}	
		}
		include ('single_request.php'); // Find a save a single request
	} else {
		include ('paginated_prayer_requests.php'); // Get all prayer requests	
	}

	// Get All Prayer Walls
	$enmpe_wallpreparredsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " ORDER BY pwid ASC"; 
	$enmpe_prayerwalls = $wpdb->get_results( $enmpe_wallpreparredsql );
}

?>
<div class="wrap"> 
<?php if ( isset($_GET['enmpe_pid']) && is_numeric($_GET['enmpe_pid']) ) { // If a specific prayer is requested ?>
	<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/editprayers.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/jquery.limit.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/prayertweet.js'; ?>"></script>

	<h2 class="enmpe">Edit Prayer Request</h2>
	<?php include ('errorbox.php'); ?>
	<?php include ('messagebox.php'); ?>
	
	<?php if (isset($_GET['enmpe_c'])) { ?>
		<?php if (isset($_GET['enmpe_swid'])) { ?>
			<p><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) . "&amp;enmpe_c=" . $_GET['enmpe_c'] . "&amp;enmpe_p=" . $_GET['enmpe_p'] . "&amp;enmpe_swid=" . $_GET['enmpe_swid'] ?>">&laquo; Return to Prayer Requests</a></p>
		<?php } else { ?>
			<p><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) . "&amp;enmpe_c=" . $_GET['enmpe_c'] . "&amp;enmpe_p=" . $_GET['enmpe_p']  ?>">&laquo; Return to Prayer Requests</a></p>
		<?php } ?>
	<?php } else { ?>
		<?php if (isset($_GET['enmpe_swid'])) { ?>
			<p><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) . "&amp;enmpe_swid=" . $_GET['enmpe_swid'] ?>">&laquo; Return to Prayer Requests</a></p>
		<?php } else { ?>
			<p><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) ?>">&laquo; Return to Prayer Requests</a></p>
		<?php } ?>
	<?php } ?>
	
	<table class="widefat"> 
		<thead> 
			<tr> 
				<th>From</th> 
				<th>Email</th> 
				<th>Phone</th>
				<th>Date Received</th>
				<th>Last Moderated By</th> 
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?php if (strlen($enmpe_prayer->name) == 0) { ?>No Name Provided<?php } else { ?><?php echo $enmpe_prayer->name ?><?php } ?></td>
				<td><a href="mailto:<?php echo $enmpe_prayer->email ?>"><?php echo $enmpe_prayer->email ?></a></td>
				<td><?php echo $enmpe_prayer->phone ?></td>
				<td><?php echo date('F j, Y', strtotime($enmpe_prayer->date_received)) ?> at <?php echo date('g:i A', strtotime($enmpe_prayer->date_received)) ?></td>
				<td><?php if ( $enmpe_prayer->post_this == 0 ) { ?>Awaiting Moderation<?php } else { ?><?php echo $enmpe_prayer->moderated_by ?><?php } ?></td>
			</tr>
		</tbody>
	</table>
	<h4>Original Request:</h4>
	<?php echo Markdown(stripslashes('&quot;' . $enmpe_prayer->prayer . '&quot;')) ?>
	
	<?php if ($enmpe_prayer->desired_share_option == "Share Online") { ?><h3>Please share this prayer request online.</h3><?php } ?>
	<?php if ($enmpe_prayer->desired_share_option == "Share Online Anonymously") { ?><h3>Please share this prayer request online <em>anonymously</em>.</h3><?php } ?>
	<?php if ($enmpe_prayer->desired_share_option == "DO NOT Share Online") { ?><h3>DO NOT share this prayer request online.</h3><?php } ?>
	
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
	<table class="form-table">
		<tr valign="top">
			<th scope="row">Update Name:</th>
			<td><input id='prayerengine_name' name='prayerengine_name' type='text' value='<?php echo $enmpe_prayer->name ?>' tabindex="1" /></td>
		</tr>
		<tr valign="top">
			<th scope="row">How to Share:</th>
			<td>
				<?php if ($enmpe_prayer->post_this == 0) { ?>
				<select name="prayerengine_share_option" id="prayerengine_share_option" tabindex="2">
					<option value="Share Online" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_share_option'] == "Share Online") { echo 'selected="selected"';}} else {if ($enmpe_prayer->desired_share_option == "Share Online") {echo 'selected="selected"';}} ?>>Share Online</option>
					<option value="Share Online Anonymously" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_share_option'] == "Share Online Anonymously") { echo 'selected="selected"';}} else {if ($enmpe_prayer->desired_share_option == "Share Online Anonymously") {echo 'selected="selected"';}} ?>>Share Online Anonymously</option>
					<option value="DO NOT Share Online" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_share_option'] == "DO NOT Share Online") { echo 'selected="selected"';}} else {if ($enmpe_prayer->desired_share_option == "DO NOT Share Online") {echo 'selected="selected"';}} ?>>DO NOT Share Online</option>
				</select>
				<?php } else { ?>
				<select name="prayerengine_share_option" id="prayerengine_share_option" tabindex="2">
					<option value="Share Online" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_share_option'] == "Share Online") { echo 'selected="selected"';}} else {if ($enmpe_prayer->share_option == "Share Online") {echo 'selected="selected"';}} ?>>Share Online</option>
					<option value="Share Online Anonymously" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_share_option'] == "Share Online Anonymously") { echo 'selected="selected"';}} else {if ($enmpe_prayer->share_option == "Share Online Anonymously") {echo 'selected="selected"';}} ?>>Share Online Anonymously</option>
					<option value="DO NOT Share Online" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_share_option'] == "DO NOT Share Online") { echo 'selected="selected"';}} else {if ($enmpe_prayer->share_option == "DO NOT Share Online") {echo 'selected="selected"';}} ?>>DO NOT Share Online</option>
				</select>
				<?php } ?>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Prayer Wall:</th>
			<td>
				<select name="prayerengine_wall_id" id="prayerengine_wall_id" tabindex="3">
					<?php foreach ($enmpe_prayerwalls as $enmpe_pw) { ?>
					<option value="<?php echo $enmpe_pw->pwid; ?>" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_wall_id'] == $enmpe_pwid) { echo 'selected="selected"';}} else {if ($enmpe_prayer->wall_id == $enmpe_pw->pwid) {echo 'selected="selected"';}} ?>><?php echo stripcslashes($enmpe_pw->wall_name); ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Moderated Prayer Request:</th>
			<td>
				<?php if ($enmpe_prayer->post_this == 0) { ?>
				<textarea name="prayerengine_posted_prayer" rows="8" cols="40" tabindex="4"><?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_posted_prayer'];} else {echo stripslashes($enmpe_prayer->prayer);} ?></textarea><br />
				<?php } else { ?>
				<textarea name="prayerengine_posted_prayer" rows="8" cols="40" tabindex="4"><?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_posted_prayer'];} else {echo stripslashes($enmpe_prayer->posted_prayer);} ?></textarea><br />
				<?php } ?>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Email When Someone Prays?:</th>
			<td>
				<input name="prayerengine_notifyme" id="prayerengine_notifyme" type="checkbox" value="1" class="check" tabindex="5" <?php if ($_POST && !empty($enmpe_errors) && isset($_POST['prayerengine_notifyme'])) {echo 'checked="checked"';} elseif ($enmpe_prayer->notifyme == 1) {echo 'checked="checked"';} ?> />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Prayer Answered?:</th>
			<td>
				<input name="prayerengine_answered" id="prayerengine_answered" type="checkbox" value="1" class="check" tabindex="6" <?php if ($_POST && !empty($enmpe_errors) && isset($_POST['prayerengine_answered'])) {echo 'checked="checked"';} elseif ($enmpe_prayer->answered == 1) {echo 'checked="checked"';} ?> />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Share on Twitter?:</th>
			<td>
				<input name="prayerengine_twitter_ok" id="prayerengine_twitter_ok" type="checkbox" value="1" class="check" tabindex="7" <?php if ($_POST && !empty($enmpe_errors) && isset($_POST['prayerengine_twitter_ok'])) {echo 'checked="checked"';} elseif ($enmpe_prayer->twitter_ok == 1) {echo 'checked="checked"';} ?> />
			</td>
		</tr>
		<tr valign="top" id="twitter-area" <?php if ($_POST && !empty($enmpe_errors)) {if (isset($_POST['twitter_ok'])) {}} else {if ($enmpe_prayer->twitter_ok == 1) {} else {echo 'style="display:none"';}} ?>>
			<th scope="row">Prayer Tweet:</th>
			<td>
				<textarea name="prayerengine_prayer_tweet" rows="3" cols="40" tabindex="8" id="prayerengine_prayer_tweet"><?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_prayer_tweet'];} else {echo stripslashes($enmpe_prayer->prayer_tweet);} ?></textarea><br />
				<p class="twitter-counter"><span id="counter">0</span>&nbsp;characters remaining.</p>
			</td>
		</tr>
	</table>
	<input type="hidden" name="prayerengine_id" value="<?php echo $enmpe_prayer->id ?>" id="prayerengine_id" />
	<input type="hidden" name="prayerengine_moderated_by" value="<?php echo $enmpe_userdetails->display_name ?>" id="prayerengine_moderated_by" />
	<input type="hidden" name="prayerengine_post_this" value="1" id="prayerengine_post_this" />
	<p class="submit"><input name="Submit" type="submit" class="button-primary" value="Save Changes" tabindex="9" /></p>
	</form>
	
	<?php if (isset($_GET['enmpe_c'])) { ?>
		<?php if (isset($_GET['enmpe_swid'])) { ?>
			<p><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) . "&amp;enmpe_c=" . $_GET['enmpe_c'] . "&amp;enmpe_p=" . $_GET['enmpe_p'] . "&amp;enmpe_swid=" . $_GET['enmpe_swid'] ?>">&laquo; Return to Prayer Requests</a></p>
		<?php } else { ?>
			<p><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) . "&amp;enmpe_c=" . $_GET['enmpe_c'] . "&amp;enmpe_p=" . $_GET['enmpe_p']  ?>">&laquo; Return to Prayer Requests</a></p>
		<?php } ?>
	<?php } else { ?>
		<?php if (isset($_GET['enmpe_swid'])) { ?>
			<p><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) . "&amp;enmpe_swid=" . $_GET['enmpe_swid'] ?>">&laquo; Return to Prayer Requests</a></p>
		<?php } else { ?>
			<p><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) ?>">&laquo; Return to Prayer Requests</a></p>
		<?php } ?>
	<?php } ?>
	<?php include ('pecredits.php'); ?>	
	
<?php } else { // If a specific prayer isn't requested ?>

	<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/deleteprayer.js'; ?>"></script>

	<h2 class="enmpe">Manage Prayer Requests <a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php_addnew', __FILE__ ) ?>" class="add-new-h2">Add New</a></h2>
	<?php include ('messagebox.php'); ?>
	<p>All submitted prayer requests are listed below. Click a prayer request to moderate content or edit other details. If you would like to permanently remove a prayer request, simply click "Delete." New prayer requests are labelled with a green background. Prayer requests waiting for moderation are labelled with a dark background.</p>
	
	<?php if (isset($_GET['enmpe_swid'])) { ?>
		<h3>Displaying All Requests from "<?php foreach ( $enmpe_prayerwalls as $enmpe_pw ) { if ( $enmpe_pw->pwid == $_GET['enmpe_swid'] ) { echo $enmpe_pw->wall_name; }} ?>..." <em style="font-size: 0.8em; font-weight: 300"><a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) ?>">(remove filter)</a></em></h3>
	<?php } ?>
	
	<?php include ('pagination.php'); ?>
	<table class="widefat"> 
		<thead> 
			<tr> 
				<th>Name</th> 
				<th>Prayer Wall</th>
				<th>Date Received</th> 
				<th>Moderated By</th> 
				<th>Delete?</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ( $enmpe_prayers as $enmpe_prayer ) { ?>
			<tr <?php if ( $enmpe_prayer->brand_new == 1 ) { ?>style="background-color: #d6f9de"<?php } elseif ( $enmpe_prayer->post_this == 0 ) { ?>style="background-color: #eaeaea"<?php } ?>>
				<td><a href="<?php echo $_SERVER['REQUEST_URI'] . "&amp;enmpe_pid=" . $enmpe_prayer->id; ?><?php if ($enmpe_prayer->brand_new == 1) { ?>&amp;enmpe_new=yes<?php } ?>"><?php if (strlen($enmpe_prayer->name) == 0) { ?>No Name Provided<?php } else { ?><?php echo $enmpe_prayer->name ?><?php } ?></a></td>
				<td><?php foreach ( $enmpe_prayerwalls as $enmpe_pw ) { if ( $enmpe_pw->pwid == $enmpe_prayer->wall_id ) { echo '<a href="' . admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ ) . '&amp;enmpe_swid=' . $enmpe_pw->pwid . '">' .  $enmpe_pw->wall_name . '</a>'; } }?></td>
				<td><?php echo date('F j, Y', strtotime($enmpe_prayer->date_received)) ?></td>
				<td><?php if ( $enmpe_prayer->post_this == 0 ) { ?>Awaiting Moderation<?php } else { ?><?php echo $enmpe_prayer->moderated_by ?><?php } ?></td>
				<td><form action="<?php echo $_SERVER['REQUEST_URI']; ?>&amp;enmpe_did=1" method="post" id="prayerengine-deleteform<?php echo $enmpe_prayer->id ?>"><input type="hidden" name="prayerengine_delete" value="<?php echo $enmpe_prayer->id ?>"></form><a href="#" class="prayerengine_delete" name="<?php echo $enmpe_prayer->id ?>">Delete</a></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<?php include ('pecredits.php'); ?>	
<?php } ?>
</div>
<?php } // Deny access to sneaky people!
} else {
	exit("Access Denied");
} ?>