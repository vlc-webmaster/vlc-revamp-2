<?php /* Prayer Engine - Settings Page */
global $wp_version;
if ( $wp_version != null ) { // Verify that user is allowed to access this page
if ( current_user_can( 'edit_posts' ) ) { 

	if ( isset($_GET['settings-updated']) && $_GET['settings-updated'] == "true") {
		$enmpe_options = get_option( 'enm_prayerengine_options' ); 
		generate_pe_options_css($enmpe_options);
	}

	?>
<div class="wrap"> 
	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo plugins_url() .'/prayerengine_plugin/css/colorpicker.css'; ?>" />
	<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/colorpicker.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/adminsettings.js'; ?>"></script>
	
	<h2 class="enmpe">Prayer Engine Settings</h2> 
	<ul id="enmpe-prayer-options">
		<li class="selected"><a href="#" id="enmpe-settings-general">General Settings</a></li>
		<li><a href="#" id="enmpe-settings-styles">Styles and Colors</a></li>
	</ul>
	<form action="options.php" method="post"> 
		<?php settings_fields('enm_prayerengine_options'); do_settings_sections('prayerengine_plugin'); ?> 
		<p class="submit"><input name="Submit" type="submit" class="button-primary" value="Save Changes" /></p>
	</form>
	<?php include ('pecredits.php'); ?>	
</div>
<?php } // Deny access to sneaky people!
} else {
	exit("Access Denied");
} ?>

