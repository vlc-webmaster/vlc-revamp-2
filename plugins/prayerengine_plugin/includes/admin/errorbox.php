<?php /* Prayer Engine - Display Errors */

if(!empty($enmpe_errors)) { ?>
	<div id="message" class="error">
		<p>Your changes could not be saved due to the following errors...</p>
		<ul>
		<?php foreach ($enmpe_errors as $error) {
			echo "<li>$error</li>";
		}; ?>
		</ul>
	</div> 
<?php } ?>