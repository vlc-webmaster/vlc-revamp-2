<?php /* Prayer Engine - Admin - Add a New Request */

global $wp_version;
if ( $wp_version != null ) { // Verify that user is allowed to access this page
if ( current_user_can( 'edit_posts' ) ) {

$enmpe_peoptions = get_option( 'enm_prayerengine_options' ); 

$enmpe_userdetails = wp_get_current_user(); 
if ( !defined('MARKDOWN_VERSION') ) {
	include ('markdown.php');
}

global $wpdb;

if ($_POST) {
	$enmpe_errors = array(); //Set up errors array
	$enmpe_messages = array(); //Set up messages array
	
	//Get all info from the form and validate some of it
	$enmpe_name = strip_tags($_POST['prayerengine_name']);
	
	if (empty($_POST['prayerengine_email'])) { //validate presence and format of email
		$enmpe_errors[] = '- An email address is required to submit a prayer request.';
	} else {
		if (preg_match('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,8})$^', $_POST['prayerengine_email'])) { 
			$enmpe_email = $_POST['prayerengine_email'];
		} else {
			$enmpe_errors[] = '- A valid email address is required.';
		};
	};
	
	$enmpe_phone = strip_tags($_POST['prayerengine_phone']);
	$enmpe_desiredshareoption = strip_tags($_POST['prayerengine_share_option']);
	$enmpe_shareoption = strip_tags($_POST['prayerengine_share_option']);
	
	if (empty($_POST['prayerengine_posted_prayer'])) { //validate presence and length of prayer
		$enmpe_errors[] = '- Please enter a prayer request.';
	} else {
		if (preg_match('/(href=)/', $_POST['prayerengine_posted_prayer']) || preg_match('/(HREF=)/', $_POST['prayerengine_posted_prayer'])) { // Simple check for spam hyperlinks
			$enmpe_errors[] = '- Sorry, no HTML is allowed in the prayer request.';
		} else {
			$enmpe_prayer = strip_tags($_POST['prayerengine_posted_prayer']);
			$enmpe_posted_prayer = strip_tags($_POST['prayerengine_posted_prayer']);
		}
	};
		
	if (isset($_POST['prayerengine_notifyme'])) {
		$enmpe_notifyme = strip_tags($_POST['prayerengine_notifyme']);
	} else {
		$enmpe_notifyme = 0;
	}

	if (isset($_POST['prayerengine_answered'])) {
		$enmpe_answered = strip_tags($_POST['prayerengine_answered']);
	} else {
		$enmpe_answered = 0;
	}
	
	if (isset($_POST['prayerengine_twitter_ok'])) { //tweet prayer
		$enmpe_twitter_ok = strip_tags($_POST['prayerengine_twitter_ok']);
	} else {
		$enmpe_twitter_ok = 0;
	}
	
	if (isset($_POST['prayerengine_twitter_ok'])) { //validate prayer tweet
		if (!empty($_POST['prayerengine_prayer_tweet'])) { 
			if (preg_match('/(href=)/', $_POST['prayerengine_prayer_tweet']) || preg_match('/(HREF=)/', $_POST['prayerengine_prayer_tweet'])) { // Simple check for spam hyperlinks
				$enmpe_errors[] = '- Sorry, no HTML is allowed in the prayer tweet.';
			} else {
				$enmpe_prayer_tweet = strip_tags(substr($_POST['prayerengine_prayer_tweet'],0,140));
			}
		} else {
			$enmpe_errors[] = '- Please enter a prayer tweet.';
		};
	} else {
		$enmpe_prayer_tweet = NULL;
	}
	
	$enmpe_datereceived = strip_tags($_POST['prayerengine_date_received']);
	$enmpe_prayercount = strip_tags($_POST['prayerengine_prayer_count']);
	$enmpe_brandnew = strip_tags($_POST['prayerengine_brand_new']);
	$enmpe_moderated_by = strip_tags($_POST['prayerengine_moderated_by']);
	$enmpe_postthis = strip_tags($_POST['prayerengine_post_this']);
	$enmpe_dc = md5(uniqid(rand(), true));
	$enmpe_wall_id = strip_tags($_POST['prayerengine_wall_id']); // Which prayer wall?
	
	if (empty($enmpe_errors)) {
		$enmpe_newprayer = array(
			'name' => $enmpe_name, 
			'email' => $enmpe_email,
			'phone' => $enmpe_phone,
			'share_option' => $enmpe_shareoption,
			'prayer_count' => $enmpe_prayercount,
			'moderated_by' => $enmpe_moderated_by,
			'post_this' => $enmpe_postthis,
			'prayer' => $enmpe_prayer,
			'posted_prayer' => $enmpe_posted_prayer,
			'date_received' => $enmpe_datereceived,
			'brand_new' => $enmpe_brandnew,
			'notifyme' => $enmpe_notifyme,
			'prayer_tweet' => $enmpe_prayer_tweet,
			'twitter_ok' => $enmpe_twitter_ok,
			'desired_share_option' => $enmpe_desiredshareoption,
			'delete_code' => $enmpe_dc,
			'wall_id' => $enmpe_wall_id,
			'answered' => $enmpe_answered,
			); 
		$wpdb->insert( $wpdb->prefix . "prayers", $enmpe_newprayer );
		$enmpe_messages[] = "A new prayer request has been added to the prayer wall according to your sharing instructions!";
	}
}

	// Get All Prayer Walls
	$enmpe_wallpreparredsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " ORDER BY pwid ASC"; 
	$enmpe_prayerwalls = $wpdb->get_results( $enmpe_wallpreparredsql );

?>
<div class="wrap"> 
	<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/editprayers.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/jquery.limit.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo plugins_url() .'/prayerengine_plugin/js/prayertweet.js'; ?>"></script>

	<h2 class="enmpe">Add a New Prayer Request</h2>
	<?php include ('errorbox.php'); ?>
	<?php include ('messagebox.php'); ?>

	<p>You may add a new prayer request by using the form below. <em>This prayer request will immediately be posted live to the prayer wall unless you choose "Do Not Share This."</em></p>

	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
		<table class="form-table">
			<tr valign="top">
				<th scope="row">Name:</th>
				<td><input id='prayerengine_name' name='prayerengine_name' type='text' value='<?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_name'];} ?>' tabindex="1" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">Email:</th>
				<td><input id='prayerengine_email' name='prayerengine_email' type='text' value='<?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_email'];} ?>' tabindex="2" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">Phone Number:</th>
				<td><input id='prayerengine_phone' name='prayerengine_phone' type='text' value='<?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_phone'];} ?>' tabindex="3" /></td>
			</tr>
			<tr valign="top">
				<th scope="row">How to Share:</th>
				<td>
					<select name="prayerengine_share_option" id="prayerengine_share_option" tabindex="4">
						<option value="Share Online" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_share_option'] == "Share Online") { ?>selected="selected"<?php }} ?>>Post Online</option>
						<option value="Share Online Anonymously" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_share_option'] == "Share Online Anonymously") { ?>selected="selected"<?php }} ?>>Post Online Anonymously</option>
						<option value="DO NOT Share Online" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_share_option'] == "DO NOT Share Online") { ?>selected="selected"<?php }} ?>>DO NOT Post Online</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Prayer Wall:</th>
				<td>
					<select name="prayerengine_wall_id" id="prayerengine_wall_id" tabindex="3">
						<?php foreach ($enmpe_prayerwalls as $enmpe_pw) { ?>
						<option value="<?php echo $enmpe_pw->pwid; ?>" <?php if ($_POST && !empty($enmpe_errors)) {if ($_POST['prayerengine_wall_id'] == $enmpe_pwid) { echo 'selected="selected"';}} ?>><?php echo stripcslashes($enmpe_pw->wall_name); ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Prayer Request:</th>
				<td>
					<textarea name="prayerengine_posted_prayer" id="prayerengine_posted_prayer" rows="8" cols="40" tabindex="5"><?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_posted_prayer'];} ?></textarea><br />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Email When Someone Prays?:</th>
				<td>
					<input name="prayerengine_notifyme" id="prayerengine_notifyme" type="checkbox" value="1" class="check" tabindex="6" <?php if ($_POST && !empty($enmpe_errors) && isset($_POST['prayerengine_notifyme'])) {echo 'checked="checked"';} ?> />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Prayer Answered?:</th>
				<td>
					<input name="prayerengine_answered" id="prayerengine_answered" type="checkbox" value="1" class="check" tabindex="7" <?php if ($_POST && !empty($enmpe_errors) && isset($_POST['prayerengine_answered'])) {echo 'checked="checked"';} ?> />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">Share on Twitter?:</th>
				<td>
					<input name="prayerengine_twitter_ok" id="prayerengine_twitter_ok" type="checkbox" value="1" class="check" tabindex="8" <?php if ($_POST && !empty($enmpe_errors) && isset($_POST['prayerengine_twitter_ok'])) {echo 'checked="checked"';} ?> />
				</td>
			</tr>
			<tr valign="top" id="twitter-area" <?php if ($_POST && !empty($enmpe_errors)) {if (isset($_POST['prayerengine_twitter_ok'])) {} else {echo 'style="display:none"';}} else {echo 'style="display:none"';} ?>>
				<th scope="row">Prayer Tweet:</th>
				<td>
					<textarea name="prayerengine_prayer_tweet" rows="3" cols="40" tabindex="9" id="prayerengine_prayer_tweet"><?php if ($_POST && !empty($enmpe_errors)) {echo $_POST['prayerengine_prayer_tweet'];} ?></textarea><br />
					<p class="twitter-counter"><span id="counter">0</span>&nbsp;characters remaining.</p>
				</td>
			</tr>
		</table>
		<input type="hidden" name="prayerengine_brand_new" value="0" id="prayerengine_brand_new" />
		<input type="hidden" name="prayerengine_date_received" value="<?php echo date("Y-m-d H:i:s", time()) ?>" id="prayerengine_date_received" />
		<input type="hidden" name="prayerengine_prayer_count" value="0" id="prayerengine_prayer_count" />
		<input type="hidden" name="prayerengine_moderated_by" value="<?php echo $enmpe_userdetails->display_name ?>" id="prayerengine_moderated_by" />
		<input type="hidden" name="prayerengine_post_this" value="1" id="prayerengine_post_this" />
		<p class="submit"><input name="Submit" type="submit" class="button-primary" value="Add New Request" tabindex="10" /></p>
	</form>
	<?php include ('pecredits.php'); ?>	
</div>
<?php } // Deny access to sneaky people!
} else {
	exit("Access Denied");
} ?>