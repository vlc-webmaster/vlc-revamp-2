<?php /* Prayer Engine - User Guide Page */
global $wp_version;
if ( $wp_version != null ) { // Verify that user is allowed to access this page ?>
<div class="wrap"> 
	<h2 class="enmpe">Using The Prayer Engine</h2>
	
	<p>&nbsp;</p>
	<blockquote>
	<ul>
		<li><a href="#pe-embedding">Embedding Your Prayer Wall</a></li>
		<li><a href="#pe-customizing">Customizing Your Prayer Engine Install</a></li>
		<li>&nbsp;&nbsp;&nbsp;<a href="#pe-styling">- Styling Your Prayer Wall</a></li>
		<li>&nbsp;&nbsp;&nbsp;<a href="#pe-advanced">- Advanced Customizations</a></li>
		<li>&nbsp;&nbsp;&nbsp;<a href="#pe-css-troubleshooting">- Troubleshooting</a></li>
		<li><a href="#pe-prayerwalls">Managing Prayer Walls</a></li>
		<li><a href="#pe-managing">Managing Prayer Requests</a></li>
		<li><a href="#pe-manually">Manually Adding a Request to the Prayer Wall</a></li>
		<li><a href="#pe-print">Print Reports of Prayer Requests</a></li>
		<li><a href="#pe-distributing">Distributing Prayer Requests Beyond Your Prayer Wall</a></li>
		<li>&nbsp;&nbsp;&nbsp;<a href="#pe-rss">- RSS</a></li>
		<li>&nbsp;&nbsp;&nbsp;<a href="#pe-facebook">- Facebook</a></li>
		<li>&nbsp;&nbsp;&nbsp;<a href="#pe-twitter">- Twitter</a></li>
		<li><a href="#pe-users">Managing Prayer Engine Users</a></li>
		<li><a href="#pe-emails">Email Notifications</a></li>
		<li><a href="#pe-updates">Plugin Updates</a></li>
		<li><a href="#pe-help">Need Help? Have Feedback?</a></li>
		<li><a href="#pe-usage">Acceptable Usage and Legal</a></li>
		<li><a href="#pe-seriesengine"><strong>NEW -> Manage Sermons and Podcasts</strong></a></li>
	</ul>
	</blockquote>
	<p>&nbsp;</p>
	<div id="enmpe-pe-logo"></div>
	
	<h3 id="pe-embedding">Embedding Your Prayer Wall</h3>
	<p>You can view <a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php' . '_embed' ); ?>">embed instructions here</a>.</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
	<h3 id="pe-customizing">Customizing Your Prayer Engine Install</h3>
	<h4 id="pe-styling">Styling Your Prayer Wall</h4>
	<p>You can quickly (and easily!) modify all of The Prayer Engine's colors to match your theme by using the color picker found in <em><a href="<?php echo admin_url() . "options-general.php?page=enm_prayerengine"; ?>">Settings > The Prayer Engine</a></em>. No knowledge of HTML or CSS is required.</p>
	<h4 id="pe-advanced">Advanced Customizations</h4>
	<p>Feel free to modify and customize The Prayer Engine plugin's core code as you wish (at your own risk, of course!). To make it easier to upgrade to future versions of the plugin, we advise that you house your customizations within the plugin's "custom" directory. Before installing a plugin update, backup the contents of this directory and restore your customizations when the update is complete (all customizations will be overwritten as part of the update process).</p>
	<h4 id="pe-css-troubleshooting">Troubleshooting</h4>
	<p>Although The Prayer Engine is designed to be compatible with all WordPress themes and plugins, sometimes other extensions may modify the WordPress code in a way that will cause your prayer wall to render improperly. If you see unusual blocks of white space or other display errors showing up on your prayer wall, your chosen theme or other installed plugins may be the culprit.</p>
	<p>There are a few things you can try to make your prayer wall display properly. First, try wrapping The Prayer Engine's shortcode with the "raw" shortcode (ex: [raw][prayerengine][/raw]). This is a shortcode that many developers include in their projects that will force certain blocks of code to ignore automatic (and incorrect) WordPress formatting.</p>
	<p>If adding the "raw" shortcode doesn't work (it may not be supported by your theme/plugin developer), go to <a href="<?php echo admin_url() . "options-general.php?page=enm_prayerengine"; ?>">Settings > The Prayer Engine</a> and enable the "formatting override" in the "Prayer Wall Colors" section. Similar to the approach above, open the page where The Prayer Engine is embedded and wrap The Prayer Engine's shortcode with the "peraw" shortcode (ex: [peraw][prayerengine][/peraw]). This should fix the formatting issues in most scenarios. If it does not, you may need to try another theme or disable conflicting plugins.</p>
	<h4>Color and Other Style Changes Not Taking Effect?</h4>
	<p>There's probably a permissions issue on your server.</p>
	<p>Every time you make a change to the styles in Prayer Engine's Settings menu, Prayer Engine overwrites its stylesheet (css/pe_styles.css) with a new one. In rare instances, server settings won't allow this.</p>
	<p>Thankfully, it's normally an easy fix. Have your web developer log in to your site's plugins directory with an FTP client, and ensure that the permissions on the "css" folder are 755, and the permissions on pe_styles.css in that folder are 644.</p>
	<p>If that doesn't work, you may need to contact your hosting company and have them make a permissions change for you.</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>

	<h3 id="pe-prayerwalls">Managing Prayer Walls</h3>
	<p>Prayer Engine is set up to run a single Prayer Wall immediately after your installation, but a recent plugin update allows you to create as many Prayer Walls as you like! Simply visit "<a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php' . '_prayerwalls' ); ?>">Prayer Engine > Manage Prayer Walls</a>" to create, edit and delete a Prayer Wall.</p>
	<p>From this section, you'll be able to edit the default Prayer Wall, create a new Prayer Wall or make changes to other existing Prayer Walls. If you delete a Prayer Wall, all prayer requests associated with that wall will be deleted as well. If you want to remove a Prayer Wall, but hold on to its requests, simply remove the shortcode from a page so that its no longer publically available.</p>
	<p><em>Please note: The default Prayer Wall cannot be deleted.</em></p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
	
	<h3 id="pe-managing">Managing Prayer Requests</h3>
	<p>All prayer requests received through the Prayer Wall must be moderated before they are shared in any capacity (unless moderation is disabled in <a href="<?php echo admin_url() . "options-general.php?page=enm_prayerengine"; ?>">Settings > The Prayer Engine</a>). This provides absolute control over the content visible to the public, and serves as an important barrier to gossip and the sharing of sensitive or inappropriate information.</p>
	<p>When a prayer request is successfully submitted through a Prayer Wall, administrators will immediately receive notification of its arrival via email (according to their user account settings in their WordPress User Profile page). Simply log in to WordPress's admin portal to begin a quick moderation process.</p>
	<p>After logging in, select the Prayer Engine plugin menu and click a request to view a permanent copy of the original prayer request and edit a live copy to be shared according to the user's instructions. If "Share Online" or "Share Online Anonymously" is selected when you save your changes, the prayer request will immediately be shared on the Prayer Wall and through the various prayer feeds (see "<a href="#pe-distributing">Distributing Prayer Requests Beyond Your Prayer Wall</a>"). Any user with sufficient privileges may update or remove a prayer request from the Prayer Wall at any time.</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>

	<h3 id="pe-manually">Manually Adding a Request to a Prayer Wall</h3>
	<p>If you are aware of a prayer request that needs to be added to your Prayer Wall, you can add it directly within the Prayer Engine plugin menu. Simply log in, select the Prayer Engine plugin menu and select "<a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php' . '_addnew' ); ?>">Add a Request</a>." Please note that prayer requests entered through this form will immediately be posted to the live Prayer Wall (unless "Do Not Share This" is selected).</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
	<h3 id="pe-print">Print Reports of Prayer Requests</h3>
	<p>Although society is growing more tech-savvy by the day, some groups within your ministry may prefer a physical copy of prayer requests instead of viewing them through a computer. The Prayer Engine makes it easy to provide time-based printable reports of the most recent prayer requests.</p>
	<p>Simply log in and select  "<a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php' . '_reports' ); ?>">Report Library</a>" from the Prayer Engine plugin menu. From this screen you can run a printable report of prayer requests from the last day, week, month or quarter. Select "Run this Report" and print the results directly from the web browser. It's a great way to keep your prayer teams and other ministries up-to-date and on the same page. <em>Please note: Printed reports only include requests labeled as "Share This" or "Share This Anonymously" unless you override this setting in the dropdown menus at the top of the page.</em></p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
	<h3 id="pe-distributing">Distributing Prayer Requests Beyond Your Prayer Walls</h3>
	<p>One of the most powerful parts of The Prayer Engine is its ability to share prayer requests in numerous avenues without any additional work on your end. The Prayer Engine was built with extensibility and social networks in mind, and implementing these features couldn't be easier.</p>
	<h4 id="pe-rss">RSS</h4>
	<p>The Prayer Engine includes a valid RSS Feed out of the box, which can be accessed at the following URL:</p>
	<p><a href="<?php echo home_url() . "/?feed=prayerengine" ?>" target="_blank"><?php echo home_url(); ?>/?feed=prayerengine</a></p>
	<p>To make the most out of this functionality, we recommend combining this feed with a service such as <a href="http://www.feedburner.com" target="_blank">Feedburner</a> (where you can track stats, provide multiple subscription options, and even enable email subscriptions for daily email prayer blasts!).</p>
	<h4 id="pe-facebook">Facebook</h4>
	<p>Have a Facebook Page for your prayer ministry? The RSS Feed mentioned above can be used to automatically pull the latest prayer requests directly into the Timeline on that page. Simply search for the RSS Graffiti app on Facebook and follow the instructions to set up your feed import.</p>
	<h4 id="pe-twitter">Twitter</h4>
	<p>Twitter can be a terrific way to quickly share new prayer requests, and The Prayer Engine is the best tool to take advantage of this powerful communications medium. When enabled in <a href="<?php echo admin_url() . "options-general.php?page=enm_prayerengine"; ?>">Settings > The Prayer Engine</a>, The Prayer Engine will automatically provide an option for your users to provide a special prayer request that is specially formatted for Twitter. These "Prayer Tweets" are then shared through a special RSS Feed that is specifically built to be used with RSS-to-Twitter services such as <a href="http://twitterfeed.com" target="_blank">Twitter Feed</a>.</p>
	<p>You can moderate Prayer Tweets just like normal prayer requests, and they are immediately added to the Twitter Prayer Feed as they're approved. You can access the Twitter-formatted RSS Feed at the following URL:</p>
	<p><a href="<?php echo home_url() . "/?feed=prayertweets" ?>" target="_blank"><?php echo home_url(); ?>/?feed=prayertweets</a></p>
	<h4>Feeds from Other Prayer Walls</h4>
	<p>The instructions above use the feeds for the Default Prayer Wall as examples. You can locate the feeds for your custom created Prayer Walls by clicking their titles at "<a href="<?php echo admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php' . '_prayerwalls' ); ?>">Prayer Engine > Manage Prayer Walls</a>" and scrolling to the bottom of the page.</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
	<h3 id="pe-users">Managing Prayer Engine Users</h3>
	<p>All users for this WordPress site with an admin role of "Contributor" or higher may approve/edit prayer requests and manually add requests to the Prayer Wall. Users with a role of "Editor" or higher can view the User Guide, Embed Instructions and run Prayer Reports. Only Administrators can adjust Prayer Engine Settings.</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>

	<h3 id="pe-emails">Email Notifications</h3>
	<p>Prayer Engine can send email notifications to any number of WordPress users on your site when a new prayer request is received. To enable these notifications on a user-by-user basis, edit their User Profile in WordPress and check the "Get Prayer Received Emails" box in the Prayer Engine section at the bottom of the page.</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
	<h3 id="pe-updates">Plugin Updates</h3>
	<p>From time to time, we plan to issue updates to The Prayer Engine plugin that may include bug fixes, performance improvements, and even new features! Have your site administrator keep an eye on WordPress's Plugin Updates page.</p>
	<p>Updates will maintain your changes in <a href="<?php echo admin_url() . "options-general.php?page=enm_prayerengine"; ?>">Settings > The Prayer Engine</a>, but will overwrite any changes you have made to the core plugin code and custom stylesheets. Be sure to back up the "custom" directory before updating, and reapply your modifications when the update is complete.</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
	<h3 id="pe-help">Need Help? Have Feedback?</h3>
	<p>A regularly updated list of <a href="http://theprayerengine.com/questions.php" target="_blank">Frequently Asked Questions</a> is available at <a href="http://theprayerengine.com">theprayerengine.com</a>. Feel free to contact us with additional questions or feedback!</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
	<h3 id="pe-usage">Acceptable Usage</h3>
	<ul>
		<li>You <strong>MAY</strong> use one licensed copy of The Prayer Engine to install one or multiple prayer walls on a single website.</li>
		<li>You <strong>MAY NOT</strong> install one licensed copy of The Prayer Engine on multiple websites. Each website requires its own license.</li>
	</ul>
	
	<ul>
		<li>You <strong>MAY</strong> sell one previously purchased, licensed copy of The Prayer Engine to a client as part of one web project (ie: offering The Prayer Engine as an installed component of a Ministry Website). Each project requires its own individual licensed copy of The Prayer Engine.</li>
		<li>You <strong>MAY NOT</strong> resell one or any number of copies of The Prayer Engine on its own.</li>
	</ul>
	
	<ul>
		<li>You <strong>MAY</strong> alter the style and adjust the code/functionality of your licensed copy of The Prayer Engine however you see fit.</li>
		<li>You <strong>MAY NOT</strong> reuse unique components of The Prayer Engine in any commercial product without prior written consent from Volacious Creative Media.</li>
	</ul>
	
	<ul>
		<li>You <strong>MAY</strong> use your licensed copy of The Prayer Engine to collect and distribute prayer requests from your audience.</li>
		<li>You <strong>MAY NOT</strong> use your licensed copy of The Prayer Engine for nefarious purposes such as obstructing the privacy of its users.</li>
	</ul>
	
	<ul>
		<li>You <strong>MAY</strong> remove decorative branding (excluding copyright notices) from your licensed copy of The Prayer Engine.</li>
		<li>You <strong>MAY NOT</strong> remove copyright notices or label components of The Prayer Engine under a different copyright.</li>
	</ul>
	<h4>Legal</h4>
	<p>This software is provided by the copyright holder "as is" and any express or implied warranties, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose are disclaimed. In no event shall the copyright owner be liable for any direct, indirect, incidental, special, exemplary, or consequential damages (including, but not limited to, procurement of substitute goods or services; loss of use, data, or profits; or business interruption) however caused and on any theory of liability, whether in contract, strict liability, or tort (including negligence or otherwise) arising in any way out of the use of this software, even if advised of the possibility of such damage.</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>

	<h2 id="pe-seriesengine">A NEW Plugin: Introducing the Series Engine</h2>
	<p>If you like Prayer Engine, be sure to check out <a href="http://seriesengine.com">Series Engine</a>, our awesome new WordPress plugin for managing sermons and podcasts. There's never been a better way to share your ministry's sermons on your website and across the web!</p>
	<?php include ('pecredits.php'); ?>	
</div>
<?php  // Deny access to sneaky people!
} else {
	exit("Access Denied");
} ?>