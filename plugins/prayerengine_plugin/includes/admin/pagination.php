<?php /* Prayer Engine - Pagination for Various Pages */
		if ( $wpdb != null ) { // Verify that user is allowed to access this page
			
		function enmpe_curPageName() {
			return admin_url( '/admin.php?page=prayerengine_plugin/prayerengine_plugin.php', __FILE__ );
		}
		
		if ($enmpe_pages > 1) {
			$enmpe_thispage = enmpe_curPageName();
			$enmpe_current_page = ($enmpe_start/$enmpe_display) + 1;
			echo '<div class="tablenav prayerengine"><div class="tablenav-pages">';
			if (!isset($_GET['enmpe_c']) && ($enmpe_current_page != $enmpe_pages)) {
				echo '<span class="displaying-num">Displaying 1' . ($enmpe_start - $enmpe_display) . ' of ' . $enmpe_prayercount . '</span>'; 
			} elseif ($enmpe_current_page == $enmpe_pages) {
				echo '<span class="displaying-num">Displaying ' . ($enmpe_start+1) . '-' . $enmpe_prayercount . ' of ' . $enmpe_prayercount . '</span>';
			} else {
				echo '<span class="displaying-num">Displaying ' . ($enmpe_start+1) . '-' . ($enmpe_start+$enmpe_display) . ' of ' . $enmpe_prayercount . '</span>';
			}
			
			if ( isset($_GET['enmpe_swid'])) { // Is a specific prayer wall requested?

				if ($enmpe_current_page != 1) { // make previous button if not first page
					echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . ($enmpe_start - $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '&amp;enmpe_swid=' . strip_tags($_GET['enmpe_swid']) . '" class="previous page-numbers">&laquo;</a> ';
				}
			
				if ($enmpe_pages > 11) { // Make no more than 10 links to other pages at a time.
					if ($enmpe_current_page < 6) {
						for ($i = 1; $i <= 11; $i++) { 
							if ($i != $enmpe_current_page) {
								echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '&amp;enmpe_swid=' . strip_tags($_GET['enmpe_swid']) . '" class="page-numbers">' . $i . '</a> ';
							} else {
								echo '<span class="page-numbers current">' . $i . '</span> ';
							}
						} 
						echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_pages - 1) * $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '&amp;enmpe_swid=' . strip_tags($_GET['enmpe_swid']) . '" class="page-numbers">&hellip;' . $enmpe_pages . '</a>';
					} else {

						if ($enmpe_pages - $enmpe_current_page <= 5) {
							echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=0&amp;enmpe_p=' . $enmpe_pages . '&amp;enmpe_swid=' . strip_tags($_GET['enmpe_swid']) . '" class="page-numbers">1&hellip;</a>';
							$enmpe_startpoint = $enmpe_pages - 10;
							for ($i = $enmpe_startpoint; $i <= $enmpe_pages; $i++) { 
								if ($i != $enmpe_current_page) {
									echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '&amp;enmpe_swid=' . strip_tags($_GET['enmpe_swid']) . '" class="page-numbers">' . $i . '</a> ';
								} else {
									echo '<span class="page-numbers current">' . $i . '</span> ';
								}
							} 
						
						} else {
							if ($enmpe_current_page != 6) {
								echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=0&amp;enmpe_p=' . $enmpe_pages . '&amp;enmpe_swid=' . strip_tags($_GET['enmpe_swid']) . '" class="page-numbers">1&hellip;</a>';
							}					
							$enmpe_startpoint = $enmpe_current_page - 5;
							$enmpe_endpoint = $enmpe_current_page + 5;
							for ($i = $enmpe_startpoint; $i <= $enmpe_endpoint; $i++) { 
								if ($i != $enmpe_current_page) {
									echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '&amp;enmpe_swid=' . strip_tags($_GET['enmpe_swid']) . '" class="page-numbers">' . $i . '</a> ';
								} else {
									echo '<span class="page-numbers current">' . $i . '</span> ';
								}
							} 
							echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_pages - 1) * $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '&amp;enmpe_swid=' . strip_tags($_GET['enmpe_swid']) . '" class="page-numbers">&hellip;' . $enmpe_pages . '</a>';	
						}
					}
				} else {
					for ($i = 1; $i <= $enmpe_pages; $i++) { 
						if ($i != $enmpe_current_page) {
							echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '&amp;enmpe_swid=' . strip_tags($_GET['enmpe_swid']) . '" class="page-numbers">' . $i . '</a> ';
						} else {
							echo '<span class="page-numbers current">' . $i . '</span> ';
						}
					} 
				}
			
				if ($enmpe_current_page != $enmpe_pages) { // make next button if not the last page
					echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . ($enmpe_start + $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '&amp;enmpe_swid=' . strip_tags($_GET['enmpe_swid']) . '" class="next page-numbers">&raquo;</a>';
				}
			} else { // No prayer wall is requested

				if ($enmpe_current_page != 1) { // make previous button if not first page
					echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . ($enmpe_start - $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '" class="previous page-numbers">&laquo;</a> ';
				}
			
				if ($enmpe_pages > 11) { // Make no more than 10 links to other pages at a time.
					if ($enmpe_current_page < 6) {
						for ($i = 1; $i <= 11; $i++) { 
							if ($i != $enmpe_current_page) {
								echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="page-numbers">' . $i . '</a> ';
							} else {
								echo '<span class="page-numbers current">' . $i . '</span> ';
							}
						} 
						echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_pages - 1) * $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '" class="page-numbers">&hellip;' . $enmpe_pages . '</a>';
					} else {

						if ($enmpe_pages - $enmpe_current_page <= 5) {
							echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=0&amp;enmpe_p=' . $enmpe_pages . '" class="page-numbers">1&hellip;</a>';
							$enmpe_startpoint = $enmpe_pages - 10;
							for ($i = $enmpe_startpoint; $i <= $enmpe_pages; $i++) { 
								if ($i != $enmpe_current_page) {
									echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="page-numbers">' . $i . '</a> ';
								} else {
									echo '<span class="page-numbers current">' . $i . '</span> ';
								}
							} 
						
						} else {
							if ($enmpe_current_page != 6) {
								echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=0&amp;enmpe_p=' . $enmpe_pages . '" class="page-numbers">1&hellip;</a>';
							}					
							$enmpe_startpoint = $enmpe_current_page - 5;
							$enmpe_endpoint = $enmpe_current_page + 5;
							for ($i = $enmpe_startpoint; $i <= $enmpe_endpoint; $i++) { 
								if ($i != $enmpe_current_page) {
									echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="page-numbers">' . $i . '</a> ';
								} else {
									echo '<span class="page-numbers current">' . $i . '</span> ';
								}
							} 
							echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_pages - 1) * $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '" class="page-numbers">&hellip;' . $enmpe_pages . '</a>';	
						}
					}
				} else {
					for ($i = 1; $i <= $enmpe_pages; $i++) { 
						if ($i != $enmpe_current_page) {
							echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . (($enmpe_display * ($i - 1))) . '&amp;enmpe_p=' . $enmpe_pages . '" class="page-numbers">' . $i . '</a> ';
						} else {
							echo '<span class="page-numbers current">' . $i . '</span> ';
						}
					} 
				}
			
				if ($enmpe_current_page != $enmpe_pages) { // make next button if not the last page
					echo '<a href="' . $enmpe_thispage . '&amp;enmpe_c=' . ($enmpe_start + $enmpe_display) . '&amp;enmpe_p=' . $enmpe_pages . '" class="next page-numbers">&raquo;</a>';
				}

			}
	
			echo "<div style=\"clear: both;\"></div></div></div>\n"; 	
		}
		// Deny access to sneaky people!
		} else {
			exit("Access Denied");
		}
?>