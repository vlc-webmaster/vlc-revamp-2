<?php /* Prayer Engine - Printable Prayer Reports */
global $wp_version;
if ( $wp_version != null ) { // Verify that user is allowed to access this page
if ( current_user_can( 'edit_posts' ) ) {
	global $wpdb;

	// Get All Prayer Walls
	$enmpe_preparredsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " ORDER BY pwid ASC"; 
	$enmpe_prayerwalls = $wpdb->get_results( $enmpe_preparredsql ); 
 ?>
<div class="wrap"> 
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('#enmpe-embed-prayerwalls').change(function() {
				var findvalue = jQuery(this).val();
				var privatevalue = jQuery('#enmpe-private').val();
				if ( findvalue == 1 ) {
					jQuery('.enmpe_report_day').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/day.php?enmpe_pv='; ?>"+privatevalue);
					jQuery('.enmpe_report_week').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/week.php?enmpe_pv='; ?>"+privatevalue);
					jQuery('.enmpe_report_month').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/month.php?enmpe_pv='; ?>"+privatevalue);
					jQuery('.enmpe_report_quarter').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/quarter.php?enmpe_pv='; ?>"+privatevalue);
				} else {
					jQuery('.enmpe_report_day').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/day.php?enmpe_pwid='; ?>"+findvalue+"&amp;enmpe_pv="+privatevalue);
					jQuery('.enmpe_report_week').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/week.php?enmpe_pwid='; ?>"+findvalue+"&amp;enmpe_pv="+privatevalue);
					jQuery('.enmpe_report_month').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/month.php?enmpe_pwid='; ?>"+findvalue+"&amp;enmpe_pv="+privatevalue);
					jQuery('.enmpe_report_quarter').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/quarter.php?enmpe_pwid='; ?>"+findvalue+"&amp;enmpe_pv="+privatevalue);
				};
			});
			jQuery('#enmpe-private').change(function() {
				var findvalue = jQuery('#enmpe-embed-prayerwalls').val();
				var privatevalue = jQuery(this).val();
				if ( findvalue == 1 ) {
					jQuery('.enmpe_report_day').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/day.php?enmpe_pv='; ?>"+privatevalue);
					jQuery('.enmpe_report_week').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/week.php?enmpe_pv='; ?>"+privatevalue);
					jQuery('.enmpe_report_month').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/month.php?enmpe_pv='; ?>"+privatevalue);
					jQuery('.enmpe_report_quarter').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/quarter.php?enmpe_pv='; ?>"+privatevalue);
				} else {
					jQuery('.enmpe_report_day').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/day.php?enmpe_pwid='; ?>"+findvalue+"&amp;enmpe_pv="+privatevalue);
					jQuery('.enmpe_report_week').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/week.php?enmpe_pwid='; ?>"+findvalue+"&amp;enmpe_pv="+privatevalue);
					jQuery('.enmpe_report_month').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/month.php?enmpe_pwid='; ?>"+findvalue+"&amp;enmpe_pv="+privatevalue);
					jQuery('.enmpe_report_quarter').attr("href","<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/quarter.php?enmpe_pwid='; ?>"+findvalue+"&amp;enmpe_pv="+privatevalue);
				};
			});
		});
	</script>
	<h2 class="enmpe">Print Prayer Reports</h2>
	<p>Various printable reports are listed below. Select the report best suited for your needs, and click "Run This Report." Please note that all reports use the current date as the starting point.</p>
	<p><strong>Select a Prayer Wall:</strong>&nbsp;
		<select name="enmpe-embed-prayerwalls" id="enmpe-embed-prayerwalls" size="1">
			<?php foreach ( $enmpe_prayerwalls as $enmpe_pw ) { ?>
			<option value="<?php echo $enmpe_pw->pwid; ?>"><?php echo $enmpe_pw->wall_name; ?></option>
			<?php } ?>
			<option value="0">Display Requests from All Prayer Walls</option>			
		</select>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Include "DO NOT SHARE" Requests?:</strong>&nbsp;
		<select name="enmpe-private" id="enmpe-private" size="1">
			<option value="0">No</option>	
			<option value="1">Yes</option>		
		</select>
	</p>
	<br />
	<h3>Received in the Last Day</h3>
	<div style="margin: -40px 0 0 260px"><a href="<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/day.php'; ?>" class="button-secondary enmpe_report_day" target="_blank">Run This Report</a></div>
	<p>This report will return a list of all prayer requests received in the last day. Only moderated prayer requests that are publicly shared will appear in this report.</p>
	<br />
	
	<h3>Received in the Last Week</h3>
	<div style="margin: -40px 0 0 260px"><a href="<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/week.php'; ?>" class="button-secondary enmpe_report_week" target="_blank">Run This Report</a></div>
	<p>This report will return a list of all prayer requests received in the last week. Only moderated prayer requests that are publicly shared will appear in this report.</p>
	<br />
	
	<h3>Received in the Last Month</h3>
	<div style="margin: -40px 0 0 260px"><a href="<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/month.php'; ?>" class="button-secondary enmpe_report_month" target="_blank">Run This Report</a></div>
	<p>This report will return a list of all prayer requests received in the last month. Only moderated prayer requests that are publicly shared will appear in this report.</p>
	<br />
	
	<h3>Received in the Last Quarter</h3>
	<div style="margin: -40px 0 0 260px"><a href="<?php echo plugins_url() .'/prayerengine_plugin/includes/admin/reports/quarter.php'; ?>" class="button-secondary enmpe_report_quarter" target="_blank">Run This Report</a></div>
	<p>This report will return a list of all prayer requests received in the last quarter. Only moderated prayer requests that are publicly shared will appear in this report.</p>
	<?php include ('pecredits.php'); ?>	
</div>
<?php } // Deny access to sneaky people!
} else {
	exit("Access Denied");
} ?>