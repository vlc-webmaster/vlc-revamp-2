<?php /* Prayer Engine - Pagination for Various Pages */

global $wpdb;
if ( $wpdb != null ) { // Verify that user is allowed to access this page

$enmpe_display = 15; // How many records to display
$enmpe_countsql = "SELECT id FROM " . $wpdb->prefix . "prayers" . $enmpe_swid; 
$enmpe_runsqlcount = $wpdb->get_results( $enmpe_countsql );
$enmpe_prayercount = $wpdb->num_rows;

if ( isset($_GET['enmpe_p']) && isset($_GET['enmpe_did']) ) { // # of pages already been determined.
	if ($enmpe_prayercount > $enmpe_display) { // More than 1 page.
		$enmpe_pages = ceil($enmpe_prayercount/$enmpe_display);
	} else {
		$enmpe_pages = 1;
	}
} elseif ( isset($_GET['enmpe_p']) && is_numeric($_GET['enmpe_p']) ) {
	$enmpe_pages = strip_tags($_GET['enmpe_p']);
} else { // Need to determine # of pages.
	if ($enmpe_prayercount > $enmpe_display) { // More than 1 page.
		$enmpe_pages = ceil($enmpe_prayercount/$enmpe_display);
	} else {
		$enmpe_pages = 1;
	}
}

// Determine where in the database to start returning results...
if (isset($_GET['enmpe_c']) && is_numeric($_GET['enmpe_c'])) {
	if ( strip_tags($_GET['enmpe_c']) >= $enmpe_prayercount ) {
		$enmpe_start = strip_tags($_GET['enmpe_c']) - $enmpe_display;
	} else {
		$enmpe_start = strip_tags($_GET['enmpe_c']);
	}
} else {
	$enmpe_start = 0;
}

// Get records from database according to the page and display count
$enmpe_preparredsql = "SELECT * FROM " . $wpdb->prefix . "prayers" . $enmpe_swid . " ORDER BY id DESC LIMIT %d, %d"; 
$enmpe_sql = $wpdb->prepare( $enmpe_preparredsql, $enmpe_start, $enmpe_display );
$enmpe_prayers = $wpdb->get_results( $enmpe_sql );

// Deny access to sneaky people!
} else {
	exit("Access Denied");
}
?>