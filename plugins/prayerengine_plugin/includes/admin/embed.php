<?php /* Prayer Engine - Embed Instructions */
global $wp_version;
if ( $wp_version != null ) { // Verify that user is allowed to access this page
	global $wpdb;

	// Get All Prayer Walls
	$enmpe_preparredsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " ORDER BY pwid ASC"; 
	$enmpe_prayerwalls = $wpdb->get_results( $enmpe_preparredsql ); 
?>
<div class="wrap"> 
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('#enmpe-embed-prayerwalls').change(function() {
				var findvalue = jQuery(this).val();
				if ( findvalue == 1 ) {
					jQuery('#enmpe_shortcode').empty();
					jQuery('#enmpe_shortcode').append('Shortcode: &nbsp;<strong>[prayerengine]</strong>');
				} else {
					jQuery('#enmpe_shortcode').empty();
					jQuery('#enmpe_shortcode').append('Shortcode: &nbsp;<strong>[prayerengine_s enmpe_sw='+findvalue+']</strong>');
				};
			});
		});
	</script>
	<h2 class="enmpe">Embedding Your Prayer Walls</h2>
	
	<p>The Prayer Engine's public prayer walls will quickly become the most valuable tools in your prayer ministry. A prayer wall can be easily embedded within any page or post on your WordPress site by entering the following <a href="http://codex.wordpress.org/Shortcode" target="_blank">shortcode</a> into the visual editor:</p>
	<p>&nbsp;</p>
	<p>Select a Prayer Wall:&nbsp;
		<select name="enmpe-embed-prayerwalls" id="enmpe-embed-prayerwalls" size="1">
			<?php foreach ( $enmpe_prayerwalls as $enmpe_pw ) { ?>
			<option value="<?php echo $enmpe_pw->pwid; ?>"><?php echo $enmpe_pw->wall_name; ?></option>
			<?php } ?>
			<option value="0">Display Requests from All Prayer Walls</option>			
		</select>
	</p>
	<p id="enmpe_shortcode">Shortcode: &nbsp;<strong>[prayerengine]</strong></p>
	<p>&nbsp;</p>
	<p>You can place content above and below the shortcode to flesh out the page however you wish (just make sure the shortcode is on its own line). See the example below:</p>
	<p style="text-align: center"><img src="<?php echo plugins_url() .'/prayerengine_plugin/images/embed_example.jpg'; ?>" width="618" height="468" alt="Example of using the Prayer Engine shortcode" style="border: 5px solid #ECECEC" /></p>
	
	<h3>Styling Your Prayer Walls</h3>
	<p>There are two ways you can customize your prayer walls:</p>
	<p><strong>Change Colors in the Settings Menu</strong> - You can quickly (and easily!) modify all of The Prayer Engine's colors to match your theme by using the color picker found in <em><a href="<?php echo admin_url() . "options-general.php?page=enm_prayerengine"; ?>">Settings > The Prayer Engine</a></em>. No knowledge of HTML or CSS is required.</p>
	<p><strong>ADVANCED - Use Custom CSS</strong> - If you want complete control over the look and feel of your Prayer Engine prayer wall, enable the custom stylesheets in <em><a href="<?php echo admin_url() . "options-general.php?page=enm_prayerengine"; ?>">Settings > The Prayer Engine</a></em>. This will direct the prayer wall to two stylesheets in the plugin's 'custom > css' directory which you can modify as you see fit.</p> 
	<p><em>PLEASE NOTE: Plugin updates will overwrite your modifications to the custom stylesheets. Be sure to back them up before updating, and reapply your modifications when the update is complete.</em></p>
	
	<?php include ('pecredits.php'); ?>	
</div>
<?php // Deny access to sneaky people!
} else {
	exit("Access Denied");
} ?>