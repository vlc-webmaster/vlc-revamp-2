<?php /* Prayer Engine - Daily Report of All Public Prayer Requests */
	require_once 'report_header.php';
	
	if ( current_user_can( 'edit_pages' ) ) { 
		if ( isset($_GET['enmpe_pwid']) ) {
			if ( $_GET['enmpe_pwid'] > 0 ) {
				$enmpe_pw = " AND wall_id = " . strip_tags($_GET['enmpe_pwid']);
			} else {
				$enmpe_pw = " AND wall_id >= 0";
			}
		} else {
			$enmpe_pw = " AND wall_id = 1";
		}

		if ( isset($_GET['enmpe_pv']) ) {
			if ( $_GET['enmpe_pv'] > 0 ) {
				$enmpe_pv = " OR share_option = 'DO NOT Share Online'";
			} else {
				$enmpe_pv = null;
			}
		} else {
			$enmpe_pv = null;
		}

		$enmpe_findday = "SELECT * FROM " . $wpdb->prefix . "prayers" . " WHERE post_this = 1 AND (share_option = 'Share Online' OR share_option = 'Share Online Anonymously'" . $enmpe_pv . ") AND (DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= date_received || date_received = CURDATE())" . $enmpe_pw . " ORDER BY date_received DESC"; 
		$enmpe_prayers = $wpdb->get_results( $enmpe_findday );
		$enmpe_reportcount = $wpdb->num_rows;
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Prayer Engine - Requests Received in the Last Month</title>
	<link rel="stylesheet" href="../../../css/pe_backend.css" type="text/css" />
</head>
<body id="pe-report">

<h1>Prayer Requests From the Last Month</h1>

<?php include 'format_report_results.php'; ?>

</body>
</html>
<?php } else {
	$enmpe_redirecturl = home_url();
	header("Location: $enmpe_redirecturl");
} ?>