<?php

require '../../../../../../wp-blog-header.php'; // ADJUST THIS PATH if using a non-standard WordPress install

if ( current_user_can( 'edit_pages' ) ) { 

	$enmpe_options = get_option( 'enm_prayerengine_options' ); 
	$enmpe_ministryname = $enmpe_options['ministryname'];

	global $wpdb;
	if ( !defined('MARKDOWN_VERSION') ) {
		require_once '../markdown.php';
	}
}

?>