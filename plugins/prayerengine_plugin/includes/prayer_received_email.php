<?php /* Prayer Engine - Send Email to User When Prayer Request is Received */

global $wp_version;
if ( $wp_version != null ) {
	
	if ( !defined('ENMPE_EMAIL_FIND_PAGE') ) {	
		function enmpe_email_find_page() {
			define('ENMPE_EMAIL_FIND_PAGE', 'yes');
			$enmpe_get_url = parse_url( strtok( $_SERVER['REQUEST_URI'], '&' ) );
			if ( $enmpe_get_url['query'] == null ) {
				return "http://" . $_SERVER['SERVER_NAME'] . strtok( $_SERVER['REQUEST_URI'], '&' ) . "?enmpe=1";
			} else {
				return "http://" . $_SERVER['SERVER_NAME'] . strtok( $_SERVER['REQUEST_URI'], '&' );
			}
		}
	}

$enmpe_delete_url = enmpe_email_find_page();

$enmpe_email_message = "Thank you for sharing your prayer request at " . home_url() . "\n\n";
$enmpe_email_message .= "Your Name: " . $enmpe_name . "\n";
$enmpe_email_message .= "Received: " . date('F j, Y', strtotime($enmpe_datereceived)) . "\n";
$enmpe_email_message .= "Your Email: " . $enmpe_email . "\n";
$enmpe_email_message .= "Your Phone Number: " . $enmpe_phone . "\n\n";
$enmpe_email_message .= stripslashes($enmpe_prayer) . "\n\n";
$enmpe_email_message .= "Your Sharing Instructions: Please " . $enmpe_shareoption . "\n\n";
$enmpe_email_message .= "Your prayer request is currently being moderated, and will then be shared according to your instructions. You may remove this prayer request from our Prayer Wall at any time by clicking the following link (this cannot be undone):\n\n";
$enmpe_email_message .= $enmpe_delete_url . "&enmpe_e=" . urlencode($enmpe_email) . "&enmpe_dc=" . $enmpe_dc . " \n\n";
$enmpe_email_message .= "If your prayer request has been answered, please let us know by clicking this link:\n\n";
$enmpe_email_message .= $enmpe_delete_url . "&enmpe_e=" . urlencode($enmpe_email) . "&enmpe_ac=" . $enmpe_dc . " \n\n";
$enmpe_email_message .= "This is an automated notification sent from " . home_url() . ". Please do not respond to this email, as this account is not monitored.";

$enmpe_email_to = $enmpe_email; 
$enmpe_email_subject = 'Your Prayer Request Has Been Received'; 
$enmpe_email_header = 'From: "' . $enmpe_ministry_name . '" <' . $enmpe_admin_email . '>'; 
wp_mail( $enmpe_email_to, $enmpe_email_subject, $enmpe_email_message, $enmpe_email_header ); 

// Deny access to sneaky people!
} else {
	exit("Access Denied");
}

?>