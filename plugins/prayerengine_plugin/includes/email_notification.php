<?php /* Prayer Engine - Send Email to Administrators When New Prayer Request Received */
	
	if ( $wp_version != null ) {
	if ( $wp_version >= 3.1 ) {
	    $enmpe_users = get_users( 
			array( 
				'meta_key' => 'prayerengine_admin_email',
				'meta_value' => '1',
				'meta_compare' => '='
			) 
		);
	} else {
		$enmpe_users = get_users_of_blog();
	}

	if ( !defined('ENMPE_EMAIL_FIND_PAGE_TWO') ) {	
		function enmpe_email_find_page_two() {
			define('ENMPE_EMAIL_FIND_PAGE_TWO', 'yes');
			$enmpe_get_url = parse_url( strtok( $_SERVER['REQUEST_URI'], '&' ) );
			if ( $enmpe_get_url['query'] == null ) {
				return "http://" . $_SERVER['SERVER_NAME'] . strtok( $_SERVER['REQUEST_URI'], '&' ) . "?enmpe=1";
			} else {
				return "http://" . $_SERVER['SERVER_NAME'] . strtok( $_SERVER['REQUEST_URI'], '&' );
			}
		}
	}

	$enmpe_mod_url = enmpe_email_find_page_two();
	$enmpe_rid = $wpdb->insert_id; 
	
	if ( is_array( $enmpe_users ) ) { /* Loop through each user. */
		$enmpe_moderate_url = wp_login_url( admin_url() . "admin.php?page=prayerengine_plugin/prayerengine_plugin.php&enmpe_new=yes&enmpe_pid=" . $enmpe_rid ); 
		if ( $wp_version >= 3.1 ) { // If a recent version of WordPress
			foreach ( $enmpe_users as $user ) { 
				$enmpe_admin_message = "A new prayer request has been received at " . home_url() . "\n\n";
				$enmpe_admin_message .= "From: " . $enmpe_name . "\n";
				$enmpe_admin_message .= "Prayer Wall: " . stripslashes($enmpe_prayerwall->wall_name) . "\n";
				$enmpe_admin_message .= "Received: " . date('F j, Y', strtotime($enmpe_datereceived)) . "\n";
				$enmpe_admin_message .= "Their Email: " . $enmpe_email . "\n";
				$enmpe_admin_message .= "Their Phone Number: " . $enmpe_phone . "\n\n";
				$enmpe_admin_message .= stripslashes($enmpe_prayer) . "\n\n";
				$enmpe_admin_message .= "Please " . $enmpe_shareoption . "\n\n";
				if ($enmpe_moderation != 0) {
				$enmpe_admin_message .= "To INSTANTLY POST this request as is, click the link below (you can still log in to moderate at any time):\n";
				$enmpe_admin_message .= $enmpe_mod_url . "&enmpe_id=" . $enmpe_rid . "&enmpe_mc=" . $enmpe_mc . " \n\n";
				};
				$enmpe_admin_message .= "To moderate this prayer request, please log in using the link below:\n";
				$enmpe_admin_message .= $enmpe_moderate_url;

				$enmpe_admin_email_to = $user->user_email; 
				$enmpe_admin_email_subject = 'New Prayer Request Received'; 
				$enmpe_admin_email_header = 'From: "' . $enmpe_ministry_name . '" <' . $enmpe_admin_email . '>'; 
				wp_mail( $enmpe_admin_email_to, $enmpe_admin_email_subject, $enmpe_admin_message, $enmpe_admin_email_header ); 
			}
		} else { // If they're using an older version
			foreach ( $enmpe_users as $user ) { 
				$enmpe_check_email_settings = get_usermeta( $user->ID, 'prayerengine_admin_email', true );
				if ( $enmpe_check_email_settings == 1 ) {
					$enmpe_admin_message = "A new prayer request has been received at " . home_url() . "\n\n";
					$enmpe_admin_message .= "From: " . $enmpe_name . "\n";
					$enmpe_admin_message .= "Prayer Wall: " . stripslashes($enmpe_prayerwall->wall_name) . "\n";
					$enmpe_admin_message .= "Received: " . date('F j, Y', strtotime($enmpe_datereceived)) . "\n";
					$enmpe_admin_message .= "Their Email: " . $enmpe_email . "\n";
					$enmpe_admin_message .= "Their Phone Number: " . $enmpe_phone . "\n\n";
					$enmpe_admin_message .= stripslashes($enmpe_prayer) . "\n\n";
					$enmpe_admin_message .= "Please " . $enmpe_shareoption . "\n\n";
					$enmpe_admin_message .= "To moderate this prayer request, please log in using the link below:\n";
					$enmpe_admin_message .= $enmpe_moderate_url;

					$enmpe_admin_email_to = $user->user_email; 
					$enmpe_admin_email_subject = 'New Prayer Request Received'; 
					$enmpe_admin_email_header = 'From: "' . $enmpe_ministry_name . '" <' . $enmpe_admin_email . '>'; 
					wp_mail( $enmpe_admin_email_to, $enmpe_admin_email_subject, $enmpe_admin_message, $enmpe_admin_email_header );
				}
			}
		}

	}
	// Deny access to sneaky people!
	} else {
		exit("Access Denied");
	}

?>