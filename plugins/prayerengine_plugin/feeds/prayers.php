<?php
/**
 * Prayer Feed for The Prayer Engine plugin.
 *
 * @package WordPress
 */

header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);
$more = 1;

global $wpdb;

if ( isset($_GET['pwid']) && is_numeric($_GET['pwid']) ) { // Find the relevant prayer wall
		$enmpe_sw = $_GET['pwid'];
		if ( $enmpe_sw == 0 ) {
			$enmpe_findtheprayerwallsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " WHERE pwid = %d"; 
			$enmpe_findtheprayerwall = $wpdb->prepare( $enmpe_findtheprayerwallsql, 1 );
			$enmpe_prayerwall = $wpdb->get_row( $enmpe_findtheprayerwall, OBJECT );
		} else {
			$enmpe_findtheprayerwallsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " WHERE pwid = %d"; 
			$enmpe_findtheprayerwall = $wpdb->prepare( $enmpe_findtheprayerwallsql, $enmpe_sw );
			$enmpe_prayerwall = $wpdb->get_row( $enmpe_findtheprayerwall, OBJECT );
		}
	} else {
		$enmpe_findtheprayerwallsql = "SELECT * FROM " . $wpdb->prefix . "pe_prayerwalls" . " WHERE pwid = %d"; 
		$enmpe_findtheprayerwall = $wpdb->prepare( $enmpe_findtheprayerwallsql, 1 );
		$enmpe_prayerwall = $wpdb->get_row( $enmpe_findtheprayerwall, OBJECT );
}

if ( isset($_GET['pwid']) && is_numeric($_GET['pwid']) ) {
	if ( $enmpe_sw == 0 ) {
		$enmpe_swid = ">= 1";
		$enmpe_wallid = 1;
	} else {
		$enmpe_swid = "= " . $enmpe_sw;
		$enmpe_wallid = $enmpe_sw;
	}
} else {
	$enmpe_swid = "= 1";
	$enmpe_wallid = 1;
}

$peoptions = get_option( 'enm_prayerengine_options' ); 
$pe_feedtitle = $peoptions['feedtitle'];
$pe_feeddescription = $peoptions['feeddescription'];
$pe_ministryname = $peoptions['ministryname'];

if ( $enmpe_prayerwall->displaylimit != NULL ) { // How far back should we display prayer requests?
	$enmpe_displaylimit = $enmpe_prayerwall->displaylimit;
} else {
	$enmpe_displaylimit = "nolimit";
}

if ( $enmpe_displaylimit == "3days" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 2 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "1week" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 6 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "2weeks" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 13 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "month" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "2months" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 60 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "3months" ) {
	$enmpe_displaylimitsql = "AND (DATE_SUB(CURDATE(),INTERVAL 90 DAY) <= date_received || date_received = CURDATE())";
} elseif ( $enmpe_displaylimit == "nolimit" ) {
	$enmpe_displaylimitsql = "";
}

$pe_feed_sql = "SELECT id, share_option, name, posted_prayer, date_received FROM " . $wpdb->prefix . "prayers" . " WHERE post_this = 1 AND (share_option = 'Share Online' OR share_option = 'Share Online Anonymously') " . $enmpe_displaylimitsql . " AND wall_id " . $enmpe_swid . " ORDER BY date_received DESC LIMIT 50"; 
$pe_feed_prayers = $wpdb->get_results( $pe_feed_sql );

echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title><?php echo stripslashes($pe_feedtitle) ?></title>
    <link><?php self_link(); ?></link>
    <description><?php echo stripslashes($pe_feeddescription) ?></description>
    <copyright><?php echo $pe_ministryname ?></copyright>
    <language>en-us</language>
	<atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
	<?php foreach ( $pe_feed_prayers as $prayer ) { ?>
		<item>
	      <title><?php if (($prayer->share_option == "Share Online Anonymously") || (($prayer->share_option == "Share Online") && (strlen($prayer->name) == 0))) { ?>Anonymous Prayer Request<?php } else { ?>Prayer Request From <?php echo htmlspecialchars(stripslashes($prayer->name)) ?> <?php } ?></title>
	      <description><?php echo htmlspecialchars(stripslashes($prayer->posted_prayer)) ?></description>
	      <pubDate><?php echo date('D, d M Y H:i:s O', strtotime($prayer->date_received)) ?></pubDate>
	      <link><?php self_link() ?></link>
		  <guid><?php echo self_link() . $prayer->id ?></guid>
	    </item>
	<?php } ?>
</channel>
</rss>