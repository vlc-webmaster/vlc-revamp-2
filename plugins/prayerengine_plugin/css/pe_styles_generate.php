<?php 

$enmpe_options = $data; 

if ( isset($enmpe_options['formicon']) ) {
	$enmpe_formicon = $enmpe_options['formicon'];
} else {
	$enmpe_formicon = 'light';
}
if ( isset($enmpe_options['explorebg']) ) {
	$enmpe_explorebg = $enmpe_options['explorebg'];
} else {
	$enmpe_explorebg = 'D4D4D4';
}
$enmpe_ftbg = $enmpe_options['ftbg'];
$enmpe_ftt = $enmpe_options['ftt'];
$enmpe_ft = $enmpe_options['ft'];
$enmpe_fbg = $enmpe_options['fbg'];
$enmpe_ffbdr = $enmpe_options['ffbdr'];
$enmpe_fib = $enmpe_options['fib'];
$enmpe_fit = $enmpe_options['fit'];
$enmpe_ffbg = $enmpe_options['ffbg'];
$enmpe_fft = $enmpe_options['fft'];
$enmpe_fsubbg = $enmpe_options['fsubbg'];
$enmpe_fsubt = $enmpe_options['fsubt'];
$enmpe_pag = $enmpe_options['pag'];
$enmpe_prbg = $enmpe_options['prbg'];
$enmpe_aprbg = $enmpe_options['aprbg'];
$enmpe_pbg = $enmpe_options['pbg'];
$enmpe_pt = $enmpe_options['pt'];
$enmpe_prn = $enmpe_options['prn'];
$enmpe_prt = $enmpe_options['prt'];
$enmpe_prd = $enmpe_options['prd'];
$enmpe_fbdr = $enmpe_options['fbdr'];
$enmpe_pbdr = $enmpe_options['pbdr'];
$enmpe_ermsg = $enmpe_options['ermsg'];
$enmpe_ermsgt = $enmpe_options['ermsgt'];
$enmpe_sumsg = $enmpe_options['sumsg'];
$enmpe_sumsgt = $enmpe_options['sumsgt'];
$enmpe_prdis = $enmpe_options['prdis'];
$enmpe_prdist = $enmpe_options['prdist'];
$enmpe_nchigh = $enmpe_options['nchigh'];
if ( isset($enmpe_options['patext']) ) {
	$enmpe_patext = $enmpe_options['patext'];
} else {
	$enmpe_patext = 'ffffff';
}
if ( isset($enmpe_options['pabg']) ) {
	$enmpe_pabg = $enmpe_options['pabg'];
} else {
	$enmpe_pabg = '919191';
}
$enmpe_pagt = $enmpe_options['pagt'];
$enmpe_copyt = $enmpe_options['copyt'];
$enmpe_copyh = $enmpe_options['copyh'];
if ( isset($enmpe_options['credits']) ) {
	$enmpe_logocolor = $enmpe_options['credits'];
} else {
	$enmpe_logocolor = "light";
}
if ( isset($enmpe_options['spag']) ) {
	$enmpe_spag = $enmpe_options['spag'];
} else {
	$enmpe_spag = 'f1f1f1';
}
if ( isset($enmpe_options['spagt']) ) {
	$enmpe_spagt = $enmpe_options['spagt'];
} else {
	$enmpe_spagt = 'D4D4D4';
}


?>/* Prayer Engine - Styles for Main Prayer Wall 
DO NOT EDIT - Your changes will be lost with plugin updates */

#prayerengine {
	margin: 0;
	padding: 0;
	font-family: Arial, Helvetica, sans-serif;
}

#prayerengine * {
	-webkit-box-sizing: content-box !important;
	-moz-box-sizing:    content-box !important;
	box-sizing:         content-box !important;
	-webkit-hyphens: none;
}

#prayerengine h1, #prayerengine h2, #prayerengine h3, #prayerengine h4, #prayerengine h5, #prayerengine h6, #prayerengine p, #prayerengine form, #prayerengine ul, #prayerengine ol, #prayerengine li, #prayerengine ol li, #prayerengine ul li, #prayerengine blockquote, #prayerengine input, #prayerengine input[type="submit"], #prayerengine textarea, #prayerengine select, #prayerengine label, #prayerengine table, #prayerengine table tr, #prayerengine table tr td.label, #prayerengine table tr td.prayercell, #prayerengine table tr td.inputcell, #prayerengine table tr td.twittercell, #prayerengine table tr td.optioncell  { /* resets most browser styles to enhance cross-browser compatibility */
	margin: 0;
	padding: 0;
	text-transform: none;
	letter-spacing: 0;
	line-height: 1;
	clear: none;
	font-weight: 300;
	font-family: Arial, Helvetica, sans-serif !important;
	font-variant: normal;
	float: none;
	border: none;
	-moz-border-radius: 0;
	-webkit-border-radius: 0;
	background: none;
	min-height: 0;
	text-align: left;
	text-shadow: none;
	box-shadow: none;
	font-style: normal;
}

#prayerengine table tr td.spamcell  { /* resets most browser styles to enhance cross-browser compatibility */
	margin: 0;
	padding: 0;
	text-transform: none;
	letter-spacing: 0;
	line-height: 1;
	clear: none;
	font-weight: 300;
	font-family: Arial, Helvetica, sans-serif;
	font-variant: normal;
	float: none;
	border: none;
	-moz-border-radius: 0;
	-webkit-border-radius: 0;
	min-height: 0;
	text-align: left;
}

#prayerengine table tr td.spamcell iframe {
	width: 180px !important;
}

#prayerengine br {
	display: none;
}

#prayerengine .optionarea br {
	display: block;
}

#recaptcha_image img { width: 180px; box-shadow: none; border-radius: 0; border: none; } 


#prayerengine label {
	display: inline;
}

#prayerengine a {
	border: none !important;
}
	

/* ---------- Prayer Engine Structure ---------- */

#pe-container {
	min-width: 480px;
	margin: 0;
	padding: 20px 0 20px 0;
}

/* ---------- PRAYER REQUEST FORM ---------- */

#prayerengine .pe-explore-bar {
	background-color: #<?php echo $enmpe_explorebg; ?>;
	height: 40px;
	padding: 10px;
}

#prayerengine .pe-explore-bar h4.pe-form-toggle {
	margin: 0;
	padding: 0;
	position: absolute;
}

#prayerengine .pe-explore-bar h4.pe-form-toggle a {
	display: block;
	width: 240px;
	padding: 0 0 0 39px;
	height: 40px;
	border-radius: 20px;
	<?php if ( $enmpe_formicon == "light" ) { ?>
	background: url('../images/light_contact.png') no-repeat;
	<?php } else { ?>
	background: url('../images/dark_contact.png') no-repeat;
	<?php } ?>
	background-size: 18px 18px;
	background-position: 14px 11px;
	background-color: #<?php echo $enmpe_ftbg; ?>;
	float: left;
	text-decoration: none;
	line-height: 40px;
	text-transform: uppercase;
	color: #<?php echo $enmpe_ftt; ?>;
	font-size: 14px !important;
}

#pe-submit-container {
	padding: 0;
}

#pe-form-container {
	padding: 0;
	display: none;
	background-color: #<?php echo $enmpe_fbg; ?>; 
}

#pe-twitter-area {
}

#pe-submit-area {
	text-align: right;
	padding: 10px;
	height: 40px;
	margin: 10px 0 0 0;
	background-color: #<?php echo $enmpe_ffbg; ?>;
}

/* Instructions */

#prayerengine #pe-form-container p.instructions {
	font-size: 14px !important;
	line-height: 130%;
	margin: 0 0 15px 0;
	padding: 10px 15px 0 15px;
	color: #<?php echo $enmpe_ft; ?>;
}

/* Table styles to nicely space the form */

#pe-form-container table {
	margin: 0 0 0 7px;
	width: 100%;
}

#pe-form-container table tr td {
	border: none !important;
	background: none !important;
}

#pe-form-container table tr td.inputcell, #pe-form-container table tr td.optioncell, #pe-form-container table tr td.prayercell, #pe-form-container table tr td.spamcell {
	padding: 5px;
}

#pe-form-container table tr td.label {
	white-space: normal;
	padding: 18px 10px 0 0;
	width: 100px;
	vertical-align: top;
	text-align: right;
	font-size: 14px !important;
}

#pe-form-container table tr td.label.dropdown {
	padding: 10px 10px 0 0;
}

#pe-form-container table tr td.inputcell {
	vertical-align: top;
	width: 80%;
}

#pe-form-container table tr td.prayercell, #pe-form-container table tr td.twittercell{
	padding: 5px 25px 5px 5px;
	width: 80%;
}

#pe-form-container table tr td.spamcell {
	padding: 10px 25px 5px 5px;
	width: 80%;
}

/* Label styles for the form */

#pe-form-container label {
	font-size: 14px !important;
	font-weight: 700;
	color: #<?php echo $enmpe_ft; ?>;
}

#pe-form-container label.checkbox {
	font-size: 14px !important;
	font-weight: 300;
	padding: 0 5px 0 0;
	color: #<?php echo $enmpe_ft; ?>;
	margin: -5px 0 0 0;
}

#pe-form-container label.error {
	display: block;
	padding: 2px 0 0 0;
	font-size: 11px !important;
	font-style: italic;
}

/* Input styles for the form */

#pe-form-container input, #pe-form-container textarea, #pe-form-container select {
	font-family: Arial, Helvetica;
	font-size: 15px !important;
	line-height: 120%;
	padding: 5px;
	-moz-box-shadow: none;
	-webkit-box-shadow: none;
	box-shadow: none;
	border: 2px solid #<?php echo $enmpe_ffbdr; ?>; 
	background-color: #<?php echo $enmpe_fib; ?>; 
	color: #<?php echo $enmpe_fit; ?>;
	-webkit-appearance: none;
}

#pe-form-container select {
	-webkit-appearance: menulist;
}

#pe-form-container input:focus, #pe-form-container textarea:focus, #pe-form-container select:focus {
	outline: none;
}

#pe-form-container input {
	font-size: 15px !important;
	padding: 5px 10px 5px 10px;
	height: 24px;
	width: 60%;
}

#pe-form-container input.check {
	width: 20px;
	display: inline-block;
	margin: 6px 0 0 0 !important;
	padding: 5px 0 0 0 !important;
	height: 14px;
	-webkit-appearance: checkbox;
	background: none;
}

#prayerengine #pe-form-container input.pe-submit, #prayerengine input[type="submit"] {
	display: block;
	float: right;
	width: 180px;
	height: 40px;
	line-height: 40px;
	background-color: #<?php echo $enmpe_fsubbg; ?>; 
	color: #<?php echo $enmpe_fsubt; ?>;
	text-align: center;
	text-transform: uppercase;
	margin: 0;
	border-radius: 20px;
	font-size: 14px !important;
	-webkit-appearance: none;
	margin: 0 !important;
	padding: 0 !important;
}

#pe-form-container textarea {
	font-size: 15px !important;
	padding: 10px;
	height: 150px;
	width: 92%;
}

#pe-form-container textarea#prayer {
	height: 140px;
}

/* Styles for the optional Twitter area */

#pe-twitter-area table tr td.label {
	white-space: normal;
	padding: 18px 10px 0 0;
	width: 100px;
	vertical-align: top;
	text-align: right;
	font-size: 14px !important;
}

#pe-form-container #pe-twitter-area textarea {
	height: 50px;
}

#prayerengine #pe-form-container #pe-twitter-area p.twitter-counter {
	padding: 6px 0 0 0;
	font-size: 12px !important;
	font-style: italic;
	color: #<?php echo $enmpe_ft; ?>;
}

/* PHP Error and Success Messages */

#pe-form-container #errors {
	width: 100%;
	padding: 15px 0 2px 0;
	background-color: #<?php echo $enmpe_ermsg; ?>; 
	color: #<?php echo $enmpe_ermsgt; ?>;
}

#prayerengine #pe-form-container #errors p {
	font-size: 14px !important;
	margin: 0 15px 15px 15px;
	font-weight: 700;
}

#prayerengine #pe-form-container #errors ul {
	margin: 10px 0 0 0;
}

#prayerengine #pe-form-container #errors ul li {
	margin: 0 10px 5px 30px !important;
	padding: 0;
	font-size: 14px !important;
	list-style-type: disc;
	line-height: 100%;
}

#pe-form-container #success {
	width: 100%;
	padding: 10px 0 10px 0;
	background-color: #<?php echo $enmpe_sumsg; ?>; 
	color: #<?php echo $enmpe_sumsgt; ?>;
}

#prayerengine #pe-form-container #success p {
	font-size: 14px !important;
	margin: 0 15px 0 15px;
	line-height: 120%;
	font-weight: 700;
}

/* ---------- PRAYER REQUEST LISTS ---------- */

#pe-prayer-list {
	margin: 15px 0 15px 0;
}

#prayerengine .pe-prayer-even {
	padding: 15px;
	background-color: #<?php echo $enmpe_aprbg; ?>;
}

#prayerengine .pe-prayer-odd {
	padding: 15px;
	background-color: #<?php echo $enmpe_prbg; ?>; 
}

#prayerengine .pe-prayer-even h3, #prayerengine .pe-prayer-odd h3 {
	font-size: 16px !important;
	margin: 0 0 10px 0;
	padding: 6px 0 0 0;
	font-weight: 700;
	display: block;
	color: #<?php echo $enmpe_prn; ?>;
}

#prayerengine .pe-prayer-even h3.pe-name, #prayerengine .pe-prayer-odd h3.pe-name {
	font-size: 16px !important;
	margin: 0 0 10px 0;
	padding: 6px 0 0 0;
	font-weight: 700;
	display: block;
}

#prayerengine .pe-prayer-even p, #prayerengine .pe-prayer-odd p {
	font-size: 14px !important;
	margin: 0 0 12px 0;
	padding: 0;
	line-height: 130%;
	clear: right;
	color: #<?php echo $enmpe_prt; ?>;
}

#prayerengine .pe-prayer-even h4, #prayerengine .pe-prayer-odd h4 {
	font-size: 12px !important;
	color: #<?php echo $enmpe_prd; ?>;
}

#prayerengine .pe-count-area {
	float: right;
	width: 316px;
	margin: 0 0 10px 0;
}

#prayerengine .pe-count-area h4 {
	font-size: 13px !important;
	font-weight: 300;
	text-align: right;
	padding: 10px 0 0 0;
	color: #<?php echo $enmpe_pbg; ?>;
}

#prayerengine .pe-count-area h4 strong {
	font-weight: 700;
	color: #<?php echo $enmpe_nchigh; ?>;
}

#prayerengine .pe-count-area form a.submitlink {
	display: block;
	width: 166px;
	height: 30px;
	line-height: 30px;
	border-radius: 15px;
	text-decoration: none;
	text-align: center;
	text-transform: uppercase;
	font-size: 13px !important;
	float: right;
	margin: 0 0 0 20px;
	color: #<?php echo $enmpe_pt; ?>;
}

#prayerengine .pe-count-area a.submitlink:link {background-color: #<?php echo $enmpe_pbg; ?>;}
#prayerengine .pe-count-area a.submitlink:visited {background-color: #<?php echo $enmpe_pbg; ?>;}
#prayerengine .pe-count-area a.submitlink:hover {background-color: #<?php echo $enmpe_pbg; ?>;}
#prayerengine .pe-count-area a.submitlink:active {background-color: #<?php echo $enmpe_pbg; ?>;}

#prayerengine .pe-count-area blockquote {
	display: block;
	width: 166px;
	height: 30px;
	line-height: 30px;
	border-radius: 15px;
	text-decoration: none;
	text-align: center;
	text-transform: uppercase;
	font-style: italic;
	font-size: 13px !important;
	float: right;
	margin: 0 0 0 20px;
	background-color: #<?php echo $enmpe_prdis; ?>;
	color: #<?php echo $enmpe_prdist; ?>;
}

/* ---------- PAGINATION ---------- */

#prayerengine .pe-pagination {
	text-align: center;
	padding: 10px 0 0 0;
	font-family: Arial, Helvetica, San-serif;
}

#prayerengine .pe-pagination .pe-current-page {
	display: inline-block;
	width: 30px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	text-decoration: none;
	background-color: #<?php echo $enmpe_spag; ?>; 
	color: #<?php echo $enmpe_spagt; ?>;
	border-radius: 15px;
	padding: 0;
	font-size: 14px !important;
}

#prayerengine .pe-pagination a {
	display: inline-block;
	width: 30px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	text-decoration: none;
	color: #<?php echo $enmpe_spagt; ?>;
	border-radius: 15px;
	padding: 0;
	font-size: 14px !important;
}

#prayerengine .pe-pagination a span {
	display: none;
}

#prayerengine .pe-pagination a.next.page-numbers, #prayerengine .pe-pagination a.previous.page-numbers {
	display: inline-block;
	width: 30px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	text-decoration: none;
	background-color: #<?php echo $enmpe_pag; ?>; 
	color: #<?php echo $enmpe_pagt; ?>;
	border-radius: 15px;
	padding: 0;
	font-size: 14px !important;
}

/* --------- PRAYER ENGINE BRANDING ---------- */

#prayerengine h3.enmpe-poweredby {
	margin: 5px 0 0 0 !important;
	text-indent: -9000px;
	width: 148px;
	height: 40px;
	<?php if ( $enmpe_logocolor == "light" ) { ?>
	background: url('../images/pe_light_poweredby.png') no-repeat;	
	<?php } elseif ( $enmpe_logocolor == "dark" ) { ?>
	background: url('../images/pe_dark_poweredby.png') no-repeat;
	<?php } ?>
	float: right;
	padding: 0;
}

#prayerengine h3.enmpe-poweredby a {
	display: block;
	width: 148px;
	height: 40px;
}

#prayerengine p.enmpe-poweredbytext {
	margin: 5px 0 10px 0;
	text-align: right;
	font-size: 13px !important;
	color: #<?php echo $enmpe_copyt; ?>;
}

#prayerengine p.enmpe-poweredbytext a:link, #prayerengine p.enmpe-poweredbytext a:visited, #prayerengine p.enmpe-poweredbytext a:hover, #prayerengine p.enmpe-poweredbytext a:active {
	color: #<?php echo $enmpe_copyt; ?>;
}

/* ----------- reCAPTCHA STYLES ---------- */

#prayerengine #perecaptcha_widget {
	width: 200px;
}

#prayerengine #perecaptcha_widget #recaptcha_image {
	margin: 0 0 10px 0;
	width: 180px !important;
}

#prayerengine #perecaptcha_widget span.recaptcha_only_if_image, #prayerengine #perecaptcha_widget span.recaptcha_only_if_audio {
	font-size: 15px !important;
	display: block;
	margin: 0 0 4px 0;
	color: #<?php echo $enmpe_ft; ?>;
}

#prayerengine #perecaptcha_widget #recaptcha_response_field {
	width: 170px;
}

span.perecaptcha_options {
	font-size: 14px !important;
	padding: 6px 0 1px 0;
	font-weight: 700;
	display: block;
	color: #<?php echo $enmpe_ft; ?>;
}

div.recaptcha_only_if_image a, div.recaptcha_only_if_audio a, div.perecaptcha_link a {
	font-size: 14px !important;
	text-decoration: underline;
	color: #<?php echo $enmpe_ft; ?>;
}

/* Version 2.0 */

#prayerengine .pe-prayer-even h4.answered, #prayerengine .pe-prayer-odd h4.answered {
	display: block;
	padding: 5px 3px 5px 3px;
	text-align: center;
	font-size: 14px !important;
	margin: 0 0 6px 0;
	text-transform: uppercase;
	color: #<?php echo $enmpe_patext; ?>;
	background-color: #<?php echo $enmpe_pabg; ?>;
}

/* ----- For Themes with Mobile Views ----- */

@media (max-width: 700px) {
	#pe-container {
		min-width: 100px;
	}

	#prayerengine .pe-explore-bar {
		padding: 10px 0 10px 0;
	}

	#prayerengine .pe-explore-bar h4.pe-form-toggle {
		margin: 0;
		padding: 0;
		position: relative;
		width: 259px;
		margin: 0 auto;
	}

	#prayerengine .pe-explore-bar h4.pe-form-toggle a {
		display: block;
		width: 220px;
		font-size: 13px !important;
	}

	#prayerengine .pe-count-area {
		float: none;
		width: 100%;
		margin: 0 0 10px 0;
	}

	#prayerengine .pe-count-area h4 {
		font-size: 13px !important;
		font-weight: 300;
		text-align: center;
		padding: 6px 0 0 0;
	}

	#prayerengine .pe-count-area h4 strong {
		font-weight: 700;
	}	

	#prayerengine .pe-count-area form a.submitlink {
		display: block;
		width: 166px;
		height: 30px;
		line-height: 30px;
		border-radius: 15px;
		text-decoration: none;
		text-align: center;
		text-transform: uppercase;
		font-size: 13px !important;
		float: none;
		margin: 0 auto;
		color: #<?php echo $enmpe_pt; ?>;
	}

	#prayerengine .pe-count-area blockquote {
		display: block;
		width: 166px;
		height: 30px;
		line-height: 30px
		text-decoration: none;
		text-align: center;
		text-transform: uppercase;
		font-style: italic;
		font-size: 13px !important;
		float: none;
		margin: 0 auto;
	}

	/* Instructions */

	#pe-submit-area {
		text-align: center;
		padding: 10px;
		height: 40px;
		margin: 10px 0 0 0;
	}

	/* Input styles for the form */

	#pe-form-container table {
		margin: 0 0 0 10px;	
		width: auto;
	}

	#pe-form-container table tr td {
		display: block;
		text-align: left;
		padding: 5px;
		width: 100%;
		border: none !important;
	}

	#pe-form-container table tr td.label, #pe-form-container table tr td.label.dropdown, #pe-form-container table tr td.inputcell, #pe-form-container table tr td.optioncell, #pe-form-container table tr td.prayercell, #pe-form-container table tr td.spamcell, #pe-form-container table tr td.twittercell {
		text-align: left;
		padding: 5px;
		width: 100%;
	}

	#pe-form-container table tr td.label.spam {
		text-align: left;
		padding: 10px 5px 5px 5px;
		width: 100%;
	}

	#pe-form-container table tr td.label.empty {
		display: none;
	}

	#pe-form-container table tr td input {
		width: 78%;
		margin: 0 0 8px 0;
		font-size: 16px !important;
	}

	#pe-form-container table tr td select {
		width: 82%;
		margin: 0 0 8px 0;
		font-size: 16px !important;
	}

	#pe-form-container table tr td textarea, #pe-form-container table tr td.twittercell textarea {
		width: 78%;
		margin: 0 0 8px 0;
		font-size: 16px !important;
	}

	#pe-form-container label.checkbox {
		
	}

	#pe-form-container input.check {
		width: 20px;
		display: inline-block;
		margin: 6px 0 0 0;
		padding: 5px 0 0 0;
		height: 14px;
		font-size: 30px;
	}

	#pe-form-container input[type="text"], #pe-form-container textarea {
		-webkit-appearance: none;
	}

	#prayerengine #pe-form-container input.pe-submit, #prayerengine input[type="submit"] {
		display: block;
		float: none;
		width: 180px;
		height: 40px;
		line-height: 40px;
		background-color: #<?php echo $enmpe_fsubbg; ?>; 
		color: #<?php echo $enmpe_fsubt; ?>;
		text-align: center;
		text-transform: uppercase;
		margin: 0 auto !important;
		border-radius: 20px;
		font-size: 14px !important;
		-webkit-appearance: none;
	}

	#prayerengine a.pepnumber, #prayerengine .pe-pagination span.pe-current-page {
		display: none;
	}

	#prayerengine .pe-pagination a.next.page-numbers, #prayerengine .pe-pagination a.previous.page-numbers {
		display: inline-block;
		width: 70px;
		height: 30px;
		line-height: 30px;
		text-align: center;
		text-decoration: none;
		border-radius: 15px;
		padding: 0;
		text-transform: uppercase;
	}

	#prayerengine .pe-pagination a span {
		display: inline;
	}

	#prayerengine #perecaptcha_widget {
		width: 100%;
	}

	#prayerengine #perecaptcha_widget #recaptcha_image {
		margin: 0 0 10px 0;
		width: 160px !important;
	}

	#prayerengine #perecaptcha_widget #recaptcha_response_field {
		width: 78%;
		margin: 0 0 8px 0;
		font-size: 16px !important;
	}

	#pe-submit-area {
		text-align: right;
		margin: 10px 0 0 0;
		padding: 10px 10px 10px 8px;
		height: inherit;
	}	

	#prayerengine h3.enmpe-poweredby {
		margin: 26px auto 10px auto !important;
		float: none;
	}
	
	#prayerengine p.enmpe-poweredbytext {
		margin: 26px 0 10px 0 !important;
		text-align: center;
		float: none;
	}

	#prayerengine .pe-prayer-even h4.answered span, #prayerengine .pe-prayer-odd h4.answered span {
		display: none;
	}

}


@media (-webkit-min-device-pixel-ratio: 2) {
	#prayerengine .pe-explore-bar h4.pe-form-toggle a {
		<?php if ( $enmpe_formicon == "light" ) { ?>
		background: url('../images/light_contact2x.png') no-repeat;
		<?php } else { ?>
		background: url('../images/dark_contact2x.png') no-repeat;
		<?php } ?>
		background-size: 18px 18px;
		background-position: 14px 11px;
		background-color: #<?php echo $enmpe_ftbg; ?>;
	}

	#prayerengine h3.enmpe-poweredby {
		<?php if ( $enmpe_logocolor == "light" ) { ?>
		background: url('../images/pe_light_poweredby2x.png') no-repeat;	
		<?php } elseif ( $enmpe_logocolor == "dark" ) { ?>
		background: url('../images/pe_dark_poweredby2x.png') no-repeat;
		<?php } ?>
		background-size: 148px 40px;
	}
}