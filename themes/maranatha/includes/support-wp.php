<?php
/**
 * WordPress Feature Support
 *
 * @package    Maranatha
 * @subpackage Functions
 * @copyright  Copyright (c) 2015, churchthemes.com
 * @link       http://churchthemes.com/themes/maranatha
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * @since      1.0
 */

// No direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Add theme support for WordPress features
 *
 * @since 1.0
 */
function maranatha_add_theme_support_wp() {

	global $_wp_additional_image_sizes;

	// Output HTML5 markup
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	// Title Tag
	add_theme_support( 'title-tag' );

	// RSS feeds in <head>
	add_theme_support( 'automatic-feed-links' );

	// Featured images
	add_theme_support( 'post-thumbnails' );

	// Custom Header
	add_theme_support( 'custom-header', array(
		'width'                  => $_wp_additional_image_sizes['maranatha-banner']['width'], // see includes/images.php
		'height'                 => $_wp_additional_image_sizes['maranatha-banner']['height'],
		'flex-width'             => false, // false fixes aspect ratio
		'flex-height'            => false, // false fixes aspect ratio
		'header-text'            => false,
	) );

	// Hide admin bar on front-end (interferes with sticky menu)
	// An "Edit" button for posts/pages shows as sticky at bottom
	show_admin_bar( false );

	// See support-wp.php for custom add_editor_style() with Customizer colors and fonts

}

add_action( 'after_setup_theme', 'maranatha_add_theme_support_wp' );
