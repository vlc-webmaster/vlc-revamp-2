<?php


function theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
    
    wp_enqueue_script( 'vlc-scrollto', get_stylesheet_directory_uri() . '/js/scrollto.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

add_filter( 'gplc_remove_choices', '__return_false' );
add_filter( 'gplc_pre_render_choice', 'my_add_how_many_left_message', 10, 4 );
function my_add_how_many_left_message( $choice, $exceeded_limit, $field, $form ) {
	$choice_counts = GWLimitChoices::get_choice_counts( $form['id'], $field );
	$count = rgar( $choice_counts, $choice['value'] ) ? rgar( $choice_counts, $choice['value'] ) : 0;
	$limit = rgar($choice, 'limit');
	$how_many_left = max( $limit - $count, 0 );
	$message = "($how_many_left spots left)";
	$choice['text'] = $choice['text'] . " $message";
	return $choice;
}